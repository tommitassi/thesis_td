# TRY UTILS

from utils.plotter.importer import *
from utils.trainer.trainAnn import *
from utils.plotter.plot3D import *
from utils.plotter.plot2D import *
from utils.getter.getParams import *
from utils.getter.getProblem import *
from utils.solver.supg import solveSUPG
from utils.solver.findTau import *
from utils.plotter.exporter import exportSolution

import fenics as fenics
import numpy as np
import matplotlib.pyplot as plt

###############################################################################
## Set parameters
degree = 3
n = 20
mu = 1e-4

###############################################################################
## Define problem
mesh, h = getMesh(n)

Pe_g = getPeg(mu)
Pe_h = getPeh(mu, h)
print("Peclet:", Pe_h)

f = getForcing(mu, degree)
u_exact = getExactSolution(mu, degree)

###############################################################################
## Theory prediction
tau_th = getTauTheory(n, degree, mu)
# uh_th = solveSUPG(n, degree, mu, tau_th)

###############################################################################
## Minimization prediction
## Error
# tau_min = minimizeError(n, degree, mu, flag_export=0, normType='H1')
# uh_min = solveSUPG(n, degree, mu, tau_min)

## Residual
# tau_min = minimizeResidual(n, degree, mu, flag_plot=0)
# uh_min = solveSUPG(n, degree, mu, 16.737031083428068)

###############################################################################
## Non-stabilized solution
# uh_none = solveSUPG(n, degree, mu, 0)

###############################################################################
## Dataset plot
# train_feat, train_targ, val_feat, val_targ, mean, std = loadAnnDataset("models/6th/dataset6.csv", flag_plot=0)

###############################################################################
## Neural-network-tau solution
# nn_model = loadAnnModel("models/6th/model_3x64")
#
# x_nn = np.array([[degree, h, np.log10(Pe_g)]])
# prediction_nn = nn_model.predict( (x_nn - mean) / std )
# tau_nn = 10**(-prediction_nn[0][0])
#
# uh_nn = solveSUPG(n, degree, mu, tau_nn)

###############################################################################
## Dataset vs Predictions vs Theory plot
train_feat, train_targ, val_feat, val_targ, mean, std = loadAnnDataset("models/6th/dataset6.csv", flag_plot=0)
nn_model = loadAnnModel("models/6th/model_3x64")

n_mu = 50
mu = np.geomspace(1e-5, 1e-1, n_mu)
Pe_g = 1 / np.sqrt(2) / mu
n_vec = [20]
degree_vec = [1, 2, 3]

A = np.zeros((n_mu, 3))
A[:,2] = np.log10(Pe_g)

err_nn = np.zeros_like(mu)
err_th = np.zeros_like(mu)

for n in n_vec:
    _, h = getMesh(n)
    A[:,1].fill(h)
    for degree in degree_vec:
        A[:,0].fill(degree)
        tau_nn = nn_model.predict( ( A - mean )/std )
        tau_nn = 10**(-tau_nn)

        if degree==1:
            color = 'r'
        elif degree==2:
            color = 'b'
        else:
            color= 'g'

        plt.plot(Pe_g, tau_nn, label="ANN     r = {}".format(degree), linewidth=2)

        # for j in range(n_mu):
        #     uh_nn = solveSUPG(n, degree, mu[j], tau_nn[j][0])
        #     err_nn[j] = getError(uh_nn, degree, mu[j])
        #
        # plt.plot(Pe_g, err_nn, label="ANN", linewidth=2)

for n in n_vec:
    for degree in degree_vec:

        if degree==1:
            color = '#1f77b4'
        elif degree==2:
            color = '#ff7f0e'
        else:
            color= '#2ca02c'

        plt.plot(Pe_g, getTauTheory(n, degree, mu), '--', label="Theory r = {}".format(degree), linewidth=2, color=color)

        # for j in range(n_mu):
        #     uh_th = solveSUPG(n, degree, mu[j], getTauTheory(n, degree, mu[j]))
        #     err_th[j] = getError(uh_th, degree, mu[j])
        #
        # plt.plot(Pe_g, err_th, '--', label="Theory", linewidth=2)

plt.xlabel(r'$\mathbb{P}_{\mathrm{e}g}$', fontsize=12)
plt.ylabel(r"$\tau$", fontsize=12)
plt.xscale("log")
plt.yscale("log")

plt.legend()

###############################################################################
## Make predictions
# predictionsAnn3D("models/6th/dataset6.csv", "models/6th/model_3x64", "Pe_g", "h",
#                  x_min=1e1, x_max=1e5, y_min=np.sqrt(2)/40, y_max=np.sqrt(2)/10,
#                  notUsed_value=4, nx=40, ny=30, flag_xlog=1, flag_theory=0)

###############################################################################
## Exact solution
# V = fenics.FunctionSpace(mesh, 'CG', degree)
# uh_exact = fenics.interpolate(u_exact, V)

###############################################################################
## Print tau-s
# print("Tau-theory:", tau_th)
# print("Tau-minimized:", tau_min)
# print("Tau-nn:", tau_nn)

###############################################################################
## Print errors
# print("Error tau-theory:", getError(uh_th, degree, mu))
# print("Error tau-minimized:", getError(uh_min, degree, mu))
# print("Error non-stabilized:", getError(uh_none, degree, mu))
# print("Error ANN-stabilized:", getError(uh_nn, degree, mu))

###############################################################################
## Plots
# title_params = "n = {}, Pe_h = {:.3}, r = {}".format(n, Pe_h, degree)

# solution3D(uh_th, title=None)
# # solution3D(uh_min, title="Minimized - " + title_params)
# solution3D(uh_none, title=None)
# solution3D(uh_exact, title=None, zlabel=r'$u(x,y)$')
# solution3D(uh_nn, title=None)

# minimization2D(n, degree, mu, "error", tau_min=tau_th/3, tau_max=5*tau_th, N_eval=79, title=None)
# plt.plot(tau_min, getError(uh_min, degree, mu), '.', label="Minimum", markersize=10)
# plt.plot(tau_th, getError(uh_th, degree, mu), '.', fillstyle='none', label="Theoretical", markersize=10)
# plt.legend()
# minimization2D(n, degree, mu, "residual")

# residual3D(uh_th, degree, mu, title="Residual of stabilized solution")
# residual3D(uh_none, degree, mu, title="Residual of non-stabilized solution")
# residual3D(uh_exact, degree_raised, mu, title="Residual of exact solution")

# '#1f77b4', '#ff7f0e', '#2ca02c',
# coords = mesh.coordinates()
# extractedSolution2D(degree, mu, uh_none, 0.9, r"Non-stabilized ($\tau = 0$)")
# extractedSolution2D(degree, mu, uh_th, 0.9, r"Theoretical $\tau$", '#2ca02c')
# extractedSolution2D(degree, mu, uh_nn, 0.9, r"ANN's $\tau$", '#d62728')

plt.show()
