# CREATE DATASET FOR ML

from utils.getter.getProblem import *
from utils.getter.getParams import *
from utils.solver.findTau import *

import numpy as np
from scipy.stats import loguniform

###############################################################################
# PARAMETRS DEFINITION

# File name
fname = "newDataset.csv"

# Total number of iterations per degree per mesh
max_iter = 100

# Save every batch_size minimizations
batch_size = 2

# FE space degrees
degree_vec = [1, 2, 3]

# Mesh sizes
n_vec = [10, 20, 40]

# Mu
mu_min = 1.0e-5
mu_max = 1.0e-1

###############################################################################
# LOOP
## x = [degree, h, Pe_global]
## y = [tau]

print('\n################# TOTAL NUMBER OF ITERATIONS: {} #################'.format(len(n_vec)*len(degree_vec)*max_iter))
A = np.zeros((4, batch_size))
num_batches = max_iter//batch_size

# Create new csv
f1 = open(fname, "w")
f1.write('# r, h, Pe_g, tau\n')
f1.close()

for n in n_vec:

    for degree in degree_vec:

        for i in range(num_batches):

            for j in range(batch_size):

                A[0, j] = degree
                _, A[1, j] = getMesh(n)

                mu = loguniform.rvs(mu_min, mu_max) # uniform distribution on a logscale
                A[2, j] = getPeg(mu)

                A[3, j] = minimizeError(n, degree, mu, flag_export=0, normType='H1')


            f2 = open(fname, "ab")
            np.savetxt(f2, A.transpose(), delimiter=",")
            f2.close()

            print('################# Completed batch {}/{}, for degree {} and n {} #################'.format(i+1, num_batches, degree, n))
