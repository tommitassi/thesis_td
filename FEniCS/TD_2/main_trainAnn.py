## TRAIN NEURAL-NETWORKS

from utils.trainer.trainAnn import *

import numpy as np
import matplotlib.pyplot as plt

###############################################################################
## Train ANN
fname_dataset = "dataset6.csv"

trainSingleAnn(fname_dataset, 64, 3, max_epochs=25000,
               batchType=32, lr0=1e-2, activation="relu",
               decay_rate=0, flag_save=1)

# findArchitectureAnn(fname_dataset, [64], [3], max_epochs=25000,
#                     batchType=32, lr0=1e-2, activation="relu",
#                     decay_rate=0)

# findHyperparametersAnn(fname_dataset)


###############################################################################
## Learning curves
# train_features, train_targets, val_x, val_y, mean, std = loadAnnDataset(fname_dataset)
# m_full, n = train_features.shape
#
# histories = {}
#
# for m in [100, 400, m_full]:
#     index_samples = np.linspace(0, int(len(train_features)-1), m, dtype=int)
#     train_x = train_features[index_samples, :]
#     train_y = train_targets[index_samples]
#
#     model = buildModel(n, 128, 2, "relu")
#
#     batch_size = getBatch("full", m)
#     setupModel(model, m, batch_size, lr0=1e-2, decay_rate=20)
#
#     callback = [tfdocs.modeling.EpochDots()]
#     history = trainModel(model, train_x, train_y, val_x, val_y,
#                          max_epochs=20000, batch_size=batch_size, callback=callback, verbose=1)
#
#     histories["m = {}".format(m)] = history
#
# plotter = tfdocs.plots.HistoryPlotter(metric = "mean_squared_error", smoothing_std=0)
# plotter.plot(histories)
# plt.yscale("log")
# plt.xlabel("Epochs")
# plt.show()
