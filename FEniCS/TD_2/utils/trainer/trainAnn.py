## TRAIN Artificial Neural Network

from tensorflow import keras
import tensorflow_docs as tfdocs
import tensorflow_docs.modeling, tensorflow_docs.plots
import numpy as np
import optuna

from ..plotter.importer import *

###############################################################################
## trainSingleAnn() trains an ANN from a dataset
## if flag_save==1, the model is saved on disk
## it returns nothing

def trainSingleAnn(fname_dataset, n_nodes, n_layers, max_epochs=10000,
                   batchType="full", lr0=1e-2, activation="relu",
                   decay_rate=1, flag_save=0):

    ##############
    # Load dataset
    train_x, train_y, val_x, val_y, mean, std = loadAnnDataset(fname_dataset)
    m, n = train_x.shape

    ##############
    # Build model
    model = buildModel(n, n_nodes, n_layers, activation)

    ##############
    # Compile the model
    batch_size = getBatch(batchType, m)
    setupModel(model, m, batch_size, lr0, decay_rate)

    ##############
    # Train the model
    history = trainModel(model, train_x, train_y, val_x, val_y,
                         max_epochs, batch_size)

    ##############
    # Save the model
    if flag_save:
        model_json = model.to_json() # save model to json
        with open("models/newModel.json", "w") as json_file:
            json_file.write(model_json)

        model.save_weights("models/newModel.h5") # save weights to HDF5
        print("Saved model to disk")

    return 0

###############################################################################
## findArchitectureAnn() trains a set of ANNs from a dataset and plots its
## training and validation errors over the epochs
## it returns nothing

def findArchitectureAnn(fname_dataset, nodes, layers, max_epochs=10000,
                        batchType="full", lr0=1e-2, activation="relu",
                        decay_rate=1):

    # keras.backend.set_floatx('float64')

    train_x, train_y, val_x, val_y, mean, std = loadAnnDataset(fname_dataset)
    m, n = train_x.shape

    histories = {}

    for n_layers in layers:

        for n_nodes in nodes:

            model = buildModel(n, n_nodes, n_layers, activation)

            batch_size = getBatch(batchType, m)
            setupModel(model, m, batch_size, lr0, decay_rate)

            callback = [tfdocs.modeling.EpochDots()]
            history = trainModel(model, train_x, train_y, val_x, val_y,
                                 max_epochs, batch_size, callback, verbose=0)

            histories["{} x {}".format(n_layers, n_nodes)] = history

    plotter = tfdocs.plots.HistoryPlotter(metric = "mean_squared_error", smoothing_std=10)
    plotter.plot(histories)
    plt.yscale("log")
    plt.xlabel("Epochs")
    plt.show()

    return 0

###############################################################################
## findHyperparametersAnn() uses optuna library to find the best hyperparameters
## of the ANN
## it returns nothing

def findHyperparametersAnn(fname_dataset, max_epochs=10000,
                           batchType="full", lr0=1e-2):

    train_x, train_y, val_x, val_y, mean, std = loadAnnDataset(fname_dataset)
    m, n = train_x.shape

    def objective(trial):

        model = buildModel(n,
                           trial.suggest_categorical("n_nodes", [16, 32, 64, 128]),
                           trial.suggest_categorical("n_layers", [1, 2, 3]),
                           "elu")

        batch_size = getBatch(batchType, m)

        setupModel(model,
                   m,
                   batch_size,
                   lr0,
                   decay_rate=trial.suggest_categorical("decay_rate", [1, 10, 20, 50]))

        history = trainModel(model, train_x, train_y, val_x, val_y,
                             max_epochs, batch_size, verbose=0)

        score = model.evaluate(val_x, val_y, verbose=0)

        return score[1]

    study = optuna.create_study(direction="minimize")
    study.optimize(objective, n_trials=100, timeout=600)

    print("Number of finished trials: {}".format(len(study.trials)))

    print("Best trial:")
    trial = study.best_trial

    print("  Value: {}".format(trial.value))

    print("  Params: ")
    for key, value in trial.params.items():
        print("    {}: {}".format(key, value))

    return 0

###############################################################################
## buildModel() builds a sequential ANN model
## it returns the built model

def buildModel(n_features, n_nodes, n_layers, activation):

    model = keras.Sequential()

    # input layer
    model.add(keras.layers.Dense(n_nodes,
                                 activation=None, # i.e. linear
                                 input_shape=(n_features,)))
    # model.add(keras.layers.Dropout(0.1))

    # hidden layers
    for i in range(n_layers-1):
        model.add(keras.layers.Dense(n_nodes,
                                     activation=activation,
                                     # kernel_regularizer=keras.regularizers.l2(1e-3),
                                     ))
        # model.add(keras.layers.Dropout(0.1))

    # output layer
    model.add(keras.layers.Dense(1, activation="relu"))

    model.summary()

    return model

###############################################################################
## setupModel() setups the learning rate schedule, the metrics and compiles the
## model
## it returns nothing

def setupModel(model, m_dataset, batch_size, lr0, decay_rate=1):

    steps_per_epoch = m_dataset//batch_size

    lr_schedule = keras.optimizers.schedules.InverseTimeDecay(
        lr0,
        decay_steps=steps_per_epoch*1000,
        decay_rate=decay_rate,
        staircase=False)

    # lr_schedule = keras.optimizers.schedules.ExponentialDecay(
    #     lr0,
    #     decay_steps=steps_per_epoch*1000,
    #     decay_rate=decay_rate)

    metrics = [
        keras.metrics.MeanSquaredError(),
        # keras.metrics.MeanSquaredLogarithmicError()
        ]

    model.compile(
        optimizer=keras.optimizers.SGD(lr0, momentum=0.9),
        loss="mean_squared_error",
        metrics=metrics)

    return 0

###############################################################################
## trainModel() trains given model on given dataset
## it returns the history of the training

def trainModel(model, train_x, train_y, val_x, val_y, max_epochs, batch_size,
               callback=None, verbose=1):

    history = model.fit(
        train_x,
        train_y,
        batch_size=batch_size,
        epochs=max_epochs,
        validation_data=(val_x, val_y),
        callbacks=callback,
        verbose=verbose)

    return history

###############################################################################
## getBatch() gets the batch size from the string batchType (batchType == "full"
## takes the full dataset, while batchType == "n" takes a mini-batch of size n)
## it returns the batch size

def getBatch(batchType, m_dataset):

    if batchType == "full":
        batch_size = m_dataset
    else:
        batch_size = int(batchType)

    return batch_size
