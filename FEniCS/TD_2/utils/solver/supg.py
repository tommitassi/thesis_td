# SUPG SOLVER IMPLEMENTATION

from fenics import *
from ..getter.getProblem import getForcing, getExactSolution, getMesh

###############################################################################
## solveSUPG() solves a transport-diffusion 2D problem with SUPG method
## it returns the numerical solution uh

def solveSUPG(n, degree, mu, tau):

    # Finite element space
    mesh, _ = getMesh(n)
    V = FunctionSpace(mesh, 'CG', degree)

    # Problem setting
    f = getForcing(mu, degree)
    u_exact = getExactSolution(mu, degree)

    mu = Constant(mu)
    b = Constant((1.0, 1.0))

    # Boundary conditions
    def boundary(x, on_boundary):
        return on_boundary

    bc = DirichletBC(V, u_exact, boundary)

    # Matrices and vectors
    u = TrialFunction(V)
    v = TestFunction(V)

    F = mu * dot(grad(u), grad(v)) * dx
    F += dot(b, grad(u)) * v * dx
    F -= f * v * dx

    if tau:

        F -= tau * mu * div(grad(u)) * dot(b, grad(v)) * dx
        F += tau * dot(b, grad(u)) * dot(b, grad(v)) * dx
        F -= tau * f * dot(b, grad(v)) * dx

    a = lhs(F)
    L = rhs(F)

    # Solve
    uh = Function(V)

    #############################################
    ## 1. Default LU solver
    solve(a == L, uh, bc)

    #############################################
    ## 2. Setting linear solver and its parameters
    # problem = LinearVariationalProblem(a, L, uh, bc)
    # solver = LinearVariationalSolver(problem)
    #
    # solver.parameters["linear_solver"] = 'gmres'
    # solver.parameters["preconditioner"] = 'ilu'
    # solver.parameters["krylov_solver"]["absolute_tolerance"] = 1e-12
    # solver.parameters["krylov_solver"]["relative_tolerance"] = 1e-10
    # solver.parameters["krylov_solver"]["maximum_iterations"] = 1000

    # solver.solve()

    ## List of available options
    # list_linear_solver_methods()
    # list_krylov_solver_preconditioners()
    # list_linear_algebra_backends()

    #############################################
    ## 3. Assemble matrix and vector
    # A = assemble(a)
    # b = assemble(L)
    # bc.apply(A, b)
    # U = uh.vector()
    # solve(A, U, b, 'gmres', 'ilu')

    # condition_number = numpy.linalg.cond(A.array())

    return uh
