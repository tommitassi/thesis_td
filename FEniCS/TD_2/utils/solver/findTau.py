# FIND-TAU FUNCTIONS

from fenics import *
import scipy.optimize as opt
import numpy as np
import matplotlib.pyplot as plt

from ..getter.getProblem import getExactSolution
from ..getter.getParams import getTauTheory, getError, getResidual
from .supg import solveSUPG

###############################################################################
## minimizeError() finds tau that minimizes the error in 'normType' norm
## (default is 'H1')
## if flag_export==1 the steps are exported in a .csv file
## it returns the value of tau

def minimizeError(n, degree, mu, flag_export=0, normType='H1'):

    tau = minimizer(n, degree, mu, "error", flag_export=flag_export, normType=normType)

    return tau

###############################################################################
## minimizeResidual() finds tau that minimizes the residual in strong form
## if flag_export==1 the steps are exported in a .csv file
## it returns the value of tau

def minimizeResidual(n, degree, mu, flag_export=0):

    tau = minimizer(n, degree, mu, "residual", flag_export=flag_export)

    return tau

###############################################################################
## minimizer() is called by minimizeError() or minimizeResidual()
## it finds tau that minimizes the relative function
## if flag_export==1 the steps are exported in a .csv file
## it returns the value of tau

def minimizer(n, degree, mu, flag_minimize, flag_export, normType=None):

    tau_init = getTauTheory(n, degree, mu)
    temp_func = 0
    tau_iterations = np.empty((0,2))

    def tauFunc(x):
        nonlocal temp_func
        nonlocal tau_iterations

        uh = solveSUPG(n, degree, mu, x[0])

        if flag_minimize == "error":
            temp_func = getError(uh, degree, mu, normType=normType)
        elif flag_minimize == "residual":
            temp_func = getResidual(uh, degree, mu)

        if flag_export:
            tau_iterations = np.append(tau_iterations, [[x[0], temp_func]], axis=0)

        return temp_func

    x0 = np.array([tau_init])

    # res = opt.minimize_scalar(tauFunc, bracket=(0,tau_init,1))
    # res = opt.minimize(tauFunc, x0, options={'disp': True, 'gtol' : 1e-7})
    res = opt.minimize(tauFunc, x0, method="L-BFGS-B", bounds=opt.Bounds(0,1.0), options={'disp': True})

    if flag_export:
        fname_cb = "tau_callback.csv"
        f_callback = open(fname_cb, 'w')
        f_callback.write("# tau, {}\n".format(flag_minimize))
        np.savetxt(f_callback, tau_iterations, delimiter=',')
        f_callback.close()

    return res.x[0]
