# IMPORTERS

from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plt

###############################################################################
## loadAnnModel() loads the ANN model and weights from respectively a .json and
## .h5 files
## it returns the model

def loadAnnModel(fname):
    # fname must be the absolute path of the model's files without extensions

    print("Loading model:", fname)

    # create the model from .json
    json_file = open(fname + ".json", 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = keras.models.model_from_json(loaded_model_json)

    # load weights into the model from .h5
    loaded_model.load_weights(fname + ".h5")

    print("\nModel successfully loaded from disk")

    return loaded_model

###############################################################################
## loadAnnDataset() loads the ANN dataset from a csv file
## if flag_plot==1 it makes a plot of the dataset
## it returns the normalized training features and targets, the normalized
## validation features and targets and the mean and standard deviation used to
## normalize the data

def loadAnnDataset(fname, flag_plot=0):

    print("Loading dataset")

    all_features = []
    all_targets = []
    with open(fname) as f:
        for i, line in enumerate(f):
            if i == 0:
                header = line[1:].strip().split(", ")
                print("\nHEADER:", header)
                continue  # Skip header
            fields = line.strip().split(",")
            all_features.append([float(v) for v in fields[:-1]])
            all_targets.append([float(fields[-1])])
            if i == 1:
                print("EXAMPLE FEATURES:", all_features[-1])
                print("EXAMPLE TARGETS :", all_targets[-1])

    features = np.array(all_features, dtype="float32")
    targets = np.array(all_targets, dtype="float32")
    m, n = features.shape
    print("\nfeatures.shape: ({}, {})".format(m, n))
    print("targets.shape:", targets.shape)

    ######################################
    ## Plot dataset
    if flag_plot == 1:
        # eps = 0.01
        # h = np.sqrt(2)/20
        for i in range(2, n):
            plt.figure(i)
            for j in range(m):
                # if features[j,1] < h + eps and features[j,1] > h - eps:
                if features[j,0] == 1:
                    plt.plot(features[j,i], targets[j], 'or', markersize=2, label='r = 1')
                elif features[j,0] == 2:
                    plt.plot(features[j,i], targets[j], 'ob', markersize=2, label='r = 2')
                elif features[j,0] == 3:
                    plt.plot(features[j,i], targets[j], 'og', markersize=2, label='r = 3')
                else:
                    plt.plot(features[j,i], targets[j], 'oy', markersize=2, label='r = 4')
            plt.yscale('log')
            plt.xscale('log')
            plt.xlabel(header[i])
            plt.ylabel(header[-1])
            plt.legend()

    ######################################
    ## Transform targets for better training
    targets = - np.log10(targets)
    features[:,2] = np.log10(features[:,2])

    ######################################
    ## Extract validation set
    num_val_samples = int(len(features) * 0.2) ## 20% of all features
    index_val_samples = np.linspace(1, int(len(features)-2), num_val_samples, dtype=int) ## equal distribution choice
    # index_val_samples = np.arange(m-num_val_samples, m) ## last 20% choice
    val_features = features[index_val_samples]
    val_targets = targets[index_val_samples]
    train_features = np.delete(features, index_val_samples, axis=0)
    train_targets = np.delete(targets, index_val_samples, axis=0)

    print("\nNumber of training samples:", len(train_features))
    print("Number of validation samples:", num_val_samples)

    ######################################
    ## Normalize the data
    mean = np.mean(train_features, axis=0)
    train_features -= mean
    val_features -= mean

    std = np.std(train_features, axis=0)
    train_features /= std
    val_features /= std

    print("\nDataset successfully loaded\n")

    return train_features, train_targets, val_features, val_targets, mean, std
