# 3D PLOTS

from utils.plotter.importer import *
from ..getter.getParams import getResidual, getTauTheory
from .importer import *

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.tri as tri

###############################################################################
## solution3D() plots the solution in 3D over its mesh
## it returns nothing

def solution3D(uh, title=None, zlabel=r"$u_h(x,y)$", cmap="viridis"):

    mesh = uh.function_space().mesh()
    X = mesh.coordinates()[:,0]
    Y = mesh.coordinates()[:,1]
    triangulation = tri.Triangulation(X, Y, mesh.cells())

    Z = uh.compute_vertex_values()

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.plot_trisurf(triangulation, Z, cmap=cmap)
    ax.set_xlabel(r'$x$')
    ax.set_ylabel(r'$y$')
    ax.set_zlabel(zlabel)
    if title:
        plt.title(title)

    return 0

###############################################################################
## residual3D() gets the residual of the solution and plots it in 3D over its
## mesh
## it returns nothing

def residual3D(uh, degree, mu, title="3D Residual", cmap="viridis"):

    residual = getResidual(uh, degree, mu, flag_entire=1)
    solution3D(residual, title=title, zlabel="residual", cmap=cmap)

    return 0

###############################################################################
## predictionsAnn3D() plots the prediction of the given ANN in 3D over two of
## its features (defined by x_label and y_label that must correspond to header's
## names - Pe_g annot be on y)
## it returns nothing

def predictionsAnn3D(fname_dataset, fname_model, x_label, y_label,
                     x_min, x_max, y_min, y_max, notUsed_value, nx=10, ny=3,
                     flag_xlog=0, flag_theory=0):

    ## load ANN dataset
    _, _, _, _, mean, std = loadAnnDataset(fname_dataset, flag_plot=0)
    with open(fname_dataset) as f:
        for i, line in enumerate(f):
            if i == 0:
                header = line[1:].strip().split(", ")

    ## load ANN trained model
    model = loadAnnModel(fname_model)

    ## set X and Y
    if flag_xlog == 1:
        x = np.geomspace(x_min, x_max, nx)
    else:
        x = np.linspace(x_min, x_max, nx)
    y = np.linspace(y_min, y_max, ny)

    for i in range(len(header)-1):
        if header[i] == x_label:
            i_x = i
        elif header[i] == y_label:
            i_y = i
        else:
            i_notUsed = i

    ## make predictions
    Z = np.zeros((nx, ny))

    A = np.zeros((nx, 3))

    if x_label == 'Pe_g':
        A[:,i_x] = np.log10(x)
        A[:,i_notUsed].fill(notUsed_value)
    else:
        A[:,i_x] = x
        A[:,i_notUsed].fill(np.log10(notUsed_value))

    for i in range(ny):
        A[:,i_y].fill(y[i])
        z = model.predict( (A - mean) / std )
        Z[:,i] = z[:,0]

    Z = 10**(-Z)

    ## 3D plot
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    X, Y = np.meshgrid(x, y)

    if flag_xlog:
        X = np.log10(X)
        xticks_order_min = int(np.floor(np.log10(x_min)))
        xticks_order_max = int(np.floor(np.log10(x_max)))
        xticks = 10.0**(np.arange(xticks_order_min, xticks_order_max+1))
        ax.set_xticks(np.log10(xticks))
        ax.set_xticklabels(xticks)

    # Z = np.log10(Z)
    # zticks_order_min = int(np.floor(np.amin(Z)))
    # zticks_order_max = int(np.floor(np.amax(Z)))
    # zticks = 10.0**(np.arange(zticks_order_min, zticks_order_max+1))
    # ax.set_zticks(np.log10(zticks))
    # ax.set_zticklabels(zticks)

    # zticks = [0, 0.005, 0.010, 0.015, 0.020, 0.025, 0.030]
    # zticks = [0, 0.01, 0.02, 0.03, 0.04, 0.05]
    # zticks = [0, 0.0025, 0.0050, 0.0075, 0.0100, 0.0125, 0.0150, 0.0175, 0.0200, 0.0225]
    # ax.set_zticks(zticks)
    # ax.set_zticklabels(zticks)

    ax.plot_surface(X, Y, Z.T, cmap='viridis')

    ## set labels
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    ax.set_zlabel(r"$\tau$")

    ## theory predictions
    if flag_theory:
        # fig2 = plt.figure()
        # ax2 = fig2.gca(projection='3d')

        Z_theory = np.zeros_like(Z)

        if i_notUsed == 0:
            degree = notUsed_value
        elif i_notUsed == 1:
            n = int(np.sqrt(2)/notUsed_value)
        elif i_notUsed == 2:
            mu = np.sqrt(2)/notUsed_value

        for j in range (nx):

            if i_x == 0:
                degree = x[j]
            elif i_x == 1:
                n = int(np.sqrt(2)/x[j])
            elif i_x == 2:
                mu = np.sqrt(2)/x[j]

            for i in range(ny):

                if i_y == 0:
                    degree = y[i]
                elif i_y == 1:
                    n = int(np.sqrt(2)/y[i])

                Z_theory[j,i] = getTauTheory(n, degree, mu)

        ax.plot_surface(X, Y, Z_theory.T)

        ## set labels
        # ax2.set_xlabel(x_label)
        # ax2.set_ylabel(y_label)
        # ax2.set_zlabel("tau")

    return 0
