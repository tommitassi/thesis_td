# 2D PLOTS

import matplotlib.pyplot as plt
import numpy as np
from ..getter.getProblem import getExactSolution
from ..solver.supg import solveSUPG
from ..getter.getParams import *

###############################################################################
## minimization2D() evaluates the error (flag_minimize="error") or the residual
## (flag_minimize="residual") with values of tau between tau_min (default:
## tauTheory/10) and tau_max (default: 2*tauTheory) and makes a plot of them
## it returns 0

def minimization2D(n, degree, mu, flag_minimize, tau_min=None, tau_max=None, N_eval=10, title="Minimization"):

    tauTheory = getTauTheory(n, degree, mu)
    if tau_max==None:
        tau_max = 3 * tauTheory
    if tau_min==None:
        tau_min = tauTheory / 10

    if tau_min:
        Tau = np.geomspace(tau_min, tau_max, N_eval)
    else: # if tau_min==0 --> geomspace is invalid
        Tau = np.linspace(0, tau_max, N_eval)

    Func = np.zeros(N_eval)

    for i in range(N_eval):
        uh = solveSUPG(n, degree, mu, Tau[i])
        if flag_minimize == "error":
            Func[i] = getError(uh, degree, mu)
        elif flag_minimize == "residual":
            Func[i] = getResidual(uh, degree, mu)

    plt.figure()
    plt.plot(Tau, Func)
    plt.xscale("log")
    plt.xlabel(r"$\tau$", fontsize=14)
    plt.ylabel(r"$E(\tau)$", fontsize=14)
    if title:
        plt.title(title)

    return 0

###############################################################################
## extractedSolution2D() extracts the solution at a given x and makes a plot
## it returns 0

def extractedSolution2D(degree, mu, uh, x_etracted, label=None, color=None):

    V = uh.function_space()
    mesh = V.mesh()
    coords = mesh.coordinates()

    k = 0
    Y = np.array([])
    K = np.array([], dtype='int')
    for xy in coords:
        if xy[0] == x_etracted:
            Y = np.append(Y, xy[1])
            K = np.append(K, k)
        k += 1

    u_exact = getExactSolution(mu, degree)
    uh_exact = interpolate(u_exact, V)

    plt.figure()
    plt.plot(Y, uh_exact.compute_vertex_values()[K], label=r'$u_{ex}$ interpolated', linewidth=2)
    plt.plot(Y, uh.compute_vertex_values()[K], color=color, label=label, linewidth=2)
    plt.xlabel(r'$y$')
    plt.ylabel(r'$u(\bar{x}, y)$')
    plt.legend()

    return 0
