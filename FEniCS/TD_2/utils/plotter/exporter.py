# EXPORTERS

from fenics import *

###############################################################################
## exportSolution() saves the solution in a VTK file
## it returns nothing

def exportSolution(uh, fname):
    vtkfile = File(fname + '.pvd')
    vtkfile << uh
    return 0
