## GET PROBLEM FUNCTIONS

from fenics import *
import numpy as np

###############################################################################
## getForcing() implments the forcing term for this TD setting
## it returns the forcing term as a fenics expression

def getForcing(mu, degree):

    degree_raised = degree

    ## Quarteroni's book 1
    # f = Constant(1)

    ## Quarteroni's book 2
    # mu_inv = 1 / mu
    # f1 = 1 / (1 - np.exp(-mu_inv))
    # f = Expression('2 - x[0] - x[1] + (x[0]*(x[0]-1) + x[1]*(x[1]-1)) * mu_inv * exp(-(1-x[0])*(1-x[1])*mu_inv) * f1',
    #                mu_inv=mu_inv, f1=f1, degree=degree_raised)

    ## Quarteroni's book 2 - revised
    # mu_inv = 1 / np.sqrt(mu)
    # f1 = 1 / (1 - np.exp(-mu_inv))
    # f = Expression('2 - x[0] - x[1] + ((1-x[0])*(1-x[0]-mu_inv) + (1-x[1])*(1-x[1]-mu_inv)) * exp(-(1-x[0])*(1-x[1])*mu_inv) * f1',
    #                 mu_inv=mu_inv, f1=f1, degree=degree_raised)

    ## Sin-Log
    # f1 = Expression('M_PI * log2( 1+(mu-1) * (x[0]+x[1]-x[0]*x[1]) ) / log2(mu)',
    #                  mu=mu, degree=degree_raised)
    # f2 = np.pi * np.log2(np.exp(1)) / np.log2(mu)
    # f3 = Expression('(mu-1) / (1 + (mu-1)*(x[0]+x[1]-x[0]*x[1]))',
    #                  mu=mu, degree=degree_raised)
    #
    # f = Expression('mu * f2 * f3*f3 * (cos(f1) + f2*sin(f1)) * (2 + x[0]*(x[0]-2) + x[1]*(x[1]-2)) +'
    #                'f2 * f3 * cos(f1) * (2-x[0]-x[1])',
    #                mu=mu, f1=f1, f2=f2, f3=f3, degree=degree_raised)

    ## Atan
    # f1 = Expression('((x[0]-0.5)*(x[0]-0.5) + (x[1]-0.5)*(x[1]-0.5) - 0.0625) / sqrt(mu)',
    #                 mu=mu, degree=degree_raised)
    # f2 = Expression('1 / (1 + f1*f1)', f1=f1, degree=degree_raised)
    # f = Expression('2 * f2 * ( 2*sqrt(mu) - (x[0]+x[1]-1)/sqrt(mu) - f1* f2 * ((2*x[0]-1)*(2*x[0]-1) + (2*x[1]-1)*(2*x[1]-1)) )',
    #                mu=mu, f1=f1, f2=f2, degree=degree_raised)

    ## 1D sum
    f = Constant(0)

    # 1D sum 2
    # f = Constant(1)

    return f

###############################################################################
## getExactSolution() implements the exact solution for this TD setting
## it returns the exact solution as a fenics expression

def getExactSolution(mu, degree):

    ## Quarteroni's book 1
    # u = Constant(0)

    ## Quarteroni's book 2
    # mu_inv = 1 / mu
    # f1 = 1 / (1 - np.exp(-mu_inv))
    # u = Expression('x[0] + x[1] * (1-x[0]) + ( exp(-mu_inv) - exp(-(1-x[0])*(1-x[1])*mu_inv) ) * f1',
    #                mu_inv=mu_inv, f1=f1, degree=degree)

    ## Quarteroni's book 2 - revised
    # mu_inv = 1 / np.sqrt(mu)
    # f1 = 1 / (1 - np.exp(-mu_inv))
    # u = Expression('x[0] + x[1] * (1-x[0]) + ( exp(-mu_inv) - exp(-(1-x[0])*(1-x[1])*mu_inv) ) * f1',
    #                mu_inv=mu_inv, f1=f1, degree=degree)

    ## Sin-Log
    # u = Expression('sin(M_PI*log2(1+(mu-1)*(x[0]+x[1]-x[0]*x[1]))/log2(mu))',
    #                 mu=mu, degree=degree)

    ## Atan
    # u = Expression('- atan( ((x[0]-0.5)*(x[0]-0.5) + (x[1]-0.5)*(x[1]-0.5) - 0.0625) / (sqrt(mu)) )',
    #                mu=mu, degree=degree)

    ## 1D sum
    u = Expression('(exp((x[0] - 1) / mu) - exp(-1 / mu)) / (1 - exp(-1 / mu)) + '
                   '(exp((x[1] - 1) / mu) - exp(-1 / mu)) / (1 - exp(-1 / mu))',
                   mu=mu, degree=degree)

    ## 1D sum 2
    # u = Expression('0.5*(x[0] + x[1]) + (exp(-1/mu) - 0.5*exp(-(1-x[0])/mu) - 0.5*exp(-(1-x[1])/mu)) / (1-exp(-1/mu))',
    #                mu=mu, degree=degree)

    return u

###############################################################################
## getMesh() creates a structured mesh of triangles nxn on [0,1]^2
## it returns the mesh and its cell size

def getMesh(n, diagonal="left"):
    mesh = UnitSquareMesh(n, n, diagonal=diagonal)
    h = mesh.hmax()
    return mesh, h
