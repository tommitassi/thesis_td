## GET PARAMETERS

from fenics import *
import numpy as np
from .getProblem import *

###############################################################################
## getPeg() returns the global Peclet number

def getPeg(mu):
    return 1 / (np.sqrt(2) * mu)

###############################################################################
## getPeh() returns the local Peclet number

def getPeh(mu, h):
    return h / (np.sqrt(2) * mu)

###############################################################################
## getTauTheory() returns the tau that can be found on Quarteroni's book

def upwind_function(x):
    return 1 / np.tanh(x) - 1 / x

def getTauTheory(n, degree, mu):
    _, h = getMesh(n)
    Pe_h = getPeh(mu, h)
    eps = upwind_function(Pe_h/degree)
    return h / 2 / np.sqrt(2) / degree * eps

###############################################################################
## getError() computes the error in 'normType' norm (default is 'H1')
## it returns the error

def getError(uh, degree, mu, normType='H1'):
    u_exact = getExactSolution(mu, degree)

    V = uh.function_space()
    u = interpolate(u_exact, V)

    err_vertexes = uh.compute_vertex_values() - u.compute_vertex_values()
    err2 = np.dot(err_vertexes, err_vertexes)

    return sqrt(err2)
    # return sqrt( assemble((u-uh)**2 * dx ) )
    # return errornorm(u_exact, uh, normType)

###############################################################################
## getResidual() computes the residual in strong form by projecting it into the
## function space of the solution and by computing its 'L2' norm
## it returns the residual norm
## if flag_entire==1 the entire residual is returned (e.g. for 3D plots)

def getResidual(uh, degree, mu, flag_entire=0):

    mesh = uh.function_space().mesh()
    V = FunctionSpace(mesh, 'CG', degree)

    f = getForcing(mu, degree)
    mu = Constant(mu)
    b = Constant((1, 1))

    if degree == 1:
    # degree==1 => div(grad(uh))==0, so a projection of grad(uh) is used instead
        VV = VectorFunctionSpace(mesh, 'CG', degree)
        grad_uh_proj = project(grad(uh), VV)
        res_proj = project(- mu * div(grad_uh_proj) + dot(b, grad(uh)) - f, V)

    else:
        res_proj = project(- mu * div(grad(uh)) + dot(b, grad(uh)) - f, V)

    if flag_entire:
        return res_proj

    return sqrt( assemble(res_proj**2 * dx(mesh)) )
