//define quantities
L = 1.0; 
N = 160; 

h = L/N; 

Point(1) = {0, 0, 0, h};
Point(2) = {L, 0, 0, h};
Point(3) = {L, L, 0, h};
Point(4) = {0, L, 0, h};

Line(1)= {1, 2};
Line(2)= {2, 3};
Line(3)= {3, 4};
Line(4)= {4, 1};

Line Loop (1) = {1, 2, 3, 4};

//Plane Surface
Plane Surface (1) = {1};

//Physical Surface
Physical Surface ("fluid", 1)={1};

//Physical Volume
Physical Line ("bottom", 1) = {1};
Physical Line ("left", 2) = {2};
Physical Line ("top", 3) = {3};
Physical Line ("right", 4) = {4};



