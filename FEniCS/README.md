Transport-Diffusion problem (1D and 2D) solved using FEniCS library with the SUPG (Streamline Upwind Petrov-Galerkin) method.

The computation is used to create a dataset to feed the machine learning on.
