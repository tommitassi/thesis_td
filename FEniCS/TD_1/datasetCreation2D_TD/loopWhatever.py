# WHATEVER-LOOPING FUNCTION
# loopWhatever() loops over a chosen parameter and computes the optimal tau
# it returns the arrays of tau, the varied parameter and the Peclet number

# Values for flag
# 1: b (vaires bx and by with the same values)
# 2: mu
# 3: C_cos (vaires C_x and C_y with the same values)
# 4: omega (vaires omega_x and omega_y with the same values)
# 5: phi (vaires phi_x and phi_y with the same values)

from fenics import *
import numpy as np
from findTau2D import findTau2D


def loopWhatever(mesh, degree, bx, by, mu, C_x, omega_x, phi_x, C_y, omega_y, phi_y, flag, minP, maxP, rangeP):

    vecP = np.geomspace(minP, maxP, rangeP, dtype=np.float64)

    tau = np.zeros(rangeP)
    Pe = np.zeros(rangeP)

    for i in range(rangeP):

        if flag == 1:
            bx = vecP[i]
            by = vecP[i]
        elif flag == 2:
            mu = vecP[i]
        elif flag == 3:
            C_x = vecP[i]
            C_y = vecP[i]
        elif flag == 4:
            omega_x = vecP[i]
            omega_y = vecP[i]
        elif flag == 5:
            phi_x = vecP[i]
            phi_y = vecP[i]
        else:
            print('Error: invalid value of flag')
            break

        f = Expression('Cx*cos(omx*x[0] + phix) + '
                       'Cy*cos(omy*x[1] + phiy)',
                       Cx=C_x, omx=omega_x, phix=phi_x, Cy=C_y, omy=omega_y, phiy=phi_y, degree=2*degree+1)

        def u_px(x):
          return C_x / (mu*omega_x*omega_x + bx*bx/mu) * ( np.cos(omega_x*x + phi_x) + bx/mu/omega_x * np.sin(omega_x*x + phi_x) )
        def u_py(y):
          return C_y / (mu*omega_y*omega_y + by*by/mu) * ( np.cos(omega_y*y + phi_y) + by/mu/omega_y * np.sin(omega_y*y + phi_y) )

        u_exact = Expression('(upx0*(exp(bx*x[0]/mu)-exp(bx/mu)) + (exp(bx*x[0]/mu)-1)*(1-upx1)) /(exp(bx/mu)-1) + Cx / (mu*omx*omx + bx*bx/mu) * ( cos(omx*x[0] + phix) + bx/mu/omx * sin(omx*x[0] + phix) ) +'
                             '(upy0*(exp(by*x[1]/mu)-exp(by/mu)) + (exp(by*x[1]/mu)-1)*(1-upy1)) /(exp(by/mu)-1) + Cy / (mu*omy*omy + by*by/mu) * ( cos(omy*x[1] + phiy) + by/mu/omy * sin(omy*x[1] + phiy) )',
                            bx=bx, upx0=Constant(u_px(0)), upx1=Constant(u_px(1)), Cx=C_x, omx=omega_x, phix=phi_x, mu=mu,
                            by=by, upy0=Constant(u_py(0)), upy1=Constant(u_py(1)), Cy=C_y, omy=omega_y, phiy=phi_y, degree=4*degree+1)

        h = mesh.hmax()
        b_norm = sqrt(bx*bx+by*by)
        Pe[i] = b_norm * h / 2 / mu
        if b_norm * h > 6 * mu:
            tau_init = 0.5 * h / b_norm
        else:
            tau_init = 0.5 * h * h / 6 / mu

        tau[i] = findTau2D(mesh, degree, f, u_exact, bx, by, mu, tau_init)

    return tau, vecP, Pe
