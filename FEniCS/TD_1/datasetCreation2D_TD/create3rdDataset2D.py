# HOMOGENEOUS 2D DATASET

###############################################################################
# IMPORT LIBRARIES

from fenics import *
import numpy as np
from loop3rdDataset import loop3rdDataset
import matplotlib.pyplot as plt

###############################################################################
# PARAMETRS DEFINITION

# Finite elements degree
# degree =

# Problem parameters
# mu = 1.0          # diffusion
# bx, by = 1.0, 1.0   # transport

# NULL FORCING

# Mesh parameters
# Pe = 1.1          # Pe > 1.0 for unstable numerical methods
# nx, ny = 40, 40

###############################################################################
# LOOP PARAMETRS DEFINITION

# File name
fname = "tryDataset.csv"

# Total number of iterations per degree per mesh
max_iter = 200
iter_per_batch = 10

# FE space degrees
degree_vec = [1, 2, 3]

# Mesh sizes
n_vec = [20] #10, 20, 40, 80

# Peclet global: Pe_g = |b| * L / 2 / mu
Pe_h_min = 1.1 # Pe_g_min = Pe_h_min / h

# max(bx,by)/mu
b_mu_limit = 700

# Mu
mu_min = 1.5e-3
mu_max = 1.0

# Gamma: gamma = atan(by/bx)
gamma_min = 0.0
gamma_max = np.pi / 2

###############################################################################
# PROBLEM SETUP

# Homogeneous forcing
f = Constant(0)

print('\n################# TOTAL NUMBER OF ITERATIONS: {} #################'.format(len(n_vec)*len(degree_vec)*max_iter))

###############################################################################
# MAIN
## x = [degree, h, Pe_global, mu, gamma]
## y = [tau]
A = np.zeros((6, max_iter))
num_batches = max_iter//iter_per_batch

## For new csv files
f1 = open(fname, "w")
f1.write('# degree, h, Pe, Mu, Gamma, Tau\n')
f1.close()

for n in n_vec:

    mesh = UnitSquareMesh(n, n, 'crossed')
    h = mesh.hmax()

    for degree in degree_vec:

        for i in range(num_batches):

            A = loop3rdDataset(mesh, f, degree, iter_per_batch, Pe_h_min, b_mu_limit, mu_min, mu_max, gamma_min, gamma_max)

            A[0].fill(degree)
            A[1].fill(h)

            f2 = open(fname, "ab")
            np.savetxt(f2, A.transpose(), delimiter=",")
            f2.close()

            print('################# Completed batch {}/{}, for degree {} and n {} #################'.format(i+1, num_batches, degree, n))

###############################################################################
# DEBUG
# plt.plot(A[2], A[-1], '.')
# plt.show()
