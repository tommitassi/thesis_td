# HOMOGENEOUS 2D DATASET

###############################################################################
# IMPORT LIBRARIES

from fenics import *
import numpy as np
from loopPe import loopPe
import matplotlib.pyplot as plt

###############################################################################
# PARAMETRS DEFINITION

# Finite elements degree
# degree =

# Problem parameters
# mu = 1.0          # diffusion
# bx, by = 1.0, 1.0   # transport

# NULL FORCING

# Mesh parameters
# Pe = 1.1          # Pe > 1.0 for unstable numerical methods
nx, ny = 40, 40

###############################################################################
# LOOP PARAMETRS DEFINITION

# Total number of iterations per degree
max_iter = 50

# FE space degrees
degree_vec = [1, 2, 3]

# Peclet
Pe_min = 1.1
Pe_max = 10.0

# Mu
mu_down = 1.5e-3
mu_max = 1.0

# Beta (max(bx, by))
b_down = 1.0
b_mu_limit = 700.0

###############################################################################
# PROBLEM SETUP

# Mesh
mesh = UnitSquareMesh(nx, ny, 'crossed')
h = mesh.hmax()

# Homogeneous forcing
f = Constant(0)

###############################################################################
# MAIN
## x = [degree, Pe, mu, bx, by]
## y = [tau]
A = np.zeros((6, max_iter))

## For new csv files
f1 = open("tryDataset.csv", "w")
f1.write('# degree, Pe, Mu, Beta_x, Beta_y, Tau\n')
f1.close()

f2 = open('tryDataset.csv','ab')

for degree in degree_vec:

    A = loopPe(mesh, f, degree, max_iter, Pe_min, Pe_max, mu_down, mu_max, b_down, b_mu_limit)

    A[0].fill(degree)

    np.savetxt(f2, A.transpose(), delimiter=",")

    print('################################# Completed degree {} #################################'.format(degree))

f2.close()

###############################################################################
# DEBUG
# plt.plot(A[1], A[-1], '.')
# plt.show()
