###############################################################################
# IMPORT LIBRARIES

from fenics import *
import numpy as np
from loopWhatever import loopWhatever
import matplotlib.pyplot as plt

###############################################################################
# PARAMETRS DEFINITION

# Finite elements degree
degree = 1

# Problem parameters
mu = 1.0          # diffusion
bx, by = 1.0, 1.0   # transport

# Cosine forcing parameters
C = 1.0           # amplitude
omega = np.pi     # frequency
phi = 0.0         # phase

# Mesh parameters
# Pe = 1.1          # Pe > 1.0 for unstable numerical methods
nx, ny = 20, 20

###############################################################################
# LOOP PARAMETRS DEFINITION

# Loop times
max_iter = 10

# Mu loop parameters
range_mu = 200      # beta-loop max iterations
mu_min = 1.6e-3        # minimum value of beta
mu_max = 0.3      # maximum value of beta

# C loop parameters
C_min = 0
C_max = 10

# Omega loop parameters
omega_min = np.pi
omega_max = 10*np.pi

# phi loop parameters
phi_min = 0
phi_max = np.pi/2

###############################################################################
# PROBLEM SETUP

mesh = UnitSquareMesh(nx, ny, 'crossed')
h = mesh.hmax()

###############################################################################
# MAIN

A = np.zeros((8, range_mu))

f1 = open("tryDataset.csv", "w")
f1.write('# Mu, Beta_x, Beta_y, Pe, C_f, Omega, Phi, Tau\n')
f1.close()
f2 = open('tryDataset.csv','ab')

A[7], A[0], A[3]  = loopWhatever(mesh, degree, bx, by, 0, C, omega, phi, C, omega, phi,
                                2, mu_min, mu_max, range_mu)

A[1].fill(bx)
A[2].fill(by)
A[4].fill(C)
A[5].fill(omega)
A[6].fill(phi)

# for i in range(max_iter):
#
#     C = (C_max - C_min) * np.random.random_sample() + C_min
#     omega = (omega_max - omega_min) * np.random.random_sample() + omega_min
#     phi = (phi_max - phi_min) * np.random.random_sample() + phi_min
#
#     A[7], A[0], A[3]  = loopWhatever(mesh, degree, bx, by, 0, C, omega, phi, C, omega, phi,
#                             2, mu_min, mu_max, range_mu)
#
#     A[1].fill(bx)
#     A[2].fill(by)
#     A[4].fill(C)
#     A[5].fill(omega)
#     A[6].fill(phi)
#
#     np.savetxt(f2, A.transpose(), delimiter=",")
#
#     print('################################# {}/{} #################################'.format(i+1, max_iter))

np.savetxt(f2, A.transpose(), delimiter=",")
f2.close()

plt.xscale("log")
plt.plot(A[0], A[7])
plt.show()
