# TAU-THEORY FUNCTION
# tauTheory() implements the tau that can be found on Quarteroni's book
# it returns the value of tau (or an array of values)

import numpy as np

def upwind_function(x):
    return 1 / np.tanh(x) - 1 / x

def tauTheory(h, b_norm, Pe_h, r=1):
    return h / 2 / b_norm / r * upwind_function(Pe_h/r)
