# FIND-TAU FUNCTION
# findTau2D() finds tau that minimizes the error in H1 norm
# it returns the value of tau

from fenics import *
import scipy.optimize as opt
from solveSupg2D import solveSupg2D

def findTau2D(mesh, degree, f, u_exact, bx, by, mu, tau_init):

    def error_tauFunc(x):
        uh = solveSupg2D(mesh, degree, f, u_exact, bx, by, mu, x)
        return errornorm(u_exact, uh, 'H1')

    res = opt.minimize_scalar(error_tauFunc, bracket=(0, tau_init))

    return res.x
