# DATASET-CREATION-LOOPING FUNCTION
# loop3rdDataset() loops over the features: x = [degree, h, Pe_global, mu, gamma]
# it returns the array containing features and targets

from fenics import *
import numpy as np
# from scipy.stats import loguniform
from findTau2D import findTau2D


def loop3rdDataset(mesh, f, degree, max_iter, Pe_h_min, b_mu_limit, mu_min, mu_max, gamma_down, gamma_up):
    A = np.zeros((6, max_iter)) # degree, h, Pe_global, mu, gamma, tau

    h = mesh.hmax()

    for i in range(max_iter):

        Pe_min = Pe_h_min / h
        Pe_max = b_mu_limit / np.sqrt(2)
        Pe_g = (Pe_max - Pe_min) * np.random.random_sample() + Pe_min

        mu = (mu_max - mu_min) * np.random.random_sample() + mu_min
        # mu = loguniform.rvs(mu_min, mu_max) # uniform distribution on a logscale

        b_norm = 2 * mu * Pe_g # L = 1

        if b_norm / mu > b_mu_limit:
            gamma_min = np.arccos(b_mu_limit * mu / b_norm)
            gamma_max = np.arcsin(b_mu_limit * mu / b_norm)
        else:
            gamma_min = gamma_down
            gamma_max = gamma_up
        gamma = (gamma_max - gamma_min) * np.random.random_sample() + gamma_min

        bx = b_norm * np.cos(gamma)
        by = b_norm * np.sin(gamma)

        print('######## Pe: {:.5}, mu: {:.5e}, b: ({:.5},{:.5}) ########'.format(Pe_g, mu, bx, by))

        if b_norm * h > 6 * mu:
            tau_init = 0.5 * h / b_norm
        else:
            tau_init = 0.5 * h * h / 6 / mu

        u_exact = Expression('(exp(bx * (x[0] - 1) / mu) - exp(-bx / mu)) / (1 - exp(-bx / mu)) + '
                             '(exp(by * (x[1] - 1) / mu) - exp(-by / mu)) / (1 - exp(-by / mu))',
                             mu=mu, bx=bx, by=by, degree=4*degree+1)

        tau = findTau2D(mesh, degree, f, u_exact, bx, by, mu, tau_init)

        A[2,i] = Pe_g
        A[3,i] = mu
        A[4,i] = gamma
        A[5,i] = tau

    return A
