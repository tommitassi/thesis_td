# PECLET-LOOPING FUNCTION
# loopPeclet() loops over some chosen values of the Peclet choosing randomly mu
# and beta within given ranges
# it returns the arrays of tau, mu and beta

from fenics import *
import numpy as np
# from scipy.stats import loguniform
from findTau2D import findTau2D


def loopPe(mesh, f, degree, max_iter, Pe_min, Pe_max, mu_down, mu_max, b_down, b_mu_limit):

    A = np.zeros((6, max_iter)) # degree, Pe, mu, bx, by, tau

    h = mesh.hmax()

    for i in range(max_iter):

        Pe = (Pe_max - Pe_min) * np.random.random_sample() + Pe_min

        mu_min = max(mu_down, b_down*h/2/Pe)
        mu = (mu_max - mu_min) * np.random.random_sample() + mu_min
        # mu = loguniform.rvs(mu_min, mu_max) # uniform distribution on a logscale

        b_norm = 2 * mu * Pe / h

        if b_norm > b_mu_limit * mu: # if max(bx,by)/mu > b_mu_limit the solver crashes
            b_max = b_mu_limit * mu
            b_min = sqrt(b_norm*b_norm - (b_mu_limit * mu)*(b_mu_limit * mu))
        else:
            b_max = b_norm
            b_min = b_down

        by = (b_max - b_min) * np.random.random_sample() + b_min
        bx = sqrt(b_norm*b_norm - by*by)

        print('##### Pe: {:.5}, mu: {:.5e}, b: ({:.5},{:.5}) #####'.format(Pe, mu, bx, by))

        if b_norm * h > 6 * mu:
            tau_init = 0.5 * h / b_norm
        else:
            tau_init = 0.5 * h * h / 6 / mu

        u_exact = Expression('(exp(bx * (x[0] - 1) / mu) - exp(-bx / mu)) / (1 - exp(-bx / mu)) + '
                             '(exp(by * (x[1] - 1) / mu) - exp(-by / mu)) / (1 - exp(-by / mu))',
                             mu=mu, bx=bx, by=by, degree=4*degree+1)

        tau = findTau2D(mesh, degree, f, u_exact, bx, by, mu, tau_init)

        A[1,i] = Pe
        A[2,i] = mu
        A[3,i] = bx
        A[4,i] = by
        A[5,i] = tau

        print('#################### {}/{} with degree {} ####################'.format(i+1, max_iter, degree))

    return A
