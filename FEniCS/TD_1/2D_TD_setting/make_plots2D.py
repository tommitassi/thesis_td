###############################################################################
# IMPORT LIBRARIES

from fenics import *
import numpy as np
import matplotlib.pyplot as plt
from solve_supg2D import solve_supg2D
from find_tau2D import findTau_autoRange2D, findTau_minimization2D

###############################################################################
# PARAMETRS DEFINITION

# Finite elements degree
degree = 1

# Problem parameters
# mu = 1.0          # diffusion
# bx, by = 200, 200   # transport

# Cosine forcing parameters
C_x = 0.0           # amplitude in x (0 for homogeneous problem)
C_y = 0.0           # amplitude in y (0 for homogeneous problem)_
omega_x = np.pi     # frequency in x
omega_y = np.pi     # frequency in y
phi_x = 0.0         # phase in x
phi_y = 0.0         # phase in x

# Mesh parameters
# Pe = 1.1          # Pe > 1.0 for unstable numerical methods
nx, ny = 40, 40

# Tau loop parameters
range_tau = 40    # tau-loop max iterations
tau_min = 1e-06   # minimum value of tau
tau_max = 3e-04   # maximum value of tau

# Beta loop parameters
b_min, b_max = 100.0, 500.0
range_b = 5

# Mu loop parameters
mu_min, mu_max = 1.6e-3, 0.3
range_mu = 30

# Omega loop parameters
om_min, om_max = np.pi, 20*np.pi
range_om = 20

# Phi loop parameters
phi_min, phi_max = 0, np.pi
range_phi = 20

# Degree loop parameters
degree_vec = [1, 2, 3, 4, 5]

###############################################################################
# PROBLEM SETUP

# Forced
# f = Expression('Cx*cos(omx*x[0] + phix) + '
#                'Cy*cos(omy*x[1] + phiy)',
#                Cx=C_x, omx=omega_x, phix=phi_x, Cy=C_y, omy=omega_y, phiy=phi_y, degree=2*degree+1)
#
# def u_px(x):
#   return C_x / (mu*omega_x*omega_x + bx*bx/mu) * ( np.cos(omega_x*x + phi_x) + bx/mu/omega_x * np.sin(omega_x*x + phi_x) )
# def u_py(y):
#   return C_y / (mu*omega_y*omega_y + by*by/mu) * ( np.cos(omega_y*y + phi_y) + by/mu/omega_y * np.sin(omega_y*y + phi_y) )
#
# u_exact = Expression('(upx0*(exp(bx*x[0]/mu)-exp(bx/mu)) + (exp(bx*x[0]/mu)-1)*(1-upx1)) /(exp(bx/mu)-1) + Cx / (mu*omx*omx + bx*bx/mu) * ( cos(omx*x[0] + phix) + bx/mu/omx * sin(omx*x[0] + phix) ) +'
#                      '(upy0*(exp(by*x[1]/mu)-exp(by/mu)) + (exp(by*x[1]/mu)-1)*(1-upy1)) /(exp(by/mu)-1) + Cy / (mu*omy*omy + by*by/mu) * ( cos(omy*x[1] + phiy) + by/mu/omy * sin(omy*x[1] + phiy) )',
#                     bx=bx, upx0=Constant(u_px(0)), upx1=Constant(u_px(1)), Cx=C_x, omx=omega_x, phix=phi_x, mu=mu,
#                     by=by, upy0=Constant(u_py(0)), upy1=Constant(u_py(1)), Cy=C_y, omy=omega_y, phiy=phi_y, degree=4*degree+1)

# Homogeneous
# f = Constant(0)
# u_exact = Expression('(exp(bx * (x[0] - 1) / mu) - exp(-bx / mu)) / (1 - exp(-bx / mu)) + '
#                      '(exp(by * (x[1] - 1) / mu) - exp(-by / mu)) / (1 - exp(-by / mu))',
#                      mu=mu, bx=bx, by=by, degree=2*degree+1)

# Geometry setup
mesh = UnitSquareMesh(nx, ny, 'crossed')
h = mesh.hmax()
# b_norm = sqrt(bx*bx + by*by)

# Pe = b_norm * h / 2 / mu
#
# if b_norm * h > 6 * mu:
#     tau_th = 0.5 * h / b_norm
# else:
#     tau_th = 0.5 * h * h / 6 / mu
# tau_order = int(np.floor(np.log10(tau_th)))

###############################################################################
# MAIN

# mesh, u = solve_supg2D(nx, ny, degree, f, u_exact, bx, by, mu, None)
# TAU, ERR = find_tau2D(nx, ny, degree, f, u_exact, bx, by, mu, range_tau, tau_min, tau_max)

###############################################################################
# LOOPS

# Beta loop ###############################################################################
# b_vec = np.linspace(b_min, b_max, range_b, dtype=np.float64)
# tau_vec = np.zeros(range_b)
# tauTh_vec = np.zeros(range_b)
# err_vec = np.zeros(range_b)
# errTh_vec = np.zeros(range_b)
# Pe_vec = np.zeros(range_b)
#
# f = Expression('Cx*cos(omx*x[0] + phix) + '
#                'Cy*cos(omy*x[1] + phiy)',
#                Cx=C_x, omx=omega_x, phix=phi_x, Cy=C_y, omy=omega_y, phiy=phi_y, degree=2*degree+1)
#
# for i in range(0, range_b):
#
#     b_norm = sqrt(2) * b_vec[i]
#
#     Pe_vec[i] = b_norm * h / 2 / mu
#
#     # Tau theory definition
#     if b_norm * h > 6 * mu:
#         tauTh_vec[i] = 0.5 * h / b_norm
#     else:
#         tauTh_vec[i] = 0.5 * h / (6 * mu / h)
#
#     tau_order = int(np.floor(np.log10(tauTh_vec[i])))
#
#     # Exact solution
#     def u_px(x):
#       return C_x / (mu*omega_x*omega_x + b_vec[i]*b_vec[i]/mu) * ( np.cos(omega_x*x + phi_x) + b_vec[i]/mu/omega_x * np.sin(omega_x*x + phi_x) )
#     def u_py(y):
#       return C_y / (mu*omega_y*omega_y + b_vec[i]*b_vec[i]/mu) * ( np.cos(omega_y*y + phi_y) + b_vec[i]/mu/omega_y * np.sin(omega_y*y + phi_y) )
#
#     u_exact = Expression('(upx0*(exp(bx*x[0]/mu)-exp(bx/mu)) + (exp(bx*x[0]/mu)-1)*(1-upx1)) /(exp(bx/mu)-1) + Cx / (mu*omx*omx + bx*bx/mu) * ( cos(omx*x[0] + phix) + bx/mu/omx * sin(omx*x[0] + phix) ) +'
#                          '(upy0*(exp(by*x[1]/mu)-exp(by/mu)) + (exp(by*x[1]/mu)-1)*(1-upy1)) /(exp(by/mu)-1) + Cy / (mu*omy*omy + by*by/mu) * ( cos(omy*x[1] + phiy) + by/mu/omy * sin(omy*x[1] + phiy) )',
#                         bx=b_vec[i], upx0=Constant(u_px(0)), upx1=Constant(u_px(1)), Cx=C_x, omx=omega_x, phix=phi_x, mu=mu,
#                         by=b_vec[i], upy0=Constant(u_py(0)), upy1=Constant(u_py(1)), Cy=C_y, omy=omega_y, phiy=phi_y, degree=4*degree+1)
#
#     # Solve
#     tau_vec[i], err_vec[i] = findTau_autoRange2D(nx, ny, degree, f, u_exact, b_vec[i], b_vec[i], mu, range_tau, tau_order)
#     tau_vec[i], err_vec[i] = findTau_minimization2D(nx, ny, degree, f, u_exact, b_vec[i], b_vec[i], mu, tauTh_vec[i])
#
# plotTau = plt.figure(1)
# plt.plot(b_vec, tau_vec, label='Optimized')
# plt.plot(b_vec, tauTh_vec, label='Theory')
# plt.title('Beta - Tau')
# plt.legend()
#
# plt.show()

# Mu loop ###############################################################################
# mu_vec = np.geomspace(mu_min, mu_max, range_mu, dtype=np.float64)
# tau_vec = np.zeros(range_mu)
# tauTh_vec = np.zeros(range_mu)
# err_vec = np.zeros(range_mu)
# errTh_vec = np.zeros(range_mu)
# Pe_vec = np.zeros(range_mu)
#
# f = Expression('Cx*cos(omx*x[0] + phix) + '
#                'Cy*cos(omy*x[1] + phiy)',
#                Cx=C_x, omx=omega_x, phix=phi_x, Cy=C_y, omy=omega_y, phiy=phi_y, degree=2*degree+1)
#
# for i in range(0, range_mu):
#
#     Pe_vec[i] = b_norm * h / 2 / mu_vec[i]
#
#     # Tau theory definition
#     if b_norm * h > 6 * mu_vec[i]:
#         tauTh_vec[i] = 0.5 * h / b_norm
#     else:
#         tauTh_vec[i] = 0.5 * h / (6 * mu_vec[i] / h)
#
#     tau_order = int(np.floor(np.log10(tauTh_vec[i])))
#
#     # Exact solution
#     def u_px(x):
#       return C_x / (mu_vec[i]*omega_x*omega_x + bx*bx/mu_vec[i]) * ( np.cos(omega_x*x + phi_x) + bx/mu_vec[i]/omega_x * np.sin(omega_x*x + phi_x) )
#     def u_py(y):
#       return C_y / (mu_vec[i]*omega_y*omega_y + by*by/mu_vec[i]) * ( np.cos(omega_y*y + phi_y) + by/mu_vec[i]/omega_y * np.sin(omega_y*y + phi_y) )
#
#     u_exact = Expression('(upx0*(exp(bx*x[0]/mu)-exp(bx/mu)) + (exp(bx*x[0]/mu)-1)*(1-upx1)) /(exp(bx/mu)-1) + Cx / (mu*omx*omx + bx*bx/mu) * ( cos(omx*x[0] + phix) + bx/mu/omx * sin(omx*x[0] + phix) ) +'
#                          '(upy0*(exp(by*x[1]/mu)-exp(by/mu)) + (exp(by*x[1]/mu)-1)*(1-upy1)) /(exp(by/mu)-1) + Cy / (mu*omy*omy + by*by/mu) * ( cos(omy*x[1] + phiy) + by/mu/omy * sin(omy*x[1] + phiy) )',
#                         bx=bx, upx0=Constant(u_px(0)), upx1=Constant(u_px(1)), Cx=C_x, omx=omega_x, phix=phi_x, mu=mu_vec[i],
#                         by=by, upy0=Constant(u_py(0)), upy1=Constant(u_py(1)), Cy=C_y, omy=omega_y, phiy=phi_y, degree=4*degree+1)
#
#     # Solve
#     tau_vec[i], err_vec[i] = findTau_minimization2D(nx, ny, degree, f, u_exact, bx, by, mu_vec[i], tauTh_vec[i])
#
# plotTau = plt.figure(1)
# plt.plot(mu_vec, tau_vec, label='Optimized')
# plt.plot(mu_vec, tauTh_vec, label='Theory')
# plt.xscale('log')
# plt.xlabel('Mu')
# plt.ylabel('Tau')
# plt.title('Compare (varying mu)')
# plt.legend()
#
# plt.show()

# Omega loop ###############################################################################
# om_vec = np.linspace(om_min, om_max, range_om, dtype=np.float64)
# tau_vec = np.zeros(range_om)
# tauTh_vec = np.zeros(range_om)
# err_vec = np.zeros(range_om)
#
#
# for i in range(0, range_om):
#
#     tauTh_vec[i] = tau_th
#
#     # Exact solution
#     f = Expression('Cx*cos(omx*x[0] + phix) + '
#                    'Cy*cos(omy*x[1] + phiy)',
#                    Cx=C_x, omx=om_vec[i], phix=phi_x, Cy=C_y, omy=om_vec[i], phiy=phi_y, degree=2*degree+1)
#
#     def u_px(x):
#       return C_x / (mu*om_vec[i]*om_vec[i] + bx*bx/mu) * ( np.cos(om_vec[i]*x + phi_x) + bx/mu/om_vec[i] * np.sin(om_vec[i]*x + phi_x) )
#     def u_py(y):
#       return C_y / (mu*om_vec[i]*om_vec[i] + by*by/mu) * ( np.cos(om_vec[i]*y + phi_y) + by/mu/om_vec[i] * np.sin(om_vec[i]*y + phi_y) )
#
#     u_exact = Expression('(upx0*(exp(bx*x[0]/mu)-exp(bx/mu)) + (exp(bx*x[0]/mu)-1)*(1-upx1)) /(exp(bx/mu)-1) + Cx / (mu*omx*omx + bx*bx/mu) * ( cos(omx*x[0] + phix) + bx/mu/omx * sin(omx*x[0] + phix) ) +'
#                          '(upy0*(exp(by*x[1]/mu)-exp(by/mu)) + (exp(by*x[1]/mu)-1)*(1-upy1)) /(exp(by/mu)-1) + Cy / (mu*omy*omy + by*by/mu) * ( cos(omy*x[1] + phiy) + by/mu/omy * sin(omy*x[1] + phiy) )',
#                         bx=bx, upx0=Constant(u_px(0)), upx1=Constant(u_px(1)), Cx=C_x, omx=om_vec[i], phix=phi_x, mu=mu,
#                         by=by, upy0=Constant(u_py(0)), upy1=Constant(u_py(1)), Cy=C_y, omy=om_vec[i], phiy=phi_y, degree=4*degree+1)
#
#     # Solve
#     # tau_vec[i], err_vec[i] = findTau_autoRange2D(nx, ny, degree, f, u_exact, bx, by, mu, range_tau, tau_order)
#     tau_vec[i], err_vec[i] = findTau_minimization2D(nx, ny, degree, f, u_exact, bx, by, mu, tau_th)
#
# plotTau = plt.figure(1)
# plt.plot(om_vec, tau_vec, label='Minimizing error')
# plt.plot(om_vec, tauTh_vec, label='Theory')
# plt.xlabel('omega')
# plt.ylabel('tau')
# plt.title('Omega - Tau')
# plt.legend()
#
# plt.show()

# Phi loop ###############################################################################
# phi_vec = np.linspace(phi_min, phi_max, range_phi, dtype=np.float64)
# tau_vec = np.zeros(range_phi)
# tauTh_vec = np.zeros(range_phi)
# err_vec = np.zeros(range_phi)
#
#
# for i in range(0, range_phi):
#
#     phi_x = phi_vec[i]
#     phi_y = phi_vec[i]
#
#     tauTh_vec[i] = tau_th
#
#     # Exact solution
#     f = Expression('Cx*cos(omx*x[0] + phix) + '
#                    'Cy*cos(omy*x[1] + phiy)',
#                    Cx=C_x, omx=omega_x, phix=phi_x, Cy=C_y, omy=omega_y, phiy=phi_y, degree=2*degree+1)
#
#     def u_px(x):
#       return C_x / (mu*omega_x*omega_x + bx*bx/mu) * ( np.cos(omega_x*x + phi_x) + bx/mu/omega_x * np.sin(omega_y*x + phi_x) )
#     def u_py(y):
#       return C_y / (mu*omega_y*omega_y + by*by/mu) * ( np.cos(omega_y*y + phi_y) + by/mu/omega_y * np.sin(omega_y*y + phi_y) )
#
#     u_exact = Expression('(upx0*(exp(bx*x[0]/mu)-exp(bx/mu)) + (exp(bx*x[0]/mu)-1)*(1-upx1)) /(exp(bx/mu)-1) + Cx / (mu*omx*omx + bx*bx/mu) * ( cos(omx*x[0] + phix) + bx/mu/omx * sin(omx*x[0] + phix) ) +'
#                          '(upy0*(exp(by*x[1]/mu)-exp(by/mu)) + (exp(by*x[1]/mu)-1)*(1-upy1)) /(exp(by/mu)-1) + Cy / (mu*omy*omy + by*by/mu) * ( cos(omy*x[1] + phiy) + by/mu/omy * sin(omy*x[1] + phiy) )',
#                         bx=bx, upx0=Constant(u_px(0)), upx1=Constant(u_px(1)), Cx=C_x, omx=omega_x, phix=phi_x, mu=mu,
#                         by=by, upy0=Constant(u_py(0)), upy1=Constant(u_py(1)), Cy=C_y, omy=omega_y, phiy=phi_y, degree=4*degree+1)
#
#     # Solve
#     # tau_vec[i], err_vec[i] = findTau_autoRange2D(nx, ny, degree, f, u_exact, bx, by, mu, range_tau, tau_order)
#     tau_vec[i], err_vec[i] = findTau_minimization2D(nx, ny, degree, f, u_exact, bx, by, mu, tau_th)
#
# plotTau = plt.figure(1)
# plt.plot(phi_vec, tau_vec, label='Minimizing error')
# plt.plot(phi_vec, tauTh_vec, label='Theory')
# plt.xlabel('phi')
# plt.ylabel('tau')
# plt.title('Tau - Phi')
# plt.legend()
#
# plt.show()

###############################################################################
# ERROR PLOT
# IMPORTANT: modify findTau_autoRange() to run this part such that it outputs the whole vector

# tau_star, err_star = findTau_autoRange2D(nx, ny, degree, f, u_exact, bx, by, mu, range_tau, tau_order)
#
#
# plt.plot(tau_star, err_star)
# plt.title('mu = {}, beta = ({}, {}), C = ({}, {}), Pe = {}'.format(mu, bx, by, C_x, C_y, Pe))
# plt.xlabel('Tau')
# plt.ylabel('Error')
#
# plt.show()

###############################################################################
# DEGREE PLOT
# tau_vec = np.zeros(len(degree_vec))
# i = 0
# for degree in degree_vec:
#     tau_vec[i], _ = findTau_minimization2D(nx, ny, degree, f, u_exact, bx, by, mu, tau_th)
#     i += 1
#
# plt.plot(degree_vec, tau_vec, '-o')
# plt.title('mu = {}, beta = ({}, {}), Pe = {:.4}'.format(mu, bx, by, Pe))
# plt.xlabel('degree')
# plt.ylabel('Tau')
#
# plt.show()

###############################################################################
# Pe GLOBAL CONSTANT
Pe_g = 3
b_vec = np.linspace(b_min, b_max, range_b, dtype=np.float64)
tau_vec = np.zeros(range_b)
err_vec = np.zeros(range_b)
i = 0

f = Expression('Cx*cos(omx*x[0] + phix) + '
               'Cy*cos(omy*x[1] + phiy)',
               Cx=C_x, omx=omega_x, phix=phi_x, Cy=C_y, omy=omega_y, phiy=phi_y, degree=2*degree+1)

for b_norm in b_vec:

    bx = b_norm / np.sqrt(2)
    by = b_norm / np.sqrt(2)
    mu = b_norm / 2 / Pe_g

    # Tau theory definition
    if b_norm * h > 6 * mu:
        tau_th = 0.5 * h / b_norm
    else:
        tau_th = 0.5 * h / (6 * mu / h)

    # Exact solution
    def u_px(x):
      return C_x / (mu*omega_x*omega_x + bx*by/mu) * ( np.cos(omega_x*x + phi_x) + bx/mu/omega_x * np.sin(omega_x*x + phi_x) )
    def u_py(y):
      return C_y / (mu*omega_y*omega_y + by*by/mu) * ( np.cos(omega_y*y + phi_y) + by/mu/omega_y * np.sin(omega_y*y + phi_y) )

    u_exact = Expression('(upx0*(exp(bx*x[0]/mu)-exp(bx/mu)) + (exp(bx*x[0]/mu)-1)*(1-upx1)) /(exp(bx/mu)-1) + Cx / (mu*omx*omx + bx*bx/mu) * ( cos(omx*x[0] + phix) + bx/mu/omx * sin(omx*x[0] + phix) ) +'
                         '(upy0*(exp(by*x[1]/mu)-exp(by/mu)) + (exp(by*x[1]/mu)-1)*(1-upy1)) /(exp(by/mu)-1) + Cy / (mu*omy*omy + by*by/mu) * ( cos(omy*x[1] + phiy) + by/mu/omy * sin(omy*x[1] + phiy) )',
                        bx=bx, upx0=Constant(u_px(0)), upx1=Constant(u_px(1)), Cx=C_x, omx=omega_x, phix=phi_x, mu=mu,
                        by=by, upy0=Constant(u_py(0)), upy1=Constant(u_py(1)), Cy=C_y, omy=omega_y, phiy=phi_y, degree=4*degree+1)

    # Solve
    tau_vec[i], err_vec[i] = findTau_minimization2D(nx, ny, degree, f, u_exact, bx, by, mu, tau_th)

    i += 1

plotTau = plt.figure(1)
plt.plot(b_vec, tau_vec)
plt.xlabel("|b|")
plt.ylabel("Tau")
plt.title("Pe_globale = {}, h = {:.4}, degree = {}, bx=by".format(Pe_g, h, degree))
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

plt.show()
