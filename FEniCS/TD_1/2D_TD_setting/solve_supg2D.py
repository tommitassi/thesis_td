# SUPG SOLVER IMPLEMENTATION
# solve_supg2D() solves a transport-diffusion 2D problem with SUPG method
# it returns the created mesh and the approximated solution uh

from fenics import *
import numpy as np

def solve_supg2D(nx, ny, degree, f, u_exact, bx, by, mu_value, tau_K):

  # 1. Mesh generation and problem setup
  mesh = UnitSquareMesh(nx, ny, 'crossed') # potrei passare l'indirizzo della mesh

  mu = Constant(mu_value)
  b = Constant((bx, by))

  # 2. Finite element space
  V = FunctionSpace(mesh, 'CG', degree)

  g_boundary = u_exact

  def boundary(x, on_boundary):
      return on_boundary

  bc = DirichletBC(V, g_boundary, boundary)

  # 3. Matrices and vectors
  u = TrialFunction(V)
  v = TestFunction(V)

  a = (mu * dot(grad(u), grad(v)) + dot(b, grad(u)) * v) * dx
  L = f * v * dx

  if tau_K == None:
      h = CellDiameter(mesh)
      bnorm = sqrt(dot(b, b))
      tau_K = 0.5 * h / conditional(bnorm * h > 6 * mu, bnorm, 6 * mu / h)

  if tau_K:
      ResLHS = -mu * div(grad(u)) + dot(b, grad(u))
      ResRHS = f

      pv_supg = dot(b, grad(v)) + 0.5 * div(b) * v

      a_supg = tau_K * ResLHS * pv_supg * dx
      L_supg = tau_K * ResRHS * pv_supg * dx

      a = a + a_supg
      L = L + L_supg
      
  # 4. Solve
  uh = Function(V)
  solve(a == L, uh, bc)

  return mesh, uh
