###############################################################################
# IMPORT LIBRARIES

from fenics import *
import numpy as np
import matplotlib.pyplot as plt
from solve_supg2D import solve_supg2D
from find_tau2D import find_tau2D

###############################################################################
# PARAMETRS DEFINITION

# Finite elements degree
degree = 1

# Problem parameters
mu = 1.0          # diffusion
bx, by = 150.0, 100.0   # transport

# Cosine forcing parameters
C_x = 10.0           # amplitude in x (0 for homogeneous problem)
C_y = 50.0           # amplitude in y (0 for homogeneous problem)_
omega_x = np.pi     # frequency in x
omega_y = np.pi     # frequency in y
phi_x = 0.0         # phase in x
phi_y = 0.0         # phase in x

# Mesh parameters
# Pe = 1.1          # Pe > 1.0 for unstable numerical methods
nx, ny = 20, 20

# Tau loop parameters
range_tau = 60    # tau-loop max iterations
tau_min = 1e-06   # minimum value of tau
tau_max = 3e-04   # maximum value of tau

# # Beta loop parameters
# range_b = 30      # beta-loop max iterations
# b_min = 50        # minimum value of beta
# b_max = 300       # maximum value of beta

###############################################################################
# PROBLEM SETUP

# Forced
f = Expression('Cx*cos(omx*x[0] + phix) + '
               'Cy*cos(omy*x[1] + phiy)',
               Cx=C_x, omx=omega_x, phix=phi_x, Cy=C_y, omy=omega_y, phiy=phi_y, degree=2*degree+1)

def u_px(x):
  return C_x / (mu*omega_x*omega_x + bx*bx/mu) * ( np.cos(omega_x*x + phi_x) + bx/mu/omega_x * np.sin(omega_x*x + phi_x) )
def u_py(y):
  return C_y / (mu*omega_y*omega_y + by*by/mu) * ( np.cos(omega_y*y + phi_y) + by/mu/omega_y * np.sin(omega_y*y + phi_y) )

u_exact = Expression('(upx0*(exp(bx*x[0]/mu)-exp(bx/mu)) + (exp(bx*x[0]/mu)-1)*(1-upx1)) /(exp(bx/mu)-1) + Cx / (mu*omx*omx + bx*bx/mu) * ( cos(omx*x[0] + phix) + bx/mu/omx * sin(omx*x[0] + phix) ) +'
                     '(upy0*(exp(by*x[1]/mu)-exp(by/mu)) + (exp(by*x[1]/mu)-1)*(1-upy1)) /(exp(by/mu)-1) + Cy / (mu*omy*omy + by*by/mu) * ( cos(omy*x[1] + phiy) + by/mu/omy * sin(omy*x[1] + phiy) )',
                    bx=bx, upx0=Constant(u_px(0)), upx1=Constant(u_px(1)), Cx=C_x, omx=omega_x, phix=phi_x, mu=mu,
                    by=by, upy0=Constant(u_py(0)), upy1=Constant(u_py(1)), Cy=C_y, omy=omega_y, phiy=phi_y, degree=4*degree+1)

# Homogeneous
# f = Constant(0)
# u_exact = Expression('(exp(bx * (x[0] - 1) / mu) - exp(-bx / mu)) / (1 - exp(-bx / mu)) + '
#                      '(exp(by * (x[1] - 1) / mu) - exp(-by / mu)) / (1 - exp(-by / mu))',
#                      mu=mu, bx=bx, by=by, degree=2*degree+1)

###############################################################################
# MAIN

mesh, u = solve_supg2D(nx, ny, degree, f, u_exact, bx, by, mu, None)
# TAU, ERR = find_tau2D(nx, ny, degree, f, u_exact, bx, by, mu, range_tau, tau_min, tau_max)
# print(TAU)

h = mesh.hmax()
b_norm = sqrt(bx*bx + by*by)
# Pe = 1.1
Pe = b_norm * h / 2 / mu
# mu_new = sqrt(bx*bx + by*by) * h / 2 / Pe
if b_norm * h > 6 * mu:
    tau_th = 0.5 * h / b_norm
else:
    tau_th = 0.5 * h * h / 6 / mu
tau_order = int(np.floor(np.log10(tau_th)))
# print(tau_th)
#
# print('Error H1: {}'.format(errornorm(u_exact, u, 'H10')))
# print('Error L2: {}\n'.format(errornorm(u_exact, u, 'L2')))
# print('Peclet: {}'.format(Pe))

V = FunctionSpace(mesh, 'CG', 4*degree)
g_boundary = u_exact
def boundary(x, on_boundary):
  return on_boundary
bc = DirichletBC(V, g_boundary, boundary)

plotNum = plt.figure(1)
plot(u)
plt.title('Numerical')

plotExact = plt.figure(2)
plot(project(u_exact, V=V, bcs=bc, mesh=mesh))
plt.title('Exact')

plt.show()
