# REFINE MESH
# use different mesh to see how the residual behaves

###############################################################################
# IMPORT LIBRARIES
import numpy as np
import matplotlib.pyplot as plt
from fenics import *

from solveSupg2D_res import solveSupg2D_res
from findTau2D_res import findTau2D_res

###############################################################################
# PARAMETRS DEFINITION

# Finite elements degree
# degree = CHANGED
degree_vec = [1, 2, 3, 4]

# Problem parameters
mu = 1          # diffusion
bx, by = 350, 350   # transport

# Cosine forcing parameters
C = 1.0           # amplitude (0 for homogeneous problem)_
omega = np.pi     # frequency
phi = 0.0         # phase

# Mesh parameters
# Pe = 4          # Pe > 1.0 for unstable numerical methods
nx, ny = 20, 20

# Tau loop parameters
tau_min = 0
tau_max = None     # None to set tau_max = tau_th
range_tau = 40    # tau-loop max iterations

###############################################################################
# PROBLEM SETUP
# Geometry setup
mesh = UnitSquareMesh(nx, ny, 'crossed')
h = mesh.hmax()

b_norm = sqrt(bx*bx + by*by)
Pe = b_norm * h / 2 / mu

if b_norm * h > 6 * mu:
    tau_th = 0.5 * h / b_norm
else:
    tau_th = 0.5 * h * h / 6 / mu

if tau_max is None:
    tau_max = tau_th
tau_star = np.linspace(tau_min, tau_max, range_tau)

###############################################################################
# LOOPING SETUP
index = 0
tau_vec = np.zeros(len(degree_vec))
res_vec = np.zeros(len(degree_vec))
tauDis_vec = np.zeros(len(degree_vec))

res_star = np.zeros(range_tau)

for degree in degree_vec:

    # Problem setup
    f = Expression('C*cos(om*x[0] + phi) + '
                   'C*cos(om*x[1] + phi)',
                   C=C, om=omega, phi=phi, degree=2*degree+1)

    def u_px(x):
      return C / (mu*omega*omega + bx*bx/mu) * ( np.cos(omega*x + phi) + bx/mu/omega * np.sin(omega*x + phi) )
    def u_py(y):
      return C / (mu*omega*omega + by*by/mu) * ( np.cos(omega*y + phi) + by/mu/omega * np.sin(omega*y + phi) )

    u_exact = Expression('(upx0*(exp(bx*x[0]/mu)-exp(bx/mu)) + (exp(bx*x[0]/mu)-1)*(1-upx1)) /(exp(bx/mu)-1) + C / (mu*om*om + bx*bx/mu) * ( cos(om*x[0] + phi) + bx/mu/om * sin(om*x[0] + phi) ) +'
                         '(upy0*(exp(by*x[1]/mu)-exp(by/mu)) + (exp(by*x[1]/mu)-1)*(1-upy1)) /(exp(by/mu)-1) + C / (mu*om*om + by*by/mu) * ( cos(om*x[1] + phi) + by/mu/om * sin(om*x[1] + phi) )',
                        bx=bx, upx0=Constant(u_px(0)), upx1=Constant(u_px(1)), C=C, om=omega, phi=phi, mu=mu,
                        by=by, upy0=Constant(u_py(0)), upy1=Constant(u_py(1)), degree=4*degree+1)

    # Compute tau and residual
    tau_vec[index], res_vec[index] = findTau2D_res(mesh, degree, f, u_exact, bx, by, mu, tau_th)

    # Find discrete minimum
    index_tau = 0
    for tau_i in tau_star:
        _, res_star[index_tau] = solveSupg2D_res(mesh, degree, f, u_exact, bx, by, mu, tau_i)
        index_tau += 1
    idx_min = np.where(res_star == np.amin(res_star))
    tauDis_vec[index] = tau_star[idx_min[0][0]]

    # Increment index
    index += 1
    print("Completed degree = {}".format(degree))

###############################################################################
# PLOTS
plt.figure(1)
plt.plot(degree_vec, tau_vec, label='Optimization')
plt.plot(degree_vec, tauDis_vec, label='Discrete')
plt.xlabel('degree')
plt.ylabel('Tau')
plt.legend()
plt.title('Pe = {}'.format(Pe))

# plt.figure(2)
# plt.plot(degree_vec, res_vec, label='Residual')
# plt.plot(degree_vec, resTh_vec, label='Theory')
# plt.xlabel('nx = ny')
# plt.ylabel('Residual')
# plt.legend()
# plt.title('Pe = {}'.format(Pe))

plt.show()
