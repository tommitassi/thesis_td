# COMPUTE RESIDUAL FUNCTION
# computeResidual() computes the residual for solution uh of the 2D TD problem
# it returns the value of the residual

from fenics import *

def computeResidual(uh, mesh, degree, f, bx, by, mu_value):

    mu = Constant(mu_value)
    b = Constant((bx, by))

    V = FunctionSpace(mesh, 'CG', degree)


    ###########################################################################
    # STRONG FORM
    # div(grad(uh)) è sempre ZERO se gli elementi finiti hanno grado UNO !!!
    # la derivata seconda è sempre nulla! calcola uh con degree>=2

    # if degree == 1: # WATCH OUT: if the passed degree is different from the
    #                 # one used to compute uh, this implementation does not work
    #                 # however, degree should be the same for the right norm
    #     # Method 2
    #     VV = VectorFunctionSpace(mesh, 'CG', degree)
    #     rr = project(grad(uh), VV)
    #     rx, ry = rr.split()
    #     r = project(- mu * (rx.dx(0) + ry.dx(1)) + dot(b, grad(uh)) - f, V)
    #
    # else:
    #     # Method 1
    #     rr = - mu * div(grad(uh)) + dot(b, grad(uh)) - f
    #     r = project(rr, V)
    #
    # R = norm(r, 'L2', mesh=mesh)

    ## Another way (WHATCH OUT: f = 0)
    degree_rise = 3
    V_rise = FunctionSpace(mesh, 'CG', degree+degree_rise)
    rr = project(- mu * div(grad(uh)) + dot(b, grad(uh)), V_rise)
    R = sqrt( assemble(rr**2 * dx(mesh)) )
    ## exactly the same as
    # r = project(- mu * div(grad(uh)) + dot(b, grad(uh)), V_rise)
    # R = errornorm(r, Constant(0), 'L2', mesh=mesh)

    ###########################################################################
    # WEAK FORM
    # requires u_exact as an additional input

    # g_boundary = u_exact
    #
    # def boundary(x, on_boundary):
    #     return on_boundary
    #
    # bc = DirichletBC(V, g_boundary, boundary)
    #
    # u = TrialFunction(V)
    # v = TestFunction(V)
    #
    # def a(u, v):
    #     return (mu * dot(grad(u), grad(v)) + dot(b, grad(u)) * v) * dx
    #
    # def L(v):
    #     return f * v * dx
    #
    # A = assemble(a(u,v))
    # LL = assemble(L(v))
    # bc.apply(A)
    # bc.apply(LL)
    #
    # R = residual(A, uh.vector(), LL)

    return R
