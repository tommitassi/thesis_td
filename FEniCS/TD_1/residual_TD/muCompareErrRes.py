# COMPARE ERROR AND RESIDUAL

###############################################################################
# IMPORT LIBRARIES

import numpy as np
import matplotlib.pyplot as plt
from fenics import *

import sys
sys.path.insert(1, './../2D_TD_setting')
from find_tau2D import findTau_minimization2D

from findTau2D_res import findTau2D_res

###############################################################################
# PARAMETRS DEFINITION

# Finite elements degree
degree = 1

# Problem parameters
# mu = LOOPED         # diffusion
bx, by = 1, 1   # transport

# Cosine forcing parameters
C = 1.0           # amplitude (0 for homogeneous problem)_
omega = np.pi     # frequency
phi = 0.0         # phase

# Mesh parameters
nx, ny = 20, 20

# Mu loop parameters
range_mu = 20      # beta-loop max iterations
mu_min = 1.6e-3        # minimum value of beta
mu_max = 0.3      # maximum value of beta

###############################################################################
# LOOPING SETUP

mu_vec = np.geomspace(mu_min, mu_max, range_mu)

# err_vec = np.zeros(range_mu)
res_vec = np.zeros(range_mu)
th_vec = np.zeros(range_mu)

index = 0

f = Expression('C*cos(om*x[0] + phi) + '
               'C*cos(om*x[1] + phi)',
               C=C, om=omega, phi=phi, degree=2*degree+1)

mesh = UnitSquareMesh(nx, ny, 'crossed')
h = mesh.hmax()
b_norm = sqrt(bx*bx + by*by)

for mu in mu_vec:

    # Exact solution
    def u_px(x):
      return C / (mu*omega*omega + bx*bx/mu) * ( np.cos(omega*x + phi) + bx/mu/omega * np.sin(omega*x + phi) )
    def u_py(y):
      return C / (mu*omega*omega + by*by/mu) * ( np.cos(omega*y + phi) + by/mu/omega * np.sin(omega*y + phi) )

    u_exact = Expression('(upx0*(exp(bx*x[0]/mu)-exp(bx/mu)) + (exp(bx*x[0]/mu)-1)*(1-upx1)) /(exp(bx/mu)-1) + C / (mu*om*om + bx*bx/mu) * ( cos(om*x[0] + phi) + bx/mu/om * sin(om*x[0] + phi) ) +'
                         '(upy0*(exp(by*x[1]/mu)-exp(by/mu)) + (exp(by*x[1]/mu)-1)*(1-upy1)) /(exp(by/mu)-1) + C / (mu*om*om + by*by/mu) * ( cos(om*x[1] + phi) + by/mu/om * sin(om*x[1] + phi) )',
                        bx=bx, upx0=Constant(u_px(0)), upx1=Constant(u_px(1)), C=C, om=omega, phi=phi, mu=mu,
                        by=by, upy0=Constant(u_py(0)), upy1=Constant(u_py(1)), degree=4*degree+1)

    # Geometry setup
    Pe = b_norm * h / 2 / mu

    if b_norm * h > 6 * mu:
        tau_th = 0.5 * h / b_norm
    else:
        tau_th = 0.5 * h * h / 6 / mu

    ##########################################################################
    # COMPUTE TAU - MINIMIZING ERROR
    # err_vec[index], _ = findTau_minimization2D(nx, ny, degree, f, u_exact, bx, by, mu, tau_th)

    ##########################################################################
    # COMPUTE TAU - MINIMIZING RESIDUAL
    res_vec[index] = findTau2D_res(mesh, degree, f, u_exact, bx, by, mu, tau_th)

    th_vec[index] = tau_th
    index += 1

###############################################################################
# LOAD ERROR MINIMIZATION
fname = "./../datasetCreation2D_TD/dataset1.csv"
all_features = []
all_targets = []
with open(fname) as f:
    for i, line in enumerate(f):
        if i == 0:
            continue  # Skip header
        fields = line.strip().split(",")
        all_features.append([float(v.replace('"', "")) for v in fields[:1]])
        all_targets.append([float(fields[-1].replace('"', ""))])
mu_data = np.array(all_features, dtype="float32")
tau_data = np.array(all_targets, dtype="float32")

###############################################################################
# PLOTS
plt.figure(1)
# plt.plot(mu_vec, err_vec, label='Error')
plt.plot(mu_data, tau_data, label='Error')
plt.plot(mu_vec, res_vec, label='Residual')
# plt.plot(mu_vec, th_vec, label='Theory')
plt.xscale("log")
plt.title('Error vs Residual')
plt.xlabel('Mu')
plt.ylabel('Tau')
plt.legend()

plt.figure(2)
plt.plot(mu_data, tau_data, label='Error')
plt.plot(mu_vec, res_vec, label='Residual')
plt.plot(mu_vec, th_vec, label='Theory')
plt.xscale("log")
plt.title('Error vs Residual')
plt.xlabel('Mu')
plt.ylabel('Tau')
plt.legend()

plt.show()
