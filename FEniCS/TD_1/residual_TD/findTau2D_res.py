# FIND-TAU FUNCTION
# findTau2D_res() finds tau that minimizes the residual
# it returns the value of tau

from fenics import *
import scipy.optimize as opt
from noisyopt import minimizeCompass
from solveSupg2D_res import solveSupg2D_res

def findTau2D_res(mesh, degree, f, u_exact, bx, by, mu, tau_max):

    def error_tauFunc(x):
        _, R = solveSupg2D_res(mesh, degree, f, u_exact, bx, by, mu, x)
        return R

    result = opt.minimize_scalar(error_tauFunc, bracket=(0, tau_max))
    # result = opt.minimize_scalar(error_tauFunc, bounds=(0, tau_max), method='bounded')
    # options={'xatol': 1e-7, 'maxiter': 500, 'disp': 0}

    return result.x, result.fun

def findTau2D_resNoisy(mesh, degree, f, u_exact, bx, by, mu, tau_max, tau_init):

    def error_tauFunc(X):
        x = X[0]
        _, R = solveSupg2D_res(mesh, degree, f, u_exact, bx, by, mu, x)
        return R

    result = minimizeCompass(error_tauFunc, bounds=[0, tau_max], x0=[tau_init], paired=False)

    return result.x[0], result.fun
