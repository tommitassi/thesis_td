# FILTER DATA
# filterSignal() applies a filter on input data based on its median
# it outputs the filtered array of data and the indexes of the found outliers
# of the input array

import numpy as np

def filterSignal(signal, threshold=3):

    difference = np.abs(signal - np.median(signal))
    median_difference = np.median(difference)
    if median_difference == 0:
        s = 0
    else:
        s = difference / float(median_difference)

    outliers_idx = np.where(s > threshold)[0]
    filtered = np.delete(signal, outliers_idx)

    return filtered, outliers_idx


# filterSignal_batch() applies a filter on input data based on the median of
# batches of these data
# it outputs the filtered array of data and the indexes of the found outliers
# of the input array

def filterSignal_batch(signal, threshold=3, n_batches=2):

    outliers_idx = np.array([], dtype=int)

    batch_size = len(signal) // n_batches

    for i in range(n_batches):

        difference = np.abs(signal[i*batch_size:(i+1)*batch_size-1] - np.median(signal[i*batch_size:(i+1)*batch_size-1]))
        median_difference = np.median(difference)
        if median_difference == 0:
            s = 0
        else:
            s = difference / float(median_difference)

        outliers_idx = np.append(outliers_idx, i*batch_size + np.where(s > threshold)[0])

    filtered = np.delete(signal, outliers_idx)

    return filtered, outliers_idx
