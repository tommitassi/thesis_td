# COMPARE ERROR AND RESIDUAL

###############################################################################
# IMPORT LIBRARIES

import numpy as np
import matplotlib.pyplot as plt
from fenics import *

import sys
sys.path.insert(1, './../2D_TD_setting')
from find_tau2D import findTau_minimization2D

from findTau2D_res import findTau2D_res

###############################################################################
# PARAMETRS DEFINITION

# Finite elements degree
degree = 1

# Problem parameters
mu = 1         # diffusion
# bx, by = LOOPED   # transport

# Cosine forcing parameters
C = 1.0           # amplitude (0 for homogeneous problem)_
omega = np.pi     # frequency
phi = 0.0         # phase

# Mesh parameters
nx, ny = 20, 20

# Mu loop parameters
range_b = 20      # beta-loop max iterations
b_min = 200        # minimum value of beta
b_max = 700      # maximum value of beta

###############################################################################
# LOOPING SETUP

b_vec = np.linspace(b_min, b_max, range_b)

err_vec = np.zeros(range_b)
res_vec = np.zeros(range_b)
th_vec = np.zeros(range_b)

index = 0

f = Expression('C*cos(om*x[0] + phi) + '
               'C*cos(om*x[1] + phi)',
               C=C, om=omega, phi=phi, degree=2*degree+1)

mesh = UnitSquareMesh(nx, ny, 'crossed')
h = mesh.hmax()

for b in b_vec:
    bx = b
    by = b

    # Exact solution
    def u_px(x):
      return C / (mu*omega*omega + bx*bx/mu) * ( np.cos(omega*x + phi) + bx/mu/omega * np.sin(omega*x + phi) )
    def u_py(y):
      return C / (mu*omega*omega + by*by/mu) * ( np.cos(omega*y + phi) + by/mu/omega * np.sin(omega*y + phi) )

    u_exact = Expression('(upx0*(exp(bx*x[0]/mu)-exp(bx/mu)) + (exp(bx*x[0]/mu)-1)*(1-upx1)) /(exp(bx/mu)-1) + C / (mu*om*om + bx*bx/mu) * ( cos(om*x[0] + phi) + bx/mu/om * sin(om*x[0] + phi) ) +'
                         '(upy0*(exp(by*x[1]/mu)-exp(by/mu)) + (exp(by*x[1]/mu)-1)*(1-upy1)) /(exp(by/mu)-1) + C / (mu*om*om + by*by/mu) * ( cos(om*x[1] + phi) + by/mu/om * sin(om*x[1] + phi) )',
                        bx=bx, upx0=Constant(u_px(0)), upx1=Constant(u_px(1)), C=C, om=omega, phi=phi, mu=mu,
                        by=by, upy0=Constant(u_py(0)), upy1=Constant(u_py(1)), degree=4*degree+1)

    # Geometry setup
    b_norm = sqrt(bx*bx + by*by)
    Pe = b_norm * h / 2 / mu

    if b_norm * h > 6 * mu:
        tau_th = 0.5 * h / b_norm
    else:
        tau_th = 0.5 * h * h / 6 / mu

    ##########################################################################
    # COMPUTE TAU - MINIMIZING ERROR
    err_vec[index], _ = findTau_minimization2D(nx, ny, degree, f, u_exact, bx, by, mu, tau_th)

    ##########################################################################
    # COMPUTE TAU - MINIMIZING RESIDUAL
    res_vec[index], _ = findTau2D_res(mesh, degree, f, u_exact, bx, by, mu, tau_th)

    th_vec[index] = tau_th
    index += 1

###############################################################################
# PLOTS
plt.figure(1)
plt.plot(b_vec, err_vec, label='Error')
plt.plot(b_vec, res_vec, label='Residual')
plt.xscale("log")
plt.title('Error vs Residual')
plt.xlabel('Beta')
plt.ylabel('Tau')
plt.legend()

plt.figure(2)
plt.plot(b_vec, err_vec, label='Error')
plt.plot(b_vec, res_vec, label='Residual')
plt.plot(b_vec, th_vec, label='Theory')
plt.xscale("log")
plt.title('Error vs Residual')
plt.xlabel('Beta')
plt.ylabel('Tau')
plt.legend()

plt.show()
