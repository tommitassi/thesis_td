###############################################################################
# IMPORT LIBRARIES
import numpy as np
import matplotlib.pyplot as plt
from fenics import *

from solveSupg2D_res import solveSupg2D_res
from computeResidual import computeResidual

###############################################################################
# PARAMETRS DEFINITION

# Finite elements degree
degree = 1

# Problem parameters
mu = 1          # diffusion
bx, by = 200, 200  # transport

# Cosine forcing parameters
C = 0.0           # amplitude (0 for homogeneous problem)_
omega = np.pi     # frequency
phi = 0.0         # phase

# Mesh parameters
nx, ny = 40, 40

###############################################################################
# PROBLEM SETUP
# f = Expression('C*cos(om*x[0] + phi) + '
#                'C*cos(om*x[1] + phi)',
#                C=C, om=omega, phi=phi, degree=2*degree+1)

f = Expression('C*cos(om*x[0] + phi) + '
               'C*cos(om*x[1] + phi)',
               C=C, om=omega, phi=phi, degree=2*degree+1)

def u_px(x):
  return C / (mu*omega*omega + bx*bx/mu) * ( np.cos(omega*x + phi) + bx/mu/omega * np.sin(omega*x + phi) )
def u_py(y):
  return C / (mu*omega*omega + by*by/mu) * ( np.cos(omega*y + phi) + by/mu/omega * np.sin(omega*y + phi) )

u_exact = Expression('(upx0*(exp(bx*x[0]/mu)-exp(bx/mu)) + (exp(bx*x[0]/mu)-1)*(1-upx1)) /(exp(bx/mu)-1) + C / (mu*om*om + bx*bx/mu) * ( cos(om*x[0] + phi) + bx/mu/om * sin(om*x[0] + phi) ) +'
                     '(upy0*(exp(by*x[1]/mu)-exp(by/mu)) + (exp(by*x[1]/mu)-1)*(1-upy1)) /(exp(by/mu)-1) + C / (mu*om*om + by*by/mu) * ( cos(om*x[1] + phi) + by/mu/om * sin(om*x[1] + phi) )',
                    bx=bx, upx0=Constant(u_px(0)), upx1=Constant(u_px(1)), C=C, om=omega, phi=phi, mu=mu,
                    by=by, upy0=Constant(u_py(0)), upy1=Constant(u_py(1)), degree=4*degree+1)

# Geometry setup
mesh = UnitSquareMesh(nx, ny, 'crossed')
h = mesh.hmax()
b_norm = sqrt(bx*bx + by*by)

Pe = b_norm * h / 2 / mu

# if b_norm * h > 6 * mu:
#     tau_th = 0.5 * h / b_norm
# else:
#     tau_th = 0.5 * h * h / 6 / mu

def upwind_function(x):
    return 1 / np.tanh(x) - 1 / x

def tau_th(h, b_norm, Pe_h, r=1):
    return h / 2 / b_norm / r * upwind_function(Pe_h/r)

tau_th = tau_th(h, b_norm, Pe, degree)

###############################################################################
# EXACT SOLUTION
V = FunctionSpace(mesh, 'CG', degree)
g_boundary = u_exact
def boundary(x, on_boundary):
    return on_boundary
bc = DirichletBC(V, g_boundary, boundary)
u_ex_proj = project(u_exact, V=V, bcs=bc, mesh=mesh)

###############################################################################
# PROBLEM SOLUTION
uh, resWeak = solveSupg2D_res(mesh, degree, f, u_exact, bx, by, mu, tau_th)

resStrong = computeResidual(uh, mesh, degree, f, bx, by, mu) #si comporta bene anche se qui uso degree=1
print("Strong residual: ", resStrong)
# print("Weak residual: ", resWeak)
print("L2 error: ", errornorm(u_exact, uh, 'L2') / norm(u_ex_proj, 'L2'))
print("H1 error: ", errornorm(u_exact, uh, 'H1') / norm(u_ex_proj, 'H1'))

###############################################################################
# PLOTS

# plt.figure(1)
# c = plot(uh, mode='color')
# plt.colorbar(c)
#
# plt.figure(2)

# c2 = plot(u_ex_proj, mode='color')
# plt.colorbar(c2)
#
# plt.figure(3)
# c3 = plot(uh - u_ex_proj, mode='color')
# plt.colorbar(c3)

# plt.show()



###############################################################################
