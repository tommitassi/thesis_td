# MAKE PLOTS RESIDUAL
# plots of the residual over tau values

###############################################################################
# IMPORT LIBRARIES

import numpy as np
import matplotlib.pyplot as plt
from fenics import *
from scipy import interpolate

import sys
sys.path.insert(1, './../2D_TD_setting')
from find_tau2D import findTau_autoRange2D, find_tau2D, findTau_minimization2D

from solveSupg2D_res import solveSupg2D_res
from findTau2D_res import findTau2D_res, findTau2D_resNoisy
from filterSignal import filterSignal, filterSignal_batch

###############################################################################
# PARAMETRS DEFINITION

# Finite elements degree
degree = 2

# Problem parameters
mu = 1          # diffusion
bx, by = 200, 200   # transport

# Cosine forcing parameters
C = 0.0           # amplitude (0 for homogeneous problem)_
omega = np.pi     # frequency
phi = 0.0         # phase

# Mesh parameters
# Pe = 1.1          # Pe > 1.0 for unstable numerical methods
nx, ny = 40, 40

# Tau loop parameters
tau_min = 0
tau_max = 5e-3     # None to set tau_max = tau_th
range_tau = 40    # tau-loop max iterations

###############################################################################
# PROBLEM SETUP

# Forced expressions
f = Expression('C*cos(om*x[0] + phi) + '
               'C*cos(om*x[1] + phi)',
               C=C, om=omega, phi=phi, degree=2*degree+1)

def u_px(x):
  return C / (mu*omega*omega + bx*bx/mu) * ( np.cos(omega*x + phi) + bx/mu/omega * np.sin(omega*x + phi) )
def u_py(y):
  return C / (mu*omega*omega + by*by/mu) * ( np.cos(omega*y + phi) + by/mu/omega * np.sin(omega*y + phi) )

u_exact = Expression('(upx0*(exp(bx*x[0]/mu)-exp(bx/mu)) + (exp(bx*x[0]/mu)-1)*(1-upx1)) /(exp(bx/mu)-1) + C / (mu*om*om + bx*bx/mu) * ( cos(om*x[0] + phi) + bx/mu/om * sin(om*x[0] + phi) ) +'
                     '(upy0*(exp(by*x[1]/mu)-exp(by/mu)) + (exp(by*x[1]/mu)-1)*(1-upy1)) /(exp(by/mu)-1) + C / (mu*om*om + by*by/mu) * ( cos(om*x[1] + phi) + by/mu/om * sin(om*x[1] + phi) )',
                    bx=bx, upx0=Constant(u_px(0)), upx1=Constant(u_px(1)), C=C, om=omega, phi=phi, mu=mu,
                    by=by, upy0=Constant(u_py(0)), upy1=Constant(u_py(1)), degree=4*degree+1)

# Geometry setup
mesh = UnitSquareMesh(nx, ny, 'crossed')
h = mesh.hmax()
b_norm = sqrt(bx*bx + by*by)

Pe = b_norm * h / 2 / mu

if b_norm * h > 6 * mu:
    tau_th = 0.5 * h / b_norm
else:
    tau_th = 0.5 * h * h / 6 / mu
tau_order = int(np.floor(np.log10(tau_th)))

if tau_max is None:
    tau_max = tau_th

###############################################################################
# COMPUTE EXACT SOLUTION ERROR
## WATCH OUT: THESE ONES DOESNT USE solveSupg2D_res()
# tau_star1, err_star = findTau_autoRange2D(nx, ny, degree, f, u_exact, bx, by, mu, range_tau//2, tau_order)
# tau_star1, err_star = find_tau2D(nx, ny, degree, f, u_exact, bx, by, mu, range_tau//2, tau_min, tau_max)
tau_error, err_error = findTau_minimization2D(nx, ny, degree, f, u_exact, bx, by, mu, tau_th)

###############################################################################
# COMPUTE RESIDUALS
tau_star = np.linspace(tau_min, tau_max, range_tau)

res_star = np.zeros(np.size(tau_star))
index = 0

for tau_i in tau_star:

    _, res_star[index] = solveSupg2D_res(mesh, degree, f, u_exact, bx, by, mu, tau_i)
    index += 1

###############################################################################
# FILTER DATA
# res_filtered, outliers_idx = filterSignal_batch(res_star, threshold=7, n_batches=1)
# tau_filtered = np.delete(tau_star, outliers_idx)

###############################################################################
# INTERPOLATION
# tck = interpolate.splrep(tau_filtered, res_filtered, s=1)
# tau_interp = np.linspace(tau_min, tau_max, range_tau*10)
# f_spline = interpolate.splev(tau_interp, tck)

###############################################################################
# FIND MINIMUM
uh_theory, res_theory = solveSupg2D_res(mesh, degree, f, u_exact, bx, by, mu, tau_th)
err_theory = errornorm(u_exact, uh_theory, 'H10')

tau_found, res_found = findTau2D_res(mesh, degree, f, u_exact, bx, by, mu, tau_th)
uh_found, _ = solveSupg2D_res(mesh, degree, f, u_exact, bx, by, mu, tau_found)
err_found = errornorm(u_exact, uh_found, 'H10')

# idx1 = np.where(err_star == np.amin(err_star))
# _, res1 = solveSupg2D_res(mesh, degree, f, u_exact, bx, by, mu, tau_star1[idx1[0][0]])
_, res_error = solveSupg2D_res(mesh, degree, f, u_exact, bx, by, mu, tau_error)

idx2 = np.where(res_star == np.amin(res_star))
uh2, res2 = solveSupg2D_res(mesh, degree, f, u_exact, bx, by, mu, tau_star[idx2[0][0]])
err2 = errornorm(u_exact, uh2, 'H10')

# idxInterp = np.where(f_spline == np.amin(f_spline))
# tau_foundInterp = tau_interp[idxInterp[0][0]]
# _, res_interp = solveSupg2D_res(mesh, degree, f, u_exact, bx, by, mu, tau_foundInterp)

###############################################################################
# PRINT RESULTS
print("Theory                - tau: {:.4e} with residual: {:.5}; error: {:.5}".format(tau_th, res_theory, err_theory))

print("Error found           - tau: {:.4e} with residual: {:.5}; error: {:.5}".format(tau_error, res_error, err_error))

print("Residual min in range - tau: {:.4e} with residual: {:.5}; error: {:.5}".format(tau_star[idx2[0][0]], res2, err2))

print("Residual found        - tau: {:.4e} with residual: {:.5}; error: {:.5}".format(tau_found, res_found, err_found))
# print("Found noisy - tau: {} with residual: {}".format(tau_found2, res_found2))

# print("Interp. -  tau: {} with residual: {}".format(tau_foundInterp, res_interp))

###############################################################################
# PLOTS
# plt.figure(1)
# plt.plot(tau_star1, err_star)
# plt.title('mu = {}, beta = ({}, {}), Pe = {:.3}'.format(mu, bx, by, Pe))
# plt.xlabel('Tau')
# plt.ylabel('Error')

# plt.figure(2)
# plt.plot(tau_filtered, res_filtered, '.', label='Real values')
# plt.plot(tau_star[outliers_idx], res_star[outliers_idx], '.y', label='Outliers')
# plt.plot(tau_interp, f_spline, label='Interpolation')
# plt.plot(tau_foundInterp, res_interp, '.r', markersize=10, label='Interp. minimum')
# plt.xlabel('Tau')
# plt.ylabel('Residual')
# # plt.title('mu = {}, beta = ({}, {}), Pe = {:.3}, n = {}'.format(mu, bx, by, Pe))
# plt.title('n = {}x{}, h = {:.4}, Pe = {:.3}'.format(nx, ny, h, Pe))
# plt.legend()

plt.figure(3)
plt.plot(tau_star, res_star)
plt.plot(tau_found, res_found, '.', label='Residual Minimum')
plt.plot(tau_error, res_error, '.', label='Error minimum')
plt.plot(tau_th, res_theory, '.', label='Theory')
plt.xlabel('Tau')
plt.ylabel('Residual')
plt.legend()
plt.title('mu = {}, beta = ({}, {}), Pe = {:.3}'.format(mu, bx, by, Pe))

plt.show()
