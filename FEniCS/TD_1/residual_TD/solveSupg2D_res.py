# SUPG SOLVER IMPLEMENTATION
# solveSupg2D_res() solves a transport-diffusion 2D problem with SUPG method
# it returns the numerical solution uh and the residual

from fenics import *
import numpy as np

from computeResidual import computeResidual

def solveSupg2D_res(mesh, degree, f, u_exact, bx, by, mu_value, tau_K):

  # 1. Problem setting
  mu = Constant(mu_value)
  b = Constant((bx, by))

  # 2. Finite element space
  V = FunctionSpace(mesh, 'CG', degree)

  g_boundary = u_exact

  def boundary(x, on_boundary):
      return on_boundary

  bc = DirichletBC(V, g_boundary, boundary)

  # 3. Matrices and vectors
  u = TrialFunction(V)
  v = TestFunction(V)

  a = (mu * dot(grad(u), grad(v)) + dot(b, grad(u)) * v) * dx
  L = f * v * dx

  # AA = assemble(a) # non-stabilized weak form

  # SUPG stabilization
  if tau_K:
      ResLHS = -mu * div(grad(u)) + dot(b, grad(u))
      ResRHS = f

      pv_supg = dot(b, grad(v)) + 0.5 * div(b) * v

      a_supg = tau_K * ResLHS * pv_supg * dx
      L_supg = tau_K * ResRHS * pv_supg * dx

      a = a + a_supg
      L = L + L_supg

  # 4. Solve
  uh = Function(V)
  solve(a == L, uh, bc)

  # AA = assemble(a) # stabilized weak form
  # LL = assemble(L)
  # bc.apply(AA, LL)
  # res = residual(AA, uh.vector(), LL)

  # potrei mettere un flag per calcolare o meno res
  res = computeResidual(uh, mesh, degree, f, bx, by, mu_value) # strong form

  return uh, res
