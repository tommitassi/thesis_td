# FIND-TAU FUNCTION
# findTau() finds tau that minimizes the error in H1_0 norm
# it returns the value of tau and its error

from fenics import *
import numpy as np
import scipy.optimize as opt
from solve_supg import solveSupg, solveSupg_uOnly

def findTau(n, degree, f, u_exact, b, mu, range_tau, tau_min, tau_max):
    tau_vec = [0]
    tau_vec = np.append(tau_vec, np.geomspace(tau_min, tau_max, range_tau)) # evenly spaced on a log-scale

    errH10 = np.empty(range_tau + 1) # initialize empty array

    for i in range(0, range_tau + 1): # add 1 to count for tau=0

        _, u = solveSupg(n, degree, f, u_exact, b, mu, tau_vec[i])
        errH10[i] = errornorm(u_exact, u, 'H10')

    idx = np.where(errH10 == np.amin(errH10))

    return tau_vec[idx[0][0]], errH10[idx[0][0]]


# FIND-TAU FUNCTION 2
# findTau_autoRange() finds tau that minimizes the error in H1_0 norm, using a range computed around tau_order
# it returns the value of tau and its error

def findTau_autoRange(n, degree, f, u_exact, b, mu, range_tau, tau_order):

    tau_min = 0.8 * 10 ** (tau_order)
    tau_med = 10 ** (tau_order)
    tau_max = 10 ** (tau_order+1)

    tau_vec = [0]
    tau_vec = np.append(tau_vec, np.geomspace(tau_min, tau_med, range_tau//10 + 1) )
    tau_vec = np.append(tau_vec, np.linspace(tau_med, tau_max, range_tau - range_tau//10)[1:] )

    errH10 = np.empty(range_tau + 1) # initialize empty array

    for i in range(0, range_tau + 1): # add 1 to count for tau=0
        _, u = solveSupg(n, degree, f, u_exact, b, mu, tau_vec[i])
        errH10[i] = errornorm(u_exact, u, 'H10')

    idx = np.where(errH10 == np.amin(errH10))

    return tau_vec[idx[0][0]], errH10[idx[0][0]]


# FIND-TAU FUNCTION 3
# findTau_minimization() finds tau that minimizes the error in H1_0 norm, using a optimization algorithm
# it returns the value of tau and its error

def findTau_minimization(n, degree, f, u_exact, b, mu, tau_init):

    def error_tauFunc(x):
        uh = solveSupg_uOnly(n, degree, f, u_exact, b, mu, x)
        return errornorm(u_exact, uh, 'H10')

    res = opt.minimize_scalar(error_tauFunc, bracket=(0, tau_init))

    return res.x, res.fun
