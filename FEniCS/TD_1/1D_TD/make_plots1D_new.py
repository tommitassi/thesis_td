###############################################################################
# IMPORT LIBRARIES

from find_tau_new import *
from solve_supg import solveSupg

from fenics import *
import numpy as np
import matplotlib.pyplot as plt

###############################################################################
# PARAMETRS DEFINITION

# Finite elements degree
degree = 3

# Problem parameters
mu = 1e-2         # diffusion 1e-2
b = 1     # transport 500

# Mesh definition
n = 20           # number of elements

# Mu loop parameters
mu_min, mu_max = 2e-3, 2.5e-2
range_mu = 20

###############################################################################
# PROBLEM SETUP

# Mesh dimension definition
h = 1 / n

f = Constant(0)

def upwind_function(x):
    return 1 / np.tanh(x) - 1 / x

###############################################################################
# ERROR PLOT

mesh_exact = UnitIntervalMesh(1000)
V = FunctionSpace(mesh_exact, 'CG', degree)
# plt.figure()
# for degree in range(1,2):
#     u_exact = Expression('( exp(b*x[0]/mu)-1 ) / ( exp(b/mu)-1 )', b=b, mu=mu, degree=degree+3)
#     # se uso un degree diverso per Expression e project (se degree_proj < degree_Expres) viene una merda
#     V_exact = FunctionSpace(mesh_exact, 'CG', degree)
#     u_exact_proj = project(u_exact, V_exact)
#     plot(u_exact_proj, label="proj {}".format(degree), linewidth=2.0)
#     u_exact_interp = interpolate(u_exact, V_exact)
#     plot(u_exact_interp, label="interp {}".format(degree), linewidth=2.0)
#
# plt.legend()
# plt.show()

u_exact = Expression('( exp(-b*(1-x[0])/mu)-exp(-b/mu) ) / ( 1-exp(-b/mu) )', b=b, mu=mu, degree=degree)
Pe_h = b * h / 2 / mu
print("Peclet number is:",Pe_h)
tauTh_star = h / 2 / b / degree * upwind_function(Pe_h/degree)

_, u_th = solveSupg(n, degree, f, u_exact, b, mu, tauTh_star)
err_th = getError(u_exact, u_th)

tau_min, err_min = findTau_new(n, degree, f, u_exact, b, mu, tauTh_star)
_, u_min = solveSupg(n, degree, f, u_exact, b, mu, tau_min)

tau_star, err_star = plotTauRange(n, degree, f, u_exact, b, mu, tauTh_star)

# V = u_min.function_space()
u_exact_proj = interpolate(u_exact, V)

print("Minimized tau: {}, error: {}".format(tau_min, err_min))
print("Theoretical tau: {}, error: {}".format(tauTh_star, err_th))

plt.figure()
plt.plot(tau_star, err_star)
plt.plot(tau_min, err_min, '.', label="Minimum", markersize=10)
plt.plot(tauTh_star, err_th, '.', fillstyle='none', label="Theoretical", markersize=10)
plt.xlabel(r'$\tau$', fontsize=14)
plt.ylabel(r'$E(\tau)$', fontsize=14)
plt.xscale("log")
plt.legend()

plt.figure()
plot(u_exact_proj, label="Exact solution", linewidth=2.0, color='#d62728')
plot(u_min, label=r'SUPG solution with optimized $\tau$', linewidth=2.0, color='#ff7f0e')
plot(u_th, label=r'SUPG solution with theoretical $\tau$', linewidth=2.0, color='#2ca02c')
plt.xlabel(r'$x$', fontsize=14)
plt.ylabel(r'$u(x)$', fontsize=14)
plt.legend(fontsize=12)

# Mu loop ###############################################################################
# mu_vec3 = np.geomspace(mu_min, mu_max, range_mu, dtype=np.float64)
# mu_vec1 = np.geomspace(1e-4, 1e-3, 7, dtype=np.float64)
# mu_vec2 = np.geomspace(1e-3, 2e-3, 4, dtype=np.float64)
# mu_vec12 = np.append(mu_vec1, mu_vec2)
# mu_vec = np.append(mu_vec12, mu_vec3)
# range_mu = range_mu + 7 + 4
# tau_vec = np.zeros(range_mu)
# tauTh_vec = np.zeros(range_mu)
# err_vec = np.zeros(range_mu)
# errTh_vec = np.zeros(range_mu)
# Pe_vec = np.zeros(range_mu)
#
# i = 0
# for mu in mu_vec:
#
#     Pe_vec[i] = b * h / 2 / mu
#
#     # Tau theory definition
#     tauTh_vec[i] = h / 2 / b / degree * upwind_function(Pe_vec[i]/degree)
#
#     # Exact solution
#     u_exact = Expression('( exp(-b*(1-x[0])/mu)-exp(-b/mu) ) / ( 1-exp(-b/mu) )', b=b, mu=mu, degree=degree)
#     # u_exact = Expression('( exp(b*x[0]/mu)-1 ) / ( exp(b/mu)-1 )', b=b, mu=mu, degree=degree)
#
#     # Solve
#     print(i)
#     tau_vec[i], _ = findTau_new(n, degree, f, u_exact, b, mu, tauTh_vec[i])
#     _, uh = solveSupg(n, degree, f, u_exact, b, mu, tau_vec[i])
#     err_vec[i] = getErrorLastNode(u_exact, uh)
#
#     _, uTh = solveSupg(n, degree, f, u_exact, b, mu, tauTh_vec[i])
#     errTh_vec[i] = getErrorLastNode(u_exact, uTh)
#
#     i += 1
#
# # print(plt.rcParams['axes.prop_cycle'].by_key()['color'])
# plotTau = plt.figure(1)
# plt.plot(Pe_vec, tau_vec, label='Optimized', linewidth=2)
# plt.plot(Pe_vec, tauTh_vec, '--', label='Theoretical', linewidth=2)
# plt.xlabel(r'$\mathbb{P}_{\mathrm{e}_h}$', fontsize=12)
# plt.ylabel(r'$\tau$', fontsize=12)
# plt.xscale("log")
# plt.yscale("log")
# plt.legend()
#
# # error on last-1 node
# plotErr = plt.figure(2)
# plt.plot(Pe_vec, err_vec, label='Optimized', linewidth=2)
# plt.plot(Pe_vec, errTh_vec, '--', label='Theoretical', linewidth=2)
# plt.xlabel(r'$\mathbb{P}_{\mathrm{e}_h}$', fontsize=12)
# plt.ylabel(r'$e(x_{(K_h-1)};\tau)$', fontsize=12)
# plt.xscale("log")
# # plt.yscale("log")
# plt.legend()

###############################################################################
# DATASET PLOT

# features, targets, mean, std = loadAnnDataset("dataset1.csv", flag_plot=1, flag_extract=1)

# mu = np.geomspace(mu_min, mu_max, range_mu)
# Pe_h = b * h / 2 / mu
# tauTh_star = h / 2 / b / degree * upwind_function(Pe_h/degree)
# plt.plot(mu, tauTh_star, label="Theoretical")

# model = loadAnnModel("models/model2x64")
# A = np.zeros((range_mu, 2))
# A[:,1] = mu
# for degree in [1, 3]:
#     A[:,0].fill(degree)
#     tau_nn = model.predict( ( A - mean )/std )
#     tau_nn = 10**(-tau_nn)
#
#     if degree==1:
#         color = 'r'
#     elif degree==2:
#         color = 'b'
#     else:
#         color= 'g'
#
#     plt.plot(mu, tau_nn, label=r"ANN's predictions, $r = {}$".format(degree), linewidth=2, color=color)
#
# plt.legend()

###############################################################################
plt.show()
