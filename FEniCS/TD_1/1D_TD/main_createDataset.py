# CREATE DATASET FOR ML

from find_tau_new import *

import numpy as np
from scipy.stats import loguniform

###############################################################################
# PARAMETRS DEFINITION

# File name
fname = "newDataset.csv"

# Total number of iterations per degree per mesh
max_iter = 50

# Save every batch_size minimizations
batch_size = 2

# FE space degrees
degree_vec = [1, 3]

# Mesh sizes
n_vec = [20]

# Mu
mu_min = 2e-3
mu_max = 2.5e-2

# Beta
b = 1.0

###############################################################################
# LOOP
## x = [degree, mu]
## y = [tau]

f = Constant(0)
def upwind_function(x):
    return 1 / np.tanh(x) - 1 / x

print('\n################# TOTAL NUMBER OF ITERATIONS: {} #################'.format(len(n_vec)*len(degree_vec)*max_iter))
A = np.zeros((3, batch_size))
num_batches = max_iter//batch_size

# Create new csv
f1 = open(fname, "w")
f1.write('# r, mu, tau\n')
f1.close()

for n in n_vec:

    for degree in degree_vec:

        for i in range(num_batches):

            for j in range(batch_size):

                A[0, j] = degree

                mu = loguniform.rvs(mu_min, mu_max) # uniform distribution on a logscale
                A[1, j] = mu

                u_exact = Expression('( exp(b*x[0]/mu)-1 ) / ( exp(b/mu)-1 )', b=b, mu=mu, degree=degree)
                h = 1 / n
                Pe_h = b * h / 2 / mu
                tau_th = h / 2 / b / degree * upwind_function(Pe_h/degree)

                A[2, j], _ = findTau_new(n, degree, f, u_exact, b, mu, tau_th)


            f2 = open(fname, "ab")
            np.savetxt(f2, A.transpose(), delimiter=",")
            f2.close()

            print('################# Completed batch {}/{}, for degree {} and n {} #################'.format(i+1, num_batches, degree, n))
