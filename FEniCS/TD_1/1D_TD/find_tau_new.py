# FIND-TAU FUNCTION
# findTau() finds tau that minimizes the error in H1_0 norm
# it returns the value of tau and its error

from tensorflow import keras
from fenics import *
import numpy as np
import scipy.optimize as opt
import matplotlib.pyplot as plt
from solve_supg import solveSupg, solveSupg_uOnly

def plotTauRange(n, degree, f, u_exact, b, mu, tau_th):

    tau_min = tau_th / 2
    tau_max = tau_th * 3
    range_tau = 120
    tau_vec = np.geomspace(tau_min, tau_max, range_tau)

    errH10 = np.empty(range_tau) # initialize empty array

    for i in range(0, range_tau):

        _, u = solveSupg(n, degree, f, u_exact, b, mu, tau_vec[i])
        errH10[i] = getError(u_exact, u)

    idx = np.where(errH10 == np.amin(errH10))

    return tau_vec, errH10


# FIND-TAU FUNCTION 3
# findTau_new() finds tau that minimizes the error in H1_0 norm, using a optimization algorithm
# it returns the value of tau and its error

def findTau_new(n, degree, f, u_exact, b, mu, tau_init):

    def error_tauFunc(x):
        uh = solveSupg_uOnly(n, degree, f, u_exact, b, mu, x[0])
        return getError(u_exact, uh)

    x0 = np.array([tau_init])

    # res = opt.minimize(error_tauFunc, x0/2, options={'disp': True, 'gtol' : 1e-6})
    res = opt.minimize(error_tauFunc, x0/2, method="L-BFGS-B", bounds=opt.Bounds(0,10*tau_init), options={'disp': True, 'gtol' : 1e-6})

    return res.x[0], res.fun

def getError(u, uh):

    V = uh.function_space()
    u = interpolate(u, V)

    # it minimizes the error on nodes
    err_vertexes = uh.compute_vertex_values() - u.compute_vertex_values()
    err2 = np.dot(err_vertexes, err_vertexes)

    return sqrt(err2)

def getErrorLastNode(u, uh):

    V = uh.function_space()
    u = interpolate(u, V)

    err = uh.compute_vertex_values()[-2] - u.compute_vertex_values()[-2]
    # err2 = err*err

    return err

    # err_proj = uh - u
    # err2 = assemble(err_proj**2 * dx)
    # if normType == "H1":
    #     err2 += assemble((err_proj.dx(0))**2 * dx)

    # return errornorm(u, uh, normType)


def loadAnnDataset(fname, flag_plot=0, flag_extract=0):

    print("Loading dataset")

    all_features = []
    all_targets = []
    with open(fname) as f:
        for i, line in enumerate(f):
            if i == 0:
                header = line[1:].strip().split(", ")
                continue  # Skip header
            fields = line.strip().split(",")
            all_features.append([float(v) for v in fields[:-1]])
            all_targets.append([float(fields[-1])])

    features = np.array(all_features, dtype="float32")
    targets = np.array(all_targets, dtype="float32")
    m, n = features.shape

    ######################################
    ## Plot dataset
    if flag_plot == 1:
        for i in range(1, n):
            plt.figure(i)
            for j in range(m):
                if features[j,0] == 1:
                    plt.plot(features[j,i], targets[j], 'or', markersize=3, label=r'$r = 1$')
                elif features[j,0] == 2:
                    plt.plot(features[j,i], targets[j], 'ob', markersize=3, label=r'$r = 2$')
                else:
                    plt.plot(features[j,i], targets[j], 'og', markersize=3, label=r'$r = 3$')
            plt.yscale('log')
            plt.xscale('log')
            plt.xlabel(r'$\mu$')
            plt.ylabel(r'$\tau$')
            plt.legend()

    ######################################
    ## Extract validation set
    if flag_extract:
        # from tensorflow import keras
        num_val_samples = int(len(features) * 0.2) ## 20% of all features
        index_val_samples = np.linspace(1, int(len(features)-2), num_val_samples, dtype=int) ## equal distribution choice
        val_features = features[index_val_samples]
        val_targets = targets[index_val_samples]
        train_features = np.delete(features, index_val_samples, axis=0)
        train_targets = np.delete(targets, index_val_samples, axis=0)

    ######################################
    ## Compute mean and standard deviation
        mean = np.mean(train_features, axis=0)
        std = np.std(train_features, axis=0)

        return features, targets, mean, std

    return features, targets

def loadAnnModel(fname):
    # fname must be the absolute path of the model's files without extensions

    print("Loading model:", fname)

    # create the model from .json
    json_file = open(fname + ".json", 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = keras.models.model_from_json(loaded_model_json)

    # load weights into the model from .h5
    loaded_model.load_weights(fname + ".h5")

    print("\nModel successfully loaded from disk")

    return loaded_model
