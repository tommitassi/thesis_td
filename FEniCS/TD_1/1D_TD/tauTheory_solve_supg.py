# SUPG SOLVER IMPLEMENTATION
# tauTheory_solveSupg() solves a transport-diffusion 1D problem with SUPG method using theorized Tau
# it returns the created mesh and the approximated solution uh

from fenics import *
import numpy as np

def tauTheory_solveSupg(nx, degree, f, u_exact, b_value, mu_value):

  # 1. Mesh generation and problem setup
  mesh = UnitIntervalMesh(nx)

  mu = Constant(mu_value)
  b = Constant(b_value)

  # 2. Finite element space
  V = FunctionSpace(mesh, 'CG', degree)

  g_boundary = u_exact

  def boundary(x, on_boundary):
      return on_boundary

  bc = DirichletBC(V, g_boundary, boundary)

  # 3. Matrices and vectors
  u = TrialFunction(V)
  v = TestFunction(V)

  a = ( mu * u.dx(0) * v.dx(0) + b * u.dx(0) * v ) * dx
  L = f * v * dx

  h = CellDiameter(mesh)
  bnorm = sqrt(dot(b, b))
  tau_K = 0.5 * h / conditional(bnorm * h > 6 * mu, bnorm, 6 * mu / h)

  if tau_K:

    A = lambda u: -mu * (u.dx(0)).dx(0) + b * u.dx(0)
    A_SS = lambda u: b * u.dx(0) + 0.5 * div(b) * u

    a_stabilization = tau_K * A(u) * A_SS(v) * dx

    a = a + a_stabilization

  # 4. Solve
  uh = Function(V)
  solve(a == L, uh, bc)

  return mesh, uh
