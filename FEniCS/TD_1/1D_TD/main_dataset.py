###############################################################################
# IMPORT LIBRARIES

from fenics import *
import numpy as np
from beta_loop import betaLoop

###############################################################################
# PARAMETRS DEFINITION

# Finite elements degree
degree = 1

# Problem parameters
mu = 1.0          # diffusion
# b = LOOPED      # transport

# Cosine forcing parameters
# C_cos = LOOPED  # amplitude (0 for homogeneous problem)
# omega = LOOPED  # frequency
# phi = LOOPED    # phase

# Peclet number
Pe = 1.1          # Pe > 1.0 for unstable numerical methods

# Tau loop parameters
range_tau = 60    # tau-loop max iterations
tau_min = 1e-06   # minimum value of tau
tau_max = 3e-04   # maximum value of tau

# Beta loop parameters
range_b = 30      # beta-loop max iterations
b_min = 50        # minimum value of beta
b_max = 300       # maximum value of beta

###############################################################################
# LOOP DEFINITION

range_C = 6
range_om = 10
range_phi = 5

C_vec = np.linspace(0, 15, range_C)
om_vec = np.linspace(1*np.pi, 100*np.pi, range_om)
phi_vec = np.linspace(0, np.pi/2, range_om)

###############################################################################
# MAIN

A = np.zeros((5, range_b)) # [[tau], [beta], [C_cos], [omega], [phi]]

f1 = open("datasetNew.csv", "w")
f1.write('# Tau,                     Beta,                    C_cos,                   Omega,                   Phi\n')
f1.close()

f2 = open('datasetNew.csv','ab')

i, j, k = 0, 0, 0
for C_cos in C_vec:
    for omega in om_vec:
        for phi in phi_vec:

            A[0], _, A[1] = betaLoop(degree, mu, range_tau, tau_min, tau_max, range_b, b_min, b_max, C_cos, omega, phi, Pe)

            A[2].fill(C_cos) # it fills C_cos row
            A[3].fill(omega) # it fills omega row
            A[4].fill(phi)   # it fills phi row

            np.savetxt(f2, A.transpose(), delimiter=",")

            k = k + 1
            print('################################# {}/{} #################################'.format(k, range_C*range_om*range_phi))

        j = j + 1
        print('##################################################################')
        print('Inner iteration {} of {} completed'.format(j, range_om))
        print('Full iteration {} of {} is going'.format(i+1, range_C))
        print('##################################################################')

    j = 0
    i = i + 1
    print('##################################################################')
    print('##################################################################')
    print('Full iteration {} of {} completed'.format(i, range_C))
    print('##################################################################')
    print('##################################################################')


f2.close()
