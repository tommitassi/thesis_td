###############################################################################
# IMPORT LIBRARIES

from fenics import *
import numpy as np
import matplotlib.pyplot as plt
from solve_supg import solveSupg
from tauTheory_solve_supg import tauTheory_solveSupg
from find_tau import findTau_autoRange, findTau_minimization

###############################################################################
# PARAMETRS DEFINITION

# Finite elements degree
degree = 1

# Problem parameters
mu = 0.005          # diffusion
b = 1.0         # transport

# Cosine forcing parameters
C_cos = 1      # amplitude (0 for homogeneous problem)
omega = np.pi     # frequency
phi = 1         # phase

# Mesh definition
n = 50           # number of elements

# Tau loop parameters
range_tau = 40    # tau-loop max iterations

###############################################################################
# POBLEM SETUP

# Homogeneous
# f = Constant(0)
# u_exact = Expression('(exp(bx * (x[0] - 1) / mu) - exp(-bx / mu)) / (1 - exp(-bx / mu))',
#                      mu=mu, bx=b, degree=4*degree+1)

# Forced
f = Expression('C*cos(om*x[0] + phi)',
           C=C_cos, om=omega, phi=phi, degree=2*degree+1)
def u_p(x):
  return C_cos / (mu*omega*omega + b*b/mu) * ( np.cos(omega*x + phi) + b/mu/omega * np.sin(omega*x + phi) )
u_exact = Expression('(up0*(exp(b*x[0]/mu)-exp(b/mu)) + (exp(b*x[0]/mu)-1)*(1-up1)) /(exp(b/mu)-1) + C / (mu*om*om + b*b/mu) * ( cos(om*x[0] + phi) + b/mu/om * sin(om*x[0] + phi) )',
                    b=b, mu=mu, up0=u_p(0), up1=u_p(1), C=C_cos, om=omega, phi=phi, degree=4*degree)

# Mesh dimension definition
h = 1 / n
Pe = b * h / 2 / mu

# Tau theory definition
if b * h > 6 * mu:
    tau_th = 0.5 * h / b
else:
    tau_th = 0.5 * h / (6 * mu / h)
tau_order = int(np.floor(np.log10(tau_th)))

###############################################################################
# SOLUTION

mesh, u = tauTheory_solveSupg(n, degree, f, u_exact, b, mu)

tau, err_f = findTau_autoRange(n, degree, f, u_exact, b, mu, range_tau, tau_order)
_, u_f = solveSupg(n, degree, f, u_exact, b, mu, tau)

tau2, err_f2 = findTau_minimization(n, degree, f, u_exact, b, mu, tau_th)
_, u_f2 = solveSupg(n, degree, f, u_exact, b, mu, tau2)

V = FunctionSpace(mesh, 'CG', 4*degree)
g_boundary = u_exact
def boundary(x, on_boundary):
  return on_boundary
bc = DirichletBC(V, g_boundary, boundary)

print('\nPeclet: {}'.format(Pe))
print('\nErrors for tau predicted (tau = {})'.format(tau_th))
print('Error H1: {}'.format(errornorm(u_exact, u, 'H10')))
print('Error L2: {}'.format(errornorm(u_exact, u, 'L2')))
print('\nErrors for tau found (tau = {})'.format(tau))
print('Error H1: {}'.format(errornorm(u_exact, u_f, 'H10')))
print('Error L2: {}'.format(errornorm(u_exact, u_f, 'L2')))
print('\nErrors for tau found through minimization (tau = {})'.format(tau2))
print('Error H1: {}'.format(errornorm(u_exact, u_f2, 'H10')))
print('Error L2: {}'.format(errornorm(u_exact, u_f2, 'L2')))

plot(project(u_exact, V=V, bcs=bc, mesh=mesh), label='Exact')
plot(u, label='Theory')
plot(u_f, label='Optimized')
plot(u_f2, label='Optimized 2')
plt.legend()
plt.title('Peclet = {}'.format(Pe))
plt.show()
