###############################################################################
# IMPORT LIBRARIES

from fenics import *
import numpy as np
import matplotlib.pyplot as plt
from tauTheory_solve_supg import tauTheory_solveSupg
from find_tau import findTau_autoRange, findTau_minimization
from solve_supg import solveSupg

###############################################################################
# PARAMETRS DEFINITION

# Finite elements degree
degree = 1

# Problem parameters
# mu = 1.0          # diffusion
b = 1.0        # transport

# Cosine forcing parameters
C_cos = 10.0      # amplitude (0 for homogeneous problem)
omega = np.pi     # frequency
phi = 0.0         # phase

# Mesh definition
n = 50           # number of elements

# Tau loop parameters
range_tau = 40    # tau-loop max iterations

# Beta loop parameters
b_min, b_max = 10.0, 700.0
range_b = 50

# Mu loop parameters
mu_min, mu_max = 3e-3, 1.0
range_mu = 50

###############################################################################
# PROBLEM SETUP

# Mesh dimension definition
h = 1 / n

###############################################################################
# LOOPS

# Beta loop ###############################################################################
# b_vec = np.linspace(b_min, b_max, range_b, dtype=np.float64)
# tau_vec = np.zeros(range_b)
# tauTh_vec = np.zeros(range_b)
# tauTh2_vec = np.zeros(range_b)
# err_vec = np.zeros(range_b)
# errTh_vec = np.zeros(range_b)
# errTh2_vec = np.zeros(range_b)
# Pe_vec = np.zeros(range_b)
#
# f = Expression('C*cos(om*x[0] + phi)',
#                 C=C_cos, om=omega, phi=phi, degree=2*degree+1)
#
# for i in range(0, range_b):
#
#     Pe_vec[i] = b_vec[i] * h / 2 / mu
#
#     # Tau theory definition
#     if b_vec[i] * h > 6 * mu:
#         tauTh_vec[i] = 0.5 * h / b_vec[i]
#     else:
#         tauTh_vec[i] = 0.5 * h / (6 * mu / h)
#
#     # Tau theory 2 definition (Tezduyar)
#     tauTh2_vec[i] = 1 / ( 2*b_vec[i] / h + 4*mu / h / h)
#
#     tau_order = int(np.floor(np.log10(tauTh_vec[i])))
#
#     # Exact solution
#     def u_p(x):
#         return C_cos / (mu*omega*omega + b_vec[i]*b_vec[i]/mu) * ( np.cos(omega*x + phi) + b_vec[i]/mu/omega * np.sin(omega*x + phi) )
#     u_exact = Expression('(up0*(exp(b*x[0]/mu)-exp(b/mu)) + (exp(b*x[0]/mu)-1)*(1-up1)) /(exp(b/mu)-1) + C / (mu*om*om + b*b/mu) * ( cos(om*x[0] + phi) + b/mu/om * sin(om*x[0] + phi) )',
#                         b=b_vec[i], mu=mu, up0=u_p(0), up1=u_p(1), C=C_cos, om=omega, phi=phi, degree=4)
#
#     # Solve
#     # tau_vec[i], err_vec[i] = findTau_autoRange(n, degree, f, u_exact, b_vec[i], mu, range_tau, tau_order)
#     tau_vec[i], err_vec[i] = findTau_minimization(n, degree, f, u_exact, b_vec[i], mu, tauTh_vec[i])
#
#     _, uTh = tauTheory_solveSupg(n, degree, f, u_exact, b_vec[i], mu)
#     errTh_vec[i] = errornorm(u_exact, uTh, 'H10')
#
#     _, uTh2 = solveSupg(n, degree, f, u_exact, b_vec[i], mu, tauTh2_vec[i])
#     errTh2_vec[i] = errornorm(u_exact, uTh2, 'H10')
#
#
# plotTau = plt.figure(1)
# plt.plot(b_vec, tau_vec, label='Optimized')
# plt.plot(b_vec, tauTh_vec, label='Theory 1')
# plt.plot(b_vec, tauTh2_vec, label='Theory 2')
# plt.title('Beta - Tau')
# plt.legend()
#
# plotErr = plt.figure(2)
# plt.plot(b_vec, err_vec, label='Optimized')
# plt.plot(b_vec, errTh_vec, label='Theory')
# plt.plot(b_vec, errTh2_vec, label='Theory New')
# plt.title('Beta - Error')
# plt.legend()
#
# plt.show()

# Mu loop ###############################################################################
mu_vec = np.geomspace(mu_min, mu_max, range_mu, dtype=np.float64)
tau_vec = np.zeros(range_mu)
tauTh_vec = np.zeros(range_mu)
tauTh2_vec = np.zeros(range_mu)
err_vec = np.zeros(range_mu)
errTh_vec = np.zeros(range_mu)
errTh2_vec = np.zeros(range_mu)
Pe_vec = np.zeros(range_mu)

f = Expression('C*cos(om*x[0] + phi)',
                C=C_cos, om=omega, phi=phi, degree=2*degree+1)

for i in range(0, range_mu):

    Pe_vec[i] = b * h / 2 / mu_vec[i]

    # Tau theory definition
    if b * h > 6 * mu_vec[i]:
        tauTh_vec[i] = 0.5 * h / b
    else:
        tauTh_vec[i] = 0.5 * h / (6 * mu_vec[i] / h)

    # Tau theory 2 definition (Tezduyar)
    tauTh2_vec[i] = 1 / ( 2*b / h + 4*mu_vec[i] / h / h)

    tau_order = int(np.floor(np.log10(tauTh_vec[i])))

    # Exact solution
    def u_p(x):
        return C_cos / (mu_vec[i]*omega*omega + b*b/mu_vec[i]) * ( np.cos(omega*x + phi) + b/mu_vec[i]/omega * np.sin(omega*x + phi) )
    u_exact = Expression('(up0*(exp(b*x[0]/mu)-exp(b/mu)) + (exp(b*x[0]/mu)-1)*(1-up1)) /(exp(b/mu)-1) + C / (mu*om*om + b*b/mu) * ( cos(om*x[0] + phi) + b/mu/om * sin(om*x[0] + phi) )',
                        b=b, mu=mu_vec[i], up0=u_p(0), up1=u_p(1), C=C_cos, om=omega, phi=phi, degree=4)

    # Solve
    # tau_vec[i], err_vec[i] = findTau_autoRange(n, degree, f, u_exact, b, mu_vec[i], range_tau, tau_order)
    tau_vec[i], err_vec[i] = findTau_minimization(n, degree, f, u_exact, b, mu_vec[i], tauTh_vec[i])

    _, uTh = tauTheory_solveSupg(n, degree, f, u_exact, b, mu_vec[i])
    errTh_vec[i] = errornorm(u_exact, uTh, 'H10')

    _, uTh2 = solveSupg(n, degree, f, u_exact, b, mu_vec[i], tauTh2_vec[i])
    errTh2_vec[i] = errornorm(u_exact, uTh2, 'H10')


plotTau = plt.figure(1)
plt.xscale("log")
plt.plot(mu_vec, tau_vec, label='Optimized')
plt.plot(mu_vec, tauTh_vec, label='Theory 1')
plt.plot(mu_vec, tauTh2_vec, label='Theory 2')
plt.title('Peclet - Tau')
plt.legend()

plotErr = plt.figure(2)
plt.xscale("log")
plt.plot(mu_vec, err_vec, label='Optimized')
plt.plot(mu_vec, errTh_vec, label='Theory')
plt.plot(mu_vec, errTh2_vec, label='Theory New')
plt.title('Mu - Error')
plt.legend()

plt.show()

###############################################################################
# ERROR PLOT
# IMPORTANT: modify findTau_autoRange() to run this part

# f = Expression('C*cos(om*x[0] + phi)',
#                 C=C_cos, om=omega, phi=phi, degree=2*degree+1)
#
# def u_p(x):
#     return C_cos / (mu*omega*omega + b*b/mu) * ( np.cos(omega*x + phi) + b/mu/omega * np.sin(omega*x + phi) )
# u_exact = Expression('(up0*(exp(b*x[0]/mu)-exp(b/mu)) + (exp(b*x[0]/mu)-1)*(1-up1)) /(exp(b/mu)-1) + C / (mu*om*om + b*b/mu) * ( cos(om*x[0] + phi) + b/mu/om * sin(om*x[0] + phi) )',
#                       b=b, mu=mu, up0=u_p(0), up1=u_p(1), C=C_cos, om=omega, phi=phi, degree=4)
#
# if b * h > 6 * mu:
#     tauTh_star = 0.5 * h / b
# else:
#     tauTh_star = 0.5 * h / (6 * mu / h)
# tau_order = int(np.floor(np.log10(tauTh_star)))
#
#
# tau_star, err_star = findTau_autoRange(n, degree, f, u_exact, b, mu, range_tau, tau_order)
#
#
# plt.plot(tau_star, err_star)
# plt.title('mu = {}, beta = {}, C = {}'.format(mu, b, C_cos))
# plt.xlabel('Tau')
# plt.ylabel('Error')
#
# plt.show()
