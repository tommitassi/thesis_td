# BETA-LOOPING FUNCTION
# betaLoop loops over beta and computes tau using find_tau
# it returns the arrays of tau, error and beta

from fenics import *
import numpy as np
from find_tau import findTau

def betaLoop(degree, mu, range_tau, tau_min, tau_max, range_b, b_min, b_max, C_cos, omega, phi, Pe):

  b_vec = np.linspace(b_min, b_max, range_b, dtype=np.float64)

  tau = np.zeros(range_b)
  err = np.zeros(range_b)

  for i in range(0, range_b):

    # Problem setup
    f = Expression('C*cos(om*x[0] + phi)',
               C=C_cos, om=omega, phi=phi, degree=2*degree+1)
    def u_p(x):
      return C_cos / (mu*omega*omega + b_vec[i]*b_vec[i]/mu) * ( np.cos(omega*x + phi) + b_vec[i]/mu/omega * np.sin(omega*x + phi) )
    u_exact = Expression('(up0*(exp(b*x[0]/mu)-exp(b/mu)) + (exp(b*x[0]/mu)-1)*(1-up1)) /(exp(b/mu)-1) + C / (mu*om*om + b*b/mu) * ( cos(om*x[0] + phi) + b/mu/om * sin(om*x[0] + phi) )',
                        b=b_vec[i], mu=mu, up0=Constant(u_p(0)), up1=Constant(u_p(1)), C=C_cos, om=omega, phi=phi, degree=4*degree+1)

    # Mesh dimension definition
    h = Pe * 2 * mu / b_vec[i]
    n = int(1 // h)

    # Compute tau
    tau[i], err[i] = findTau(n, degree, f, u_exact, b_vec[i], mu, range_tau, tau_min, tau_max)
    print('i = {} - beta = {} - Minimum found at tau = {}'.format(i, b_vec[i], tau[i]))

  return tau, err, b_vec
