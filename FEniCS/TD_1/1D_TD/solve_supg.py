# SUPG SOLVER IMPLEMENTATION
# solveSupg() solves a transport-diffusion 1D problem with SUPG method
# it returns the created mesh and the approximated solution uh

from fenics import *
import numpy as np

def solveSupg(nx, degree, f, u_exact, b_value, mu_value, tau_K):

  # 1. Mesh generation and problem setup
  mesh = UnitIntervalMesh(nx)

  mu = Constant(mu_value)
  b = Constant(b_value)

  # 2. Finite element space
  V = FunctionSpace(mesh, 'CG', degree)

  g_boundary = u_exact

  def boundary(x, on_boundary):
      return on_boundary

  bc = DirichletBC(V, g_boundary, boundary)

  # 3. Matrices and vectors
  u = TrialFunction(V)
  v = TestFunction(V)

  a = ( mu * u.dx(0) * v.dx(0) + b * u.dx(0) * v ) * dx
  L = f * v * dx

  if tau_K:
      ResLHS = -mu * (u.dx(0)).dx(0) + b * u.dx(0)
      ResRHS = f

      pv_supg = b * v.dx(0) + 0.5 * div(b) * v

      a_supg = tau_K * ResLHS * pv_supg * dx
      L_supg = tau_K * ResRHS * pv_supg * dx

      a = a + a_supg
      L = L + L_supg

  # 4. Solve
  uh = Function(V)
  solve(a == L, uh, bc)

  return mesh, uh


# solveSupg_uOnly() solves a transport-diffusion 1D problem with SUPG method
# it returns only the approximated solution uh

def solveSupg_uOnly(nx, degree, f, u_exact, b_value, mu_value, tau_K):

  # 1. Mesh generation and problem setup
  mesh = UnitIntervalMesh(nx)

  mu = Constant(mu_value)
  b = Constant(b_value)

  # 2. Finite element space
  V = FunctionSpace(mesh, 'CG', degree)

  g_boundary = u_exact

  def boundary(x, on_boundary):
      return on_boundary

  bc = DirichletBC(V, g_boundary, boundary)

  # 3. Matrices and vectors
  u = TrialFunction(V)
  v = TestFunction(V)

  a = ( mu * u.dx(0) * v.dx(0) + b * u.dx(0) * v ) * dx
  L = f * v * dx

  if tau_K:
      ResLHS = -mu * (u.dx(0)).dx(0) + b * u.dx(0)
      ResRHS = f

      pv_supg = b * v.dx(0) + 0.5 * div(b) * v

      a_supg = tau_K * ResLHS * pv_supg * dx
      L_supg = tau_K * ResRHS * pv_supg * dx

      a = a + a_supg
      L = L + L_supg

  # 4. Solve
  uh = Function(V)
  solve(a == L, uh, bc)

  return uh
