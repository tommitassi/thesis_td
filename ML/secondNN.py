from tensorflow import keras
import csv
import numpy as np
from datetime import datetime
import matplotlib.pyplot as plt

######################################
## Choose parameters
N_epochs = 1000
N_neurons = 16

######################################
## Vectrize the CSV file
fname = "./../FEniCS/datasetCreation2D_TD/appendedDataset1.csv"

all_features = []
all_targets = []
with open(fname) as f:
    for i, line in enumerate(f):
        if i == 0:
            header = line[1:].strip().split(", ")
            print("\nHEADER:", header)
            continue  # Skip header
        fields = line.strip().split(",")
        all_features.append([float(v) for v in fields[:-1]])
        all_targets.append([float(fields[-1])])
        if i == 1:
            print("EXAMPLE FEATURES:", all_features[-1])
            print("EXAMPLE TARGETS :", all_targets[-1])

features = np.array(all_features, dtype="float32")
targets = np.array(all_targets, dtype="float32")
m, n = features.shape
print("\nfeatures.shape: ({}, {})".format(m, n))
print("targets.shape:", targets.shape)

######################################
## Plot dataset
# for i in range(1, n):
#     plt.figure(i)
#     for j in range(m):
#         if features[j,0] == 1:
#             plt.plot(features[j,i], targets[j], 'or', markersize=1, label='degree 1')
#         elif features[j,0] == 2:
#             plt.plot(features[j,i], targets[j], 'ob', markersize=1, label='degree 2')
#         else:
#             plt.plot(features[j,i], targets[j], 'og', markersize=1, label='degree 3')
#     # plt.plot(features[:,i], targets, 'o', markersize=1)
#     plt.yscale('log')
#     plt.xlabel(header[i])
#     plt.ylabel(header[-1])
#     plt.legend()
# plt.show()

# print(np.where(features[:,4] == np.amax(features[:,4]))) # to find specific data

######################################
## Extract validation set
num_val_samples = int(len(features) * 0.2) ## 20% of all features
index_val_samples = np.linspace(1, int(len(features)-2), num_val_samples, dtype=int) ## equal distribution choice
val_features = features[index_val_samples]
val_targets = targets[index_val_samples]
train_features = np.delete(features, index_val_samples, axis=0)
train_targets = np.delete(targets, index_val_samples, axis=0)

num_train_samples = len(train_features)
print("\nNumber of training samples:", num_train_samples)
print("Number of validation samples:", num_val_samples)

######################################
## Normalize the data
mean = np.mean(train_features, axis=0)
train_features -= mean
val_features -= mean

std = np.std(train_features, axis=0)
train_features /= std
val_features /= std

# for i in range(n):
#     print(header[i])
#     print("Max: ", np.amax(train_features[:,i]))
#     print("Min: {}\n".format(np.amin(train_features[:,i])))

#####################################
## Buikd the model
model = keras.Sequential(
    [
        keras.layers.Dense(N_neurons, activation="tanh", input_shape=(n,)),
        # keras.layers.Dense(N_neurons, activation="tanh"),
        # keras.layers.Dropout(0.1),
        keras.layers.Dense(1, activation="sigmoid"),
    ]
)
model.summary()

######################################
## Train the model
metrics = [
    keras.metrics.MeanSquaredError(),
    keras.metrics.MeanSquaredLogarithmicError(),
]

alpha = 1e-2 # initial learning rate
lr_schedule = keras.optimizers.schedules.InverseTimeDecay(
  alpha,
  decay_steps=1000,
  decay_rate=1,
  staircase=False)

model.compile(
    optimizer=keras.optimizers.Adam(lr_schedule), loss="mean_squared_logarithmic_error", metrics=metrics
)

logDir = "logs/" + datetime.now().strftime("%Y%m%d-%H%M%S")
tb_callback = keras.callbacks.TensorBoard(log_dir=logDir)

model.fit(
    train_features,
    train_targets,
    # batch_size=num_train_samples, # batch gradient descent
    epochs=N_epochs,
    # callbacks=[tb_callback],
    validation_data=(val_features, val_targets),
)

######################################
## Save the model
model_json = model.to_json() # save model to json
with open("models/newModel.json", "w") as json_file:
    json_file.write(model_json)

model.save_weights("models/newModel.h5") # save weights to HDF5
print("Saved model to disk")
