from tensorflow import keras
import tensorflow_docs as tfdocs
import tensorflow_docs.modeling
import tensorflow_docs.plots
import csv
import numpy as np
from matplotlib import pyplot as plt
from datetime import datetime

######################################
## Choose parameters
alpha_init = 1e-2
max_epochs = 100
BATCH_SIZE = 1320

######################################
## Vectrize the CSV file
fname = "./../../FEniCS/TD_1/datasetCreation2D_TD/appendedDataset1.csv"

all_features = []
all_targets = []
with open(fname) as f:
    for i, line in enumerate(f):
        if i == 0:
            header = line[1:].strip().split(", ")
            print("\nHEADER:", header)
            continue  # Skip header
        fields = line.strip().split(",")
        all_features.append([float(v) for v in fields[:-1]])
        all_targets.append([float(fields[-1])])
        if i == 1:
            print("EXAMPLE FEATURES:", all_features[-1])
            print("EXAMPLE TARGETS :", all_targets[-1])

features = np.array(all_features, dtype="float32")
targets = np.array(all_targets, dtype="float32")
m, n = features.shape
print("\nfeatures.shape: ({}, {})".format(m, n))
print("targets.shape:", targets.shape)

######################################
## Extract validation set
num_val_samples = int(len(features) * 0.2) ## 20% of all features
index_val_samples = np.linspace(1, int(len(features)-2), num_val_samples, dtype=int) ## equal distribution choice
val_features = features[index_val_samples]
val_targets = targets[index_val_samples]
train_features = np.delete(features, index_val_samples, axis=0)
train_targets = np.delete(targets, index_val_samples, axis=0)

num_train_samples = len(train_features)
print("\nNumber of training samples:", num_train_samples)
print("Number of validation samples:", num_val_samples)

######################################
## Normalize the data
mean = np.mean(train_features, axis=0)
train_features -= mean
val_features -= mean

std = np.std(train_features, axis=0)
train_features /= std
val_features /= std

###############################################################################
## FIND ARCHITECTURE

STEPS_PER_EPOCH = num_train_samples//BATCH_SIZE

lr_schedule = keras.optimizers.schedules.InverseTimeDecay(
  alpha_init,
  decay_steps=STEPS_PER_EPOCH*1000,
  decay_rate=1,
  staircase=False)

## Define functions
def get_optimizer():
  return keras.optimizers.Adam(lr_schedule)

def get_callbacks(name):
  return [
    tfdocs.modeling.EpochDots(),
    keras.callbacks.EarlyStopping(monitor='mean_squared_logarithmic_error', patience=200),
    keras.callbacks.TensorBoard(log_dir=name),
  ]

def compile_and_fit(model, name, optimizer=None):
  if optimizer is None:
    optimizer = get_optimizer()
  model.compile(optimizer=optimizer,
                loss="mean_squared_logarithmic_error",
                metrics=[
                  keras.metrics.MeanSquaredLogarithmicError(),
                  keras.metrics.MeanSquaredError()])

  model.summary()

  history = model.fit(
    train_features,
    train_targets,
    # steps_per_epoch = STEPS_PER_EPOCH,
    batch_size=BATCH_SIZE,
    epochs=max_epochs,
    validation_data=(val_features, val_targets),
    callbacks=get_callbacks(name),
    verbose=0)
  return history

## Create models
tiny_model = keras.Sequential([
    keras.layers.Dense(32, activation='tanh', input_shape=(train_features.shape[1],)),
    keras.layers.Dense(32, activation="tanh"),
    # keras.layers.Dropout(0.1),
    keras.layers.Dense(1, activation="sigmoid")
])

small_model = keras.Sequential([
    keras.layers.Dense(64, activation='tanh', input_shape=(train_features.shape[1],)),
    keras.layers.Dense(64, activation="tanh"),
    # keras.layers.Dropout(0.01),
    keras.layers.Dense(1, activation="sigmoid")
])

medium_model = keras.Sequential([
    keras.layers.Dense(128, activation='tanh', input_shape=(train_features.shape[1],)),
    keras.layers.Dense(128, activation="tanh"),
    # keras.layers.Dropout(0.05),
    keras.layers.Dense(1, activation="sigmoid")
])

large_model = keras.Sequential([
    keras.layers.Dense(256, activation='tanh', input_shape=(train_features.shape[1],)),
    keras.layers.Dense(256, activation="tanh"),
    # keras.layers.Dropout(0.1),
    keras.layers.Dense(1, activation="sigmoid")
])

size_histories = {}

size_histories['Tiny'] = compile_and_fit(tiny_model, "logs/sizes/Tiny")
size_histories['Small'] = compile_and_fit(small_model, 'logs/sizes/Small')
size_histories['Medium']  = compile_and_fit(medium_model, "logs/sizes/Medium")
size_histories['Large'] = compile_and_fit(large_model, "logs/sizes/Large")


## Evaluate the models
plotter = tfdocs.plots.HistoryPlotter(metric = 'mean_squared_logarithmic_error', smoothing_std=10)
plotter.plot(size_histories)
plt.yscale("log")
plt.xlabel("Epochs")
plt.show()
