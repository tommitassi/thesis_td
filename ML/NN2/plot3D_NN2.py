from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

###############################################################################
## Load the model
# inputString = input("Type the model file name (leave blank for new models): ")
# if not inputString:
#     inputString = "newModel"
# modelFileName = "models/" + inputString
modelFileName = "models/model_2x64_1k_stoc"

# load json and create model
json_file = open(modelFileName + ".json", 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = keras.models.model_from_json(loaded_model_json)

# load weights into new model
loaded_model.load_weights(modelFileName + ".h5")
print("Loaded model from disk")

###############################################################################
## Load the dataset
fname = "./../../FEniCS/TD_1/datasetCreation2D_TD/appendedDataset1.csv"

all_features = []
all_targets = []
with open(fname) as f:
    for i, line in enumerate(f):
        if i == 0:
            header = line[1:].strip().split(", ")
            continue  # Skip header
        fields = line.strip().split(",")
        all_features.append([float(v) for v in fields[:-1]])
        all_targets.append([float(fields[-1])])

features = np.array(all_features, dtype="float32")
targets = np.array(all_targets, dtype="float32")

num_val_samples = int(len(features) * 0.2)
index_val_samples = np.linspace(1, int(len(features)-2), num_val_samples, dtype=int)
# val_features = features[index_val_samples]
# val_targets = targets[index_val_samples]
train_features = np.delete(features, index_val_samples, axis=0)
train_targets = np.delete(targets, index_val_samples, axis=0)
num_train_samples = len(train_features)

mean = np.mean(train_features, axis=0)
std = np.std(train_features, axis=0)

###############################################################################
## Set parameters
n = 40 # fixed with this dataset
n_Pe = 10
n_mu = 100
degree = 2
Pe = np.linspace(1.1, 10, n_Pe)
mu = np.geomspace(1.6e-3, 0.3, n_mu)

###############################################################################
## Compute predictions
Z_pred = np.zeros((n_Pe, n_mu))
h = 1 / n

A = np.zeros((n_mu, 5))
A[:,0].fill(degree)
A[:,2] = mu

for i in range(n_Pe):
    A[:,1].fill(Pe[i])
    A[:,3] = 2 * Pe[i] * A[:,2] / h / np.sqrt(2)
    A[:,4] = 2 * Pe[i] * A[:,2] / h / np.sqrt(2)
    A_normed = ( A - mean )/std
    z_pred = loaded_model.predict(A_normed)
    Z_pred[i,:] = z_pred[:,0]

###############################################################################
## Tau theory - Quarteroni book
def upwind_function(x):
    return 1 / np.tanh(x) - 1 / x

def tau_th(h, b_norm, Pe_h, r=1):
    return h / 2 / b_norm / r * upwind_function(Pe_h/r)

Z_th = np.zeros((n_Pe, n_mu))
for i in range(n_Pe):
    b_norm = 2 * Pe[i] * mu / h
    Z_th[i,:] = tau_th(h, b_norm, Pe[i], degree)

###############################################################################
## 3D Plot
fig = plt.figure()
ax = fig.gca(projection='3d')

X, Y = np.meshgrid(Pe, mu)
Y = np.log10(Y) # for log scale

ax.plot_surface(X, Y, Z_pred.T, cmap='viridis')
# ax.plot_surface(X, Y, Z_th.T)

yticks = [1e-3, 1e-2, 1e-1, 1e-0] # for y log scale
ax.set_yticks(np.log10(yticks)) # for y log scale
ax.set_yticklabels(yticks) # for y log scale

ax.set_xlabel('Pe')
ax.set_ylabel('Mu')
ax.set_zlabel('Tau')

plt.title('degree = {}, h = {}'.format(degree, h))
plt.show()
