from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

###############################################################################
## Load the model
modelFileName = "models/model_2x64_1k_stoc"

# load json and create model
json_file = open(modelFileName + ".json", 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = keras.models.model_from_json(loaded_model_json)

# load weights into new model
loaded_model.load_weights(modelFileName + ".h5")
print("Loaded model from disk")

###############################################################################
## Load the dataset
fname = "./../../FEniCS/TD_1/datasetCreation2D_TD/appendedDataset1.csv"

all_features = []
all_targets = []
with open(fname) as f:
    for i, line in enumerate(f):
        if i == 0:
            header = line[1:].strip().split(", ")
            continue  # Skip header
        fields = line.strip().split(",")
        all_features.append([float(v) for v in fields[:-1]])
        all_targets.append([float(fields[-1])])

features = np.array(all_features, dtype="float32")
targets = np.array(all_targets, dtype="float32")

num_val_samples = int(len(features) * 0.2)
index_val_samples = np.linspace(1, int(len(features)-2), num_val_samples, dtype=int)
# val_features = features[index_val_samples]
# val_targets = targets[index_val_samples]
train_features = np.delete(features, index_val_samples, axis=0)
train_targets = np.delete(targets, index_val_samples, axis=0)
num_train_samples = len(train_features)

mean = np.mean(train_features, axis=0)
std = np.std(train_features, axis=0)

###############################################################################
## Set parameters
n = 40 # fixed with this dataset
n_mu = 100
degree = 1
b = 1
mu = np.geomspace(1.6e-3, 0.3, n_mu)

###############################################################################
## Compute predictions
h = 1 / n
Pe_h = np.sqrt(2) * b * h / 2 / mu
Pe_g = np.sqrt(2) * b / 2 / mu
A = np.zeros((n_mu, 5))

A[:,0].fill(degree)
A[:,1] = Pe_h
A[:,2] = mu
A[:,3].fill(b)
A[:,4].fill(b)
A_normed = ( A - mean )/std
z_pred = loaded_model.predict(A_normed)

###############################################################################
## Tau theory - Quarteroni book
def upwind_function(x):
    return 1 / np.tanh(x) - 1 / x

def tau_th(h, b_norm, Pe_h, r=1):
    return h / 2 / b_norm / r * upwind_function(Pe_h/r)

z_th = np.zeros((n_mu, 5))
z_th = tau_th(h, np.sqrt(2)*b, Pe_h, degree)

###############################################################################
## 3D Plot
plt.figure()
plt.plot(mu, z_pred, label='NN prediction')
plt.plot(mu, z_th, label='Theory')
plt.xscale('log')
plt.xlabel('Mu')
plt.ylabel('Tau')
plt.legend()
plt.title('degree = {}, h = {}, b = ({},{})'.format(degree, h, b, b))
plt.show()
