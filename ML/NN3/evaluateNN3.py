from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plt
import fenics as fenics

from loadNN import loadModel, loadDataset

import sys
sys.path.insert(1, '../../FEniCS/TD_1/datasetCreation2D_TD/')
from solveSupg2D import solveSupg2D

###############################################################################
## Load the model
modelFileName = "models/model_1x128_relu"

loaded_model = loadModel(modelFileName)

###############################################################################
## Load the dataset
fname = "./../../FEniCS/TD_1/datasetCreation2D_TD/dataset3.csv"
_, _, _, _, mean, std = loadDataset(fname)

###############################################################################
## Set parameters
n = 40
n_mu = 100
degree = 1
Pe_h = 5
mu = np.geomspace(1.5e-3, 1, n_mu)
gamma = np.pi / 4

###############################################################################
## Compute predictions
h = 1 / n
b_norm = Pe_h * 2 * mu / h
bx = b_norm * np.cos(gamma)
by = b_norm * np.sin(gamma)
Pe_g = b_norm / 2 / mu
A = np.zeros((n_mu, 5))

A[:,0].fill(degree)
A[:,1].fill(h)
A[:,2] = Pe_g
A[:,3] = mu
A[:,4].fill(gamma)
A_normed = ( A - mean )/std
z_pred = loaded_model.predict(A_normed)
Z_pred = 10**(-z_pred)

###############################################################################
## Tau theory - Quarteroni book
def upwind_function(x):
    return 1 / np.tanh(x) - 1 / x

def tau_th(h, b_norm, Pe_h, r=1):
    return h / 2 / b_norm / r * upwind_function(Pe_h/r)

Z_th = tau_th(h, b_norm, Pe_h, degree)

###############################################################################
## Compute errors
err_pred = np.zeros(n_mu)
err_th = np.zeros(n_mu)

mesh = fenics.UnitSquareMesh(n, n, 'crossed')
f = fenics.Constant(0)

for i in range(n_mu):
    u_exact = fenics.Expression('(exp(bx * (x[0] - 1) / mu) - exp(-bx / mu)) / (1 - exp(-bx / mu)) + '
                                '(exp(by * (x[1] - 1) / mu) - exp(-by / mu)) / (1 - exp(-by / mu))',
                                mu=mu[i], bx=bx[i], by=by[i], degree=4*degree+1)

    uh_pred = solveSupg2D(mesh, degree, f, u_exact, bx[i], by[i], mu[i], Z_pred[i][0])
    err_pred[i] = fenics.errornorm(u_exact, uh_pred, 'H1')

    uh_th = solveSupg2D(mesh, degree, f, u_exact, bx[i], by[i], mu[i], Z_th[i])
    err_th[i] = fenics.errornorm(u_exact, uh_th, 'H1')

###############################################################################
## 2D Plot
plt.figure()
plt.plot(mu, Z_pred, label='NN prediction')
plt.plot(mu, Z_th, label='Theory')
plt.xscale('log')
plt.xlabel('Mu')
plt.ylabel('Tau')
plt.legend()
plt.title('degree = {}, n = {}, Pe_h = {}'.format(degree, n, Pe_h))

plt.figure()
plt.plot(mu, err_pred, label='NN prediction')
plt.plot(mu, err_th, label='Theory')
plt.xscale('log')
plt.xlabel('Mu')
plt.ylabel('H1 error')
plt.legend()
plt.title('degree = {}, n = {}, Pe_h = {}'.format(degree, n, Pe_h))

plt.show()
