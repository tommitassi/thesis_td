## Transform features
## x = [degree, Pe_local, mu,        bx, by]    into
## x = [degree, h,        Pe_global, mu, gamma]

import numpy as np
from fenics import *

## Vectrize the CSV file
fname_old = "./../../FEniCS/TD_1/datasetCreation2D_TD/appendedDataset1.csv"
fname_new = "tryTransform.csv"

all_features = []
with open(fname_old) as f:
    for i, line in enumerate(f):
        if i == 0:
            header = line[1:].strip().split(", ")
            print("\nHEADER:", header)
            continue  # Skip header
        fields = line.strip().split(",")
        all_features.append([float(v) for v in fields])


old_features = np.array(all_features, dtype="float32")
m, n = old_features.shape
print("\nfeatures.shape: ({}, {})".format(m, n))

nx = 40
mesh = UnitSquareMesh(nx, nx, 'crossed')
h = mesh.hmax()

A = np.zeros((n, m))
A[0] = old_features[:,0]
A[1].fill(h)
A[2] = old_features[:,1] / h # L=1
A[3] = old_features[:,2]
A[4] = np.arctan(old_features[:,4] / old_features[:,3])
A[5] = old_features[:,5]


f = open(fname_new, "w")
f.write('# degree, h, Pe, Mu, Gamma, Tau\n')
np.savetxt(f, A.transpose(), delimiter=",")
f.close()
