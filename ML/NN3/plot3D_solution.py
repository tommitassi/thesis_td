# 3D PLOT OF SOLUTION


from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.tri as tri
import fenics as fenics

from loadNN import loadModel, loadDataset

import sys
sys.path.insert(1, '../../FEniCS/TD_1/datasetCreation2D_TD/')
from solveSupg2D import solveSupg2D
from tauTheory import tauTheory

###############################################################################
## Problem setting

degree = 1
n = 20
mu = 5e-3
b_norm = np.sqrt(2)
gamma = np.pi/4

mesh = fenics.UnitSquareMesh(n, n, 'crossed')
h = mesh.hmax()

Pe_g = b_norm / 2 / mu
Pe_h = Pe_g * h
bx = b_norm * np.cos(gamma)
by = b_norm * np.sin(gamma)

f = fenics.Constant(0)
u_exact = fenics.Expression('(exp(bx * (x[0] - 1) / mu) - exp(-bx / mu)) / (1 - exp(-bx / mu)) + '
                            '(exp(by * (x[1] - 1) / mu) - exp(-by / mu)) / (1 - exp(-by / mu))',
                            mu=mu, bx=bx, by=by, degree=4*degree+1)

###############################################################################
## Model prediction
model_fname = "models/model_1x128_relu"
dataset_fname = "./../../FEniCS/TD_1/datasetCreation2D_TD/dataset3.csv"

model = loadModel(model_fname)
_, _, _, _, mean, std = loadDataset(dataset_fname)

A = np.array([[degree, h, Pe_g, mu, gamma]])
A_normed = ( A - mean )/std
tau_pred = 10**(-model.predict(A_normed)[0][0])
uh_pred = solveSupg2D(mesh, degree, f, u_exact, bx, by, mu, tau_pred)

###############################################################################
## Theory prediction
tau_th = tauTheory(h, b_norm, Pe_h, degree)
uh_th = solveSupg2D(mesh, degree, f, u_exact, bx, by, mu, tau_th)

###############################################################################
## Non-stabilized solution
uh_none = solveSupg2D(mesh, degree, f, u_exact, bx, by, mu, 0)

###############################################################################
## Non-stabilized solution
degree_rise = 3
V = fenics.FunctionSpace(mesh, 'CG', degree+degree_rise)
uh_exact = fenics.project(u_exact, V)

###############################################################################
## Plot function
X = mesh.coordinates()[:,0]
Y = mesh.coordinates()[:,1]
triangulation = tri.Triangulation(X, Y, mesh.cells())

def plot_3D(uh, title="3D Solution"):

    Z = uh.compute_vertex_values()

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.plot_trisurf(triangulation, Z, cmap='viridis')
    plt.title(title)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('uh')

###############################################################################
## Plots
title_params = "n = {}, Pe_h = {:.3}, r = {}".format(n, Pe_h, degree)
plot_3D(uh_pred, title="NN - " + title_params)
plot_3D(uh_th, title="Theory - " + title_params)
plot_3D(uh_none, title="Non-stabilized - " + title_params)
plot_3D(uh_exact, title="Exact - " + title_params)
plt.show()
