## 3D PLOT OF NN's OUTPUT

from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from loadNN import loadDataset, loadModel

###############################################################################
## Plot flags
flag_predictions = 1
flag_theory = 0

# Choose the X-variable:
flag_degree = 0
flag_h = 1
flag_Peg = 0
flag_gamma = 0

###############################################################################
## Set parameters
n_mu = 100
mu = np.geomspace(1.5e-3, 1, n_mu)

nx = 10

if flag_degree:
    degree = np.linspace(1, 3, nx, dtype=int)
    chosenX = degree
    chosenX_label = "degree"
else:
    degree = 1 * np.ones(nx, dtype=int)

if flag_h:
    n = np.linspace(10, 80, nx, dtype=int)
    h = 1 / n
    chosenX = n
    chosenX_label = "n"
else:
    n = 20 * np.ones(nx, dtype=int)
    h = 1 / n

if flag_Peg:
    Pe_g = np.linspace(1.1/h[0], 450, nx)
    chosenX = Pe_g
    chosenX_label = "Pe global"
else:
    Pe_g = 200 * np.ones(nx)

if flag_gamma:
    gamma = np.linspace(0, np.pi/2, nx)
    chosenX = gamma
    chosenX_label = "gamma"
else:
    gamma = np.pi/4 * np.ones(nx)

###############################################################################
## Load the model
modelFileName = "models/model_1x128_relu"

loaded_model = loadModel(modelFileName)

###############################################################################
## Load the dataset
fname = "./../../FEniCS/TD_1/datasetCreation2D_TD/dataset3.csv"

train_features, train_targets, val_features, val_targets, mean, std = loadDataset(fname)
features = np.append(train_features, val_features, axis=0) * std + mean
targets = np.append(train_targets, val_targets, axis=0)
targets = 10**(-targets) #re-transform targets to original values
m, n = features.shape

###############################################################################
## Compute predictions
Z_pred = np.zeros((nx, n_mu))

A = np.zeros((n_mu, 5))
A[:,3] = mu

for i in range(nx):
    A[:,0].fill(degree[i])
    A[:,1].fill(h[i])
    A[:,2].fill(Pe_g[i])
    A[:,4].fill(gamma[i])

    A_normed = ( A - mean )/std
    z_pred = loaded_model.predict(A_normed)
    Z_pred[i,:] = z_pred[:,0]

Z_pred = 10**(-Z_pred) #re-transform targets to original values

###############################################################################
## Tau theory - Quarteroni book
def upwind_function(x):
    return 1 / np.tanh(x) - 1 / x

def tau_th(h, b_norm, Pe_h, r=1):
    return h / 2 / b_norm / r * upwind_function(Pe_h/r)

Z_th = np.zeros((nx, n_mu))
for i in range(nx):
    b_norm = 2 * Pe_g[i] * mu
    Z_th[i,:] = tau_th(h[i], b_norm, Pe_g[i] * h[i], degree[i])

###############################################################################
## 3D Plot
fig = plt.figure()
ax = fig.gca(projection='3d')

X, Y = np.meshgrid(chosenX, mu)
Y = np.log10(Y) # for log scale

# Predictions
if flag_predictions:
    ax.plot_surface(X, Y, Z_pred.T, cmap='viridis')

# Theory
if flag_theory:
    ax.plot_surface(X, Y, Z_th.T)


yticks = [1e-3, 1e-2, 1e-1, 1e-0] # for y log scale
ax.set_yticks(np.log10(yticks)) # for y log scale
ax.set_yticklabels(yticks) # for y log scale

ax.set_xlabel(chosenX_label)
ax.set_ylabel('Mu')
ax.set_zlabel('Tau')

plt.show()
