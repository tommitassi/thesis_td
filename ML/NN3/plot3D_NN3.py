## 3D PLOT OF NN's OUTPUT

from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from loadNN import loadDataset, loadModel

###############################################################################
## Plot flags
flag_predictions = 1
flag_theory = 0
flag_dataset = 0

###############################################################################
## Set parameters
degree = 1
n = 40
h = 1 / n
n_Pe = 10
n_mu = 100
mu_min = 1.5e-3
Pe_g = np.linspace(1.1/h, 450, n_Pe)
mu = np.geomspace(mu_min, 1, n_mu)
gamma = np.pi / 4

###############################################################################
## Load the model
modelFileName = "models/model_2x64"

loaded_model = loadModel(modelFileName)

###############################################################################
## Load the dataset
fname = "./../../FEniCS/TD_1/datasetCreation2D_TD/dataset3.csv"

train_features, train_targets, val_features, val_targets, mean, std = loadDataset(fname)
features = np.append(train_features, val_features, axis=0) * std + mean
targets = np.append(train_targets, val_targets, axis=0)
targets = 10**(-targets) #re-transform targets to original values
m, n = features.shape

###############################################################################
## Compute predictions
Z_pred = np.zeros((n_Pe, n_mu))

A = np.zeros((n_mu, 5))
A[:,0].fill(degree)
A[:,1].fill(h)
A[:,3] = mu
A[:,4].fill(gamma)

for i in range(n_Pe):
    A[:,2].fill(Pe_g[i])
    A_normed = ( A - mean )/std
    z_pred = loaded_model.predict(A_normed)
    Z_pred[i,:] = z_pred[:,0]

Z_pred = 10**(-Z_pred) #re-transform targets to original values

###############################################################################
## Tau theory - Quarteroni book
def upwind_function(x):
    return 1 / np.tanh(x) - 1 / x

def tau_th(h, b_norm, Pe_h, r=1):
    return h / 2 / b_norm / r * upwind_function(Pe_h/r)

Z_th = np.zeros((n_Pe, n_mu))
for i in range(n_Pe):
    b_norm = 2 * Pe_g[i] * mu
    Z_th[i,:] = tau_th(h, b_norm, Pe_g[i] * h, degree)

###############################################################################
## 3D Plot
fig = plt.figure()
ax = fig.gca(projection='3d')

X, Y = np.meshgrid(Pe_g, mu)
Y = np.log10(Y) # for log scale

# Predictions
if flag_predictions:
    ax.plot_surface(X, Y, Z_pred.T, cmap='viridis')

# Theory
if flag_theory:
    ax.plot_surface(X, Y, Z_th.T)

# Dataset
if flag_dataset:
    epsilon = 0.01
    for j in range(m):
        if features[j,0] == degree:
            if features[j,1] > h-epsilon and features[j,1] < h+epsilon:
                if features[j,3] > mu_min:
                    ax.scatter(features[j,2], np.log10(features[j,3]), targets[j])

yticks = [1e-3, 1e-2, 1e-1, 1e-0] # for y log scale
ax.set_yticks(np.log10(yticks)) # for y log scale
ax.set_yticklabels(yticks) # for y log scale

ax.set_xlabel('Pe global')
ax.set_ylabel('Mu')
ax.set_zlabel('Tau')

plt.title('degree = {}, h = {}, gamma = {:.3}°'.format(degree, h, gamma/np.pi*180))
plt.show()
