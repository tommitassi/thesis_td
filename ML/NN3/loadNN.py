## LOAD functions

# loadDataset() loads the NN dataset from a csv file
# it returns the normalized training features and targets, the normalized
# validation features and targets and the mean and standard deviation used to
# normalize the data
# it can plot the dataset if flag_plot == 1 (y axis in logscale if flag_plot_logscale == 1)

from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plt

def loadDataset(fname, flag_plot=0, flag_plot_logscale=0):

    print("Loading dataset")

    all_features = []
    all_targets = []
    with open(fname) as f:
        for i, line in enumerate(f):
            if i == 0:
                header = line[1:].strip().split(", ")
                print("\nHEADER:", header)
                continue  # Skip header
            fields = line.strip().split(",")
            all_features.append([float(v) for v in fields[:-1]])
            all_targets.append([float(fields[-1])])
            if i == 1:
                print("EXAMPLE FEATURES:", all_features[-1])
                print("EXAMPLE TARGETS :", all_targets[-1])

    features = np.array(all_features, dtype="float32")
    targets = np.array(all_targets, dtype="float32")
    targets = - np.log10(targets) ## transform targets for better training
    m, n = features.shape
    print("\nfeatures.shape: ({}, {})".format(m, n))
    print("targets.shape:", targets.shape)

    ######################################
    ## Plot dataset
    if flag_plot == 1:
        for i in range(1, n):
            plt.figure(i)
            for j in range(m):
                if features[j,0] == 1:
                    plt.plot(features[j,i], targets[j], 'or', markersize=1, label='degree 1')
                elif features[j,0] == 2:
                    plt.plot(features[j,i], targets[j], 'ob', markersize=1, label='degree 2')
                else:
                    plt.plot(features[j,i], targets[j], 'og', markersize=1, label='degree 3')
            if flag_plot_logscale == 1:
                plt.xscale('log')
            plt.yscale('log')
            plt.xlabel(header[i])
            plt.ylabel(header[-1])
            plt.legend()
        plt.show()

    ######################################
    ## Extract validation set
    num_val_samples = int(len(features) * 0.2) ## 20% of all features
    index_val_samples = np.linspace(1, int(len(features)-2), num_val_samples, dtype=int) ## equal distribution choice
    val_features = features[index_val_samples]
    val_targets = targets[index_val_samples]
    train_features = np.delete(features, index_val_samples, axis=0)
    train_targets = np.delete(targets, index_val_samples, axis=0)

    print("\nNumber of training samples:", len(train_features))
    print("Number of validation samples:", num_val_samples)

    ######################################
    ## Normalize the data
    mean = np.mean(train_features, axis=0)
    train_features -= mean
    val_features -= mean

    std = np.std(train_features, axis=0)
    train_features /= std
    val_features /= std

    print("\nDataset successfully loaded\n")

    return train_features, train_targets, val_features, val_targets, mean, std

###############################################################################

# loadModel() loads the NN model and weights from respectively a .json and .h5 files
# it returns the model

def loadModel(fname):
    # fname must have the relative path of the model with respect to this file
    # location and must be without extensions
    print("Loading model:", fname)

    # load json and create model
    json_file = open(fname + ".json", 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = keras.models.model_from_json(loaded_model_json)

    # load weights into new model
    loaded_model.load_weights(fname + ".h5")

    print("\nModel successfully loaded from disk")

    return loaded_model
