from tensorflow import keras
import csv
import numpy as np
from datetime import datetime
import matplotlib.pyplot as plt
from loadNN import loadDataset

######################################
## Choose parameters
N_neurons = 64 # 2 layers

activation_fun = "relu"

N_epochs = 30000
BATCH_SIZE = 3936 # None == 32

alpha = 2e-2 # initial learning rate

reg_l2 = 0 # regularization

callback_flag = 1
# logDir = "logs/" + datetime.now().strftime("%Y%m%d-%H%M%S")
logDir = "logs/try/this/2x64_bis"
# logDir = "logs/sizes_1xN/1x4"

save_flag = 1

######################################
## Vectrize the CSV file
fname = "./../../FEniCS/TD_1/datasetCreation2D_TD/dataset3.csv"

train_features, train_targets, val_features, val_targets, mean, std = loadDataset(fname, flag_plot=0, flag_plot_logscale=0)

m, n = train_features.shape
# for i in range(n):
#     print("\n", header[i])
#     print("Max: ", np.amax(train_features[:,i]))
#     print("Min: {}".format(np.amin(train_features[:,i])))
# print(np.where(features[:,3] == np.amin(features[:,3]))) # to find specific data

#####################################
## Buikd the model
model = keras.Sequential(
    [
        keras.layers.Dense(N_neurons, activation=activation_fun, input_shape=(n,)),
        keras.layers.Dense(N_neurons, activation=activation_fun),
        # keras.layers.Dense(N_neurons, activation=activation_fun),
        # keras.layers.Dense(N_neurons, activation=activation_fun), #, kernel_regularizer=keras.regularizers.l2(reg_l2)
        # keras.layers.Dropout(0.1),
        keras.layers.Dense(1, activation=activation_fun),
    ]
)
model.summary()

######################################
## Train the model
metrics = [
    # keras.metrics.MeanSquaredError(),
    keras.metrics.MeanSquaredLogarithmicError(),
]

STEPS_PER_EPOCH = train_features.shape[0]//BATCH_SIZE

lr_schedule = keras.optimizers.schedules.InverseTimeDecay(
  alpha,
  decay_steps=STEPS_PER_EPOCH*1000,
  decay_rate=5,
  staircase=False)

model.compile(
    optimizer=keras.optimizers.Adam(lr_schedule),
    loss="mean_squared_error",
    metrics=metrics
)

if callback_flag:
    tb_callback = [keras.callbacks.TensorBoard(log_dir=logDir)]
else:
    tb_callback = []

model.fit(
    train_features,
    train_targets,
    batch_size=BATCH_SIZE,
    epochs=N_epochs,
    callbacks=tb_callback,
    validation_data=(val_features, val_targets),
)

# ######################################
## Save the model
if save_flag:
    model_json = model.to_json() # save model to json
    with open("models/newModel.json", "w") as json_file:
        json_file.write(model_json)

    model.save_weights("models/newModel.h5") # save weights to HDF5
    print("Saved model to disk")
