## PLOT LEARNING CURVE

from tensorflow import keras
import tensorflow_docs as tfdocs
import tensorflow_docs.modeling
import tensorflow_docs.plots
import numpy as np
from matplotlib import pyplot as plt
from loadNN import loadDataset

######################################
## Choose parameters
m_vec = [100, 500, 1000, 2000, 3936]

N_neurons = 8
max_epochs = 30000
reg_l2 = 0
alpha_init = 1e-2

activation_fun = "relu"

######################################
## Load dataset
fname = "./../../FEniCS/TD_1/datasetCreation2D_TD/dataset3.csv"

train_features, train_targets, val_features, val_targets, mean, std = loadDataset(fname)

######################################
## Set ML functions
lr_schedule = keras.optimizers.schedules.InverseTimeDecay(
  alpha_init,
  decay_steps=1000,
  decay_rate=1,
  staircase=False)

def get_optimizer():
  return keras.optimizers.Adam(lr_schedule)

def get_callbacks(name):
  return [
    tfdocs.modeling.EpochDots(),
    keras.callbacks.TensorBoard(log_dir=name),
  ]

def compile_and_fit(model, name, t_features, t_targets, optimizer=None, BATCH_SIZE=None):

  if optimizer is None:
    optimizer = get_optimizer()
  model.compile(optimizer=optimizer,
                loss="mean_squared_error",
                metrics=[
                  keras.metrics.MeanSquaredLogarithmicError(),
                  keras.metrics.MeanSquaredError()])

  model.summary()

  history = model.fit(
    t_features,
    t_targets,
    batch_size=BATCH_SIZE,
    epochs=max_epochs,
    validation_data=(val_features, val_targets),
    callbacks=get_callbacks(name),
    verbose=0)
  return history

######################################
## Create sub-datasets
size_histories = {}

model = keras.Sequential([
    keras.layers.Dense(N_neurons, activation=activation_fun, kernel_regularizer=keras.regularizers.l2(reg_l2), input_shape=(train_features.shape[1],)),
    keras.layers.Dense(N_neurons, activation=activation_fun, kernel_regularizer=keras.regularizers.l2(reg_l2)),
    keras.layers.Dense(N_neurons, activation=activation_fun, kernel_regularizer=keras.regularizers.l2(reg_l2)),
    keras.layers.Dense(1, activation=activation_fun)
])

for m in m_vec:
    index_samples = np.linspace(0, int(len(train_features)-1), m, dtype=int)

    size_histories["m_{}".format(m)] = compile_and_fit(model, "logs/learningCurve/m_{}".format(m), train_features[index_samples, :], train_targets[index_samples], None, m)

######################################
## Evaluate the models
plotter = tfdocs.plots.HistoryPlotter(metric = 'mean_squared_error', smoothing_std=0)
plotter.plot(size_histories)
plt.yscale("log")
plt.xlabel("Epochs")
plt.show()
