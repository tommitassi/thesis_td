from tensorflow import keras
import tensorflow_docs as tfdocs
import tensorflow_docs.modeling
import tensorflow_docs.plots
import csv
import numpy as np
from matplotlib import pyplot as plt
from datetime import datetime
from loadNN import loadDataset

# go to double precision
# keras.backend.set_floatx('float64')

######################################
## Choose parameters
max_epochs = 30000
BATCH_SIZE = 3936

alpha_init = 1e-2
DECAY_RATE = 5

activation_fun = "relu"

######################################
# Vectrize the CSV file
fname = "./../../FEniCS/TD_1/datasetCreation2D_TD/dataset3.csv"

train_features, train_targets, val_features, val_targets, mean, std = loadDataset(fname)

###############################################################################
## FIND ARCHITECTURE

STEPS_PER_EPOCH = train_features.shape[0]//BATCH_SIZE

## Learning rate schedule
lr_schedule = keras.optimizers.schedules.InverseTimeDecay(
  alpha_init,
  decay_steps=STEPS_PER_EPOCH*1000,
  decay_rate=DECAY_RATE,
  staircase=False)

# lr_schedule = keras.optimizers.schedules.PolynomialDecay(
#     alpha_init,
#     decay_steps=STEPS_PER_EPOCH*1000,
#     end_learning_rate=1e-4,
#     power=20)


## Plot of lr_schedule
# def inverse_learning_rate(step, initial_lr, decay_rate, decay_steps):
#     return initial_lr / (1 + decay_rate * step / decay_steps)
#
# def polynomial_learning_rate(step, initial_lr, end_lr, power):
#     decay_steps = step[-1]
#     return ((initial_lr - end_lr) *(1 - step/decay_steps) ** (power)) + end_lr
#
# def exponential_learning_rate(step, initial_lr, decay_rate, decay_steps):
#     return initial_lr * decay_rate ** (step / decay_steps)
#
# step = np.linspace(0, max_epochs*STEPS_PER_EPOCH)
#
# plt.figure()
# plt.plot(step, inverse_learning_rate(step, alpha_init, 1, 1000), label="1")
# plt.plot(step, inverse_learning_rate(step, alpha_init, 2, 1000), label="2")
# plt.plot(step, inverse_learning_rate(step, alpha_init, 3, 1000), label="3")
# plt.plot(step, inverse_learning_rate(step, alpha_init, 5, 1000), label="5")
# plt.plot(step, inverse_learning_rate(step, alpha_init*2, 6, 1000), label="2_6")
# plt.plot(step, inverse_learning_rate(step, alpha_init*2, 8, 1000), label="2_8")
# plt.plot(step, inverse_learning_rate(step, alpha_init*4, 10, 1000), label="4_10")
# plt.plot(step, polynomial_learning_rate(step, alpha_init, 1e-4, 6), label="poly")
# plt.plot(step, exponential_learning_rate(step, alpha_init, 0.85, 1000), label="exp")
# plt.yscale("log")
# plt.xlabel('Epoch')
# plt.ylabel('Learning Rate')
# plt.legend()
# plt.show()


## Define functions
def get_optimizer():
  return keras.optimizers.Adam(lr_schedule)

def get_callbacks(name):
  return [
    tfdocs.modeling.EpochDots(),
    # keras.callbacks.EarlyStopping(monitor='mean_squared_error', patience=200),
    keras.callbacks.TensorBoard(log_dir=name),
  ]

def compile_and_fit(model, name, optimizer=None):
  if optimizer is None:
    optimizer = get_optimizer()
  model.compile(optimizer=optimizer,
                loss="mean_squared_error",
                metrics=[
                  keras.metrics.MeanSquaredLogarithmicError(),
                  keras.metrics.MeanSquaredError()])

  model.summary()

  history = model.fit(
    train_features,
    train_targets,
    # steps_per_epoch = STEPS_PER_EPOCH,
    batch_size=BATCH_SIZE,
    epochs=max_epochs,
    validation_data=(val_features, val_targets),
    callbacks=get_callbacks(name),
    verbose=0)
  return history

## Create models
tiny_model = keras.Sequential([
    keras.layers.Dense(16, activation=activation_fun, input_shape=(train_features.shape[1],)),
    keras.layers.Dense(16, activation=activation_fun),
    # keras.layers.Dense(16, activation=activation_fun),
    keras.layers.Dense(1, activation=activation_fun)
])

small_model = keras.Sequential([
    keras.layers.Dense(64, activation=activation_fun, input_shape=(train_features.shape[1],)),
    # keras.layers.Dense(32, activation=activation_fun),
    # keras.layers.Dense(32, activation=activation_fun),
    keras.layers.Dense(1, activation=activation_fun)
])

medium_model = keras.Sequential([
    keras.layers.Dense(64, activation=activation_fun, input_shape=(train_features.shape[1],)),
    keras.layers.Dense(64, activation=activation_fun),
    # keras.layers.Dense(64, activation=activation_fun),
    keras.layers.Dense(1, activation=activation_fun)
])

large_model = keras.Sequential([
    keras.layers.Dense(64, activation=activation_fun, input_shape=(train_features.shape[1],)),
    keras.layers.Dense(64, activation=activation_fun),
    keras.layers.Dense(64, activation=activation_fun),
    keras.layers.Dense(1, activation=activation_fun)
])

size_histories = {}

# size_histories['1 layer'] = compile_and_fit(small_model, "logs/try/1_32", optimizer=keras.optimizers.Adam(lr_schedule))
size_histories['2 layers'] = compile_and_fit(medium_model, "logs/try/2_64_float64", optimizer=keras.optimizers.Adam(lr_schedule))
size_histories['3 layers']  = compile_and_fit(large_model, "logs/try/3_64_float64", optimizer=keras.optimizers.Adam(lr_schedule))
# size_histories['2 layers'] = compile_and_fit(medium_model, "logs/optimizers/2x128/SGD/m05_nest", optimizer=keras.optimizers.SGD(lr_schedule, momentum=0.5, nesterov=True))
# size_histories['2 layers'] = compile_and_fit(medium_model, "logs/optimizers/2x128/SGD/m09_nest_05", optimizer=keras.optimizers.SGD(lr_schedule, momentum=0.9, nesterov=True))
# size_histories['RMSprop - m=0 -   rho=0.9'] = compile_and_fit(large_model, "logs/optimizers/2x128/RMSprop/m0r09", optimizer=keras.optimizers.RMSprop(lr_schedule))
# size_histories['RMSprop - m=0.9 - rho=0.99'] = compile_and_fit(large_model, "logs/optimizers/2x128/RMSprop/m09r099", optimizer=keras.optimizers.RMSprop(lr_schedule, momentum=0.9, rho=0.99))


## Evaluate the models
plotter = tfdocs.plots.HistoryPlotter(metric = 'mean_squared_error', smoothing_std=10)
plotter.plot(size_histories)
plt.yscale("log")
plt.xlabel("Epochs")
plt.show()
