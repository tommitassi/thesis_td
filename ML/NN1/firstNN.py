from tensorflow import keras
import csv
import numpy as np
from datetime import datetime
import matplotlib.pyplot as plt

## Vectrize the CSV file
fname = "./../../FEniCS/TD_1/datasetCreation2D_TD/dataset1.csv"

all_features = []
all_targets = []
with open(fname) as f:
    for i, line in enumerate(f):
        if i == 0:
            print("HEADER:", line.strip())
            continue  # Skip header
        fields = line.strip().split(",")
        # all_features.append([float(v.replace('"', "")) for v in fields[:-1]])
        all_features.append([float(v.replace('"', "")) for v in fields[:1]]) #it just takes the first feature
        all_targets.append([float(fields[-1].replace('"', ""))])
        if i == 1:
            print("EXAMPLE FEATURES:", all_features[-1])

features = np.array(all_features, dtype="float32")
targets = np.array(all_targets, dtype="float32")
print("features.shape:", features.shape)
print("targets.shape:", targets.shape)

## Extract validation set
num_val_samples = int(len(features) * 0.2) ## 20% of all features
# index_val_samples = np.random.choice(features.shape[0], num_val_samples, replace=False) ## random choice
index_val_samples = np.linspace(1, int(len(features)-2), num_val_samples, dtype=int) ## equal distribution choice
val_features = features[index_val_samples]
val_targets = targets[index_val_samples]
train_features = np.delete(features, index_val_samples, axis=0)
train_targets = np.delete(targets, index_val_samples, axis=0)

num_train_samples = len(train_features)
print("Number of training samples:", num_train_samples)
print("Number of validation samples:", num_val_samples)

# plt.plot(train_features, train_targets, 'ro', markersize=1)

## Normalize the data
mean = np.mean(train_features, axis=0)
train_features -= mean
val_features -= mean
std = np.std(train_features, axis=0)
# std = np.amax(train_features, axis=0)- np.amin(train_features, axis=0) ## range

# for j in range(features.shape[1]): ## evitate division by 0
#     if std[j]:                     ## evitate division by 0
#         train_features[:,j] /= std[j]
#         val_features[:,j] /= std[j]
train_features /= std
val_features /= std

## Buikd the model
model = keras.Sequential(
    [
        keras.layers.Dense(
            16, activation="tanh", input_shape=(train_features.shape[-1],)
        ),
        # keras.layers.Dense(32, activation="tanh"),
        # keras.layers.Dropout(0.1),
        keras.layers.Dense(1, activation="sigmoid"),
    ]
)
model.summary()

## Train the model
metrics = [
    keras.metrics.MeanSquaredError(),
    keras.metrics.MeanSquaredLogarithmicError(),
]

alpha = 1e-2 # initial learning rate
lr_schedule = keras.optimizers.schedules.InverseTimeDecay(
  alpha,
  decay_steps=1000,
  decay_rate=1,
  staircase=False)

model.compile(
    optimizer=keras.optimizers.Adam(lr_schedule), loss="mean_squared_logarithmic_error", metrics=metrics
)

logDir = "logs/" + datetime.now().strftime("%Y%m%d-%H%M%S")
tb_callback = keras.callbacks.TensorBoard(log_dir=logDir)

model.fit(
    train_features,
    train_targets,
    # batch_size=num_train_samples, # batch gradient descent
    epochs=10000,
    # callbacks=[tb_callback],
    validation_data=(val_features, val_targets),
)

## Save the model
# save model to json
model_json = model.to_json()
with open("models/newModel.json", "w") as json_file:
    json_file.write(model_json)
# save weights to HDF5
model.save_weights("models/newModel.h5")
print("Saved model to disk")

## Evaluate the model
# model.evaluate(val_features, val_targets)
# x_pred = np.geomspace(1.6e-3, 0.3, 1000)
# x_pred_norm = ( x_pred - mean )/std
# y_pred = model.predict(x_pred_norm)
#
# plt.plot(x_pred, y_pred)
# plt.xscale("log")
# plt.xlabel('Mu')
# plt.ylabel('Tau')
# plt.title('NN outcome')
# plt.show()
