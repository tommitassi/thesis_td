from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plt

## Load the model
inputString = input("Type the model file name (leave blank for new models): ")
if not inputString:
    inputString = "newModel"
modelFileName = "models/" + inputString

# load json and create model
json_file = open(modelFileName + ".json", 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = keras.models.model_from_json(loaded_model_json)

# load weights into new model
loaded_model.load_weights(modelFileName + ".h5")
print("Loaded model from disk")


## Load the dataset
fname = "./../FEniCS/TD_1/datasetCreation2D_TD/dataset1.csv"

all_features = []
all_targets = []
with open(fname) as f:
    for i, line in enumerate(f):
        if i == 0:
            continue  # Skip header
        fields = line.strip().split(",")
        all_features.append([float(v.replace('"', "")) for v in fields[:1]])
        all_targets.append([float(fields[-1].replace('"', ""))])

features = np.array(all_features, dtype="float32")
targets = np.array(all_targets, dtype="float32")

mean = np.mean(features, axis=0)
std = np.std(features, axis=0)

## Plots
plt.plot(features, targets, 'ro', markersize=1)

x_pred = np.geomspace(1.6e-3, 0.3, 1000)
x_pred_norm = ( x_pred - mean )/std
y_pred = loaded_model.predict(x_pred_norm)

plt.plot(x_pred, y_pred)
plt.xscale("log")
plt.xlabel('Mu')
plt.ylabel('Tau')
plt.title('NN outcome')
plt.show()
