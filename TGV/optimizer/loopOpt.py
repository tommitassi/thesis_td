from optimizer import *
from functions import *

###############################################################################

x0 = np.array([0.002])
n_proc = 2
folder_path = "solutions/"
fname_dns = "DNS_data.csv"
fname_cb = "optimizer.csv"

T_fin = 0.3
time_step = 2e-3
batch_opt = 0.004
batch_full = 0.1

###############################################################################

n_optimizations = int(T_fin/batch_full)
n_per_batch_full = int(batch_full/time_step)
# n_per_batch_opt = int(batch_size/time_step)
dim = x0.shape[0]
for j in range(3-dim):
    setLine("    set Neural network: A{} coefficient          =".format(2-j), 0.0, folder_path)

x = x0

setLine("    set Time step                =", time_step, folder_path)
setLine("    set Save every n timesteps =", n_per_batch_full, folder_path)

f_callback = open(folder_path + fname_cb, 'w')
f_callback.write("# time, a0\n")
f_callback.close()

for i in range(n_optimizations):

    if i == 0:
        setLine("    set Restart                  =", "false", folder_path)
    else:
        setLine("    set Restart                  =", "true", folder_path)
        setLine("    set Restart initial timestep =", restartTimestep(i, n_per_batch_full), folder_path)

    t_init = i*batch_full
    t_final = i*batch_full+batch_opt
    setLine("    set Initial time             =", t_init, folder_path)
    setLine("    set Final time               =", t_final, folder_path)

    setLine("    set Enable output          =", "false", folder_path)
    setLine("    set Serialize solution     =", "false", folder_path)
    setLine("    set Turbulence quantities filename =", "turbulence.csv", folder_path)

    result = tauOptimizer(x, n_proc, folder_path, t_final, fname_dns)

    if dim == 1:
        x = np.array([result])
    else:
        x = result
    saveResult(x, t_init, fname_cb)

    setLine("    set Final time               =", (i+1)*batch_full, folder_path)
    setLine("    set Enable output          =", "true", folder_path)
    setLine("    set Serialize solution     =", "true", folder_path)
    setLine("    set Turbulence quantities filename =", "turbulence{}.csv".format(i), folder_path)

    lifex_computeSolution(x, n_proc, folder_path)
