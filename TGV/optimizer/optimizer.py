# TAU OPTIMIZER

import multiprocessing as multip
import os, sys
import numpy as np
import scipy.optimize as opt

###############################################################################
## tauOptimizer() finds the coefficients of tau that minimize the error in 'H2'
## norm
## it returns the value of tau

def tauOptimizer(x0, n_proc, folder_path, final_time, fname_dns):

    temp_error = 0
    dim = x0.shape[0]
    tau_iterations = np.empty((0, 1 + dim))

    def errorFunction(x):
        nonlocal temp_error
        nonlocal tau_iterations

        if dim == 1:
            x = np.array([x])

        lifex_computeSolution(x, n_proc, folder_path)
        flag_crashed = checkConvergence(folder_path, final_time)
        temp_error = getError(fname_dns, folder_path+"turbulence_used.csv", flag_crashed)

        print("With x: {}, error is: {}\n".format(x, temp_error))

        # store values to write callback file
        tau_append = []
        for i in range(dim):
            tau_append.append(x[i])
        tau_append.append(temp_error)
        tau_iterations = np.append(tau_iterations, [tau_append], axis=0)

        return temp_error

    # # define bounds: x_i > 0
    # bounds=[(0, None)]
    # for i in range(dim-1):
    #     bounds.append((0, None))
    # res = opt.minimize(errorFunction, x0, method='L-BFGS-B', tol=1e-6, bounds=bounds, options={'disp': True})

    if dim == 1:
        res = opt.minimize_scalar(errorFunction, bracket=(x0[0]/1.5, x0[0]*1.5), tol=1e-5)
    else:
        res = opt.minimize(errorFunction, x0, method='Nelder-Mead', tol=1e-6, options={'disp': True})
        # options={'disp': True, 'xatol': 1e-4, 'fatol': 1e-4}

    fname_cb = folder_path + "callback.csv"
    f_callback = open(fname_cb, 'w')
    f_callback.write("# x_i, error\n")
    np.savetxt(f_callback, tau_iterations, delimiter=',')
    f_callback.close()

    return res.x

###############################################################################
## lifex_computeSolution() computes the solution for a set of coefficients of tau
## it writes the turbulence.csv file
## it returns 0

def lifex_computeSolution(x, n_proc, folder_path):

    # write new coefficients on .prm file
    fname_prm = folder_path + "lifex_example_fluid_dynamics_tgv.prm"
    fname_prm_old = folder_path + "old.prm"
    os.rename(fname_prm, fname_prm_old)
    f_old = open(fname_prm_old, 'r')
    f_new = open(fname_prm, 'w')

    dim = x.shape[0]

    for i, line in enumerate(f_old):

        def setLine(index):
            string = "    set Neural network: A{} coefficient".format(index)

            if line[:len(string)] == string:
                f_new.write(line.replace(line[50:], "{}\n".format(x[index])))
                print("Set A{} coefficient on .prm file as {}".format(index, x[index]))
                return 1

        setted = None
        for j in range(dim):
            if not setted:
                setted = setLine(j)
        if not setted:
            f_new.write(line)

    f_new.close()
    f_old.close()

    # launch new simulation on lifex
    def launch_thread(cmd):
        os.system(cmd)

    cmd = "mpirun -np " + str(n_proc) + " ./lifex_example_fluid_dynamics_tgv -f " + fname_prm + " -o " + folder_path

    print("\nLaunching lifex with {} processors...".format(n_proc))
    new_thread = multip.Process(target=launch_thread, args=(cmd,)) # set new thread by calling function
    new_thread.start() # start new thread
    os.waitpid(new_thread.pid, 0) # wait until new thread is finished
    print("Completed lifex simulation")

    return 0

###############################################################################
## checkConvergence() checks the convergence of life simulation
## it returns a flag (True if crashed, False if not)

def checkConvergence(folder_path, final_time):

    file_path = folder_path + "turbulence"

    data = loadData(file_path + ".csv")

    if np.isclose(data[-1,0], final_time):
        flag_crashed = False
    else:
        flag_crashed = True

    os.system("mv " + file_path + ".csv" + ' ' + file_path + "_used.csv")

    return flag_crashed


###############################################################################
## getError() computes the error between kinetic energies of numerical solution
## and DNS one
## it returns the mean error

def getError(fname_dns, fname_uh, flag_crashed):

    if flag_crashed:
        return 10**10

    dns_data = loadData(fname_dns)
    uh_data = loadData(fname_uh)

    volume = (2 * np.pi) ** 3
    dns_data[:,1:3] = dns_data[:,1:3] * volume # DNS data are normalized over volume

    err = 0
    N = uh_data.shape[0]
    for i in range(N):

        # interpolate DNS on time-steps of numerical solution
        ekin_dns = np.interp(uh_data[i, 0], dns_data[:,0], dns_data[:,1])
        diss_dns = np.interp(uh_data[i, 0], dns_data[:,0], dns_data[:,2])

        # compute error
        prm_lambda = 0 # err = ekin_err + prm_lambda*diss_err

        # kinetic energy error
        err += np.sqrt( (ekin_dns - uh_data[i, 1])**2 )

        # dissipation rate error
        if i == 0:
            diss_uh = - (uh_data[i+1, 1] - uh_data[i, 1]) / (uh_data[i+1, 0] - uh_data[i, 0])
        elif i == N-1:
            diss_uh = - (uh_data[i, 1] - uh_data[i-1, 1]) / (uh_data[i, 0] - uh_data[i-1, 0])
        else:
            diss_uh = - (uh_data[i+1, 1] - uh_data[i-1, 1]) / (uh_data[i+1, 0] - uh_data[i-1, 0])

        err += prm_lambda * np.sqrt( (diss_dns - diss_uh)**2 )

    err /= N

    return err

###############################################################################
## loadData() reads a csv of turbulence data (DNS or numerical)
## it returns a numpy array of the data

def loadData(fname):

    data_list = []
    with open(fname) as f:
        for i, line in enumerate(f):
            if i == 0:
                continue # skip header
            fields = line.strip().split(",")
            data_list.append([float(v) for v in fields[:-1]]) # skip last field (empty)

    return np.array(data_list, dtype="float32")
