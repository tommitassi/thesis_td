import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

N = ["_q2_ult1"]
labels = N
flag_2d = 0
flag_outliers = 0
labels = ["1", "2"]

###############################################################################
def loadCallback(fname, flag_outliers=0):
    data_list = []
    with open(fname) as f:
        for i, line in enumerate(f):
            if i == 0:
                continue # skip header
            fields = line.strip().split(",")
            data_list.append([float(v) for v in fields[:]]) # skip last field (empty)

    data_array = np.array(data_list, dtype="float32")
    x = np.array(data_array[:,0])
    y = np.array(data_array[:,1])
    z = np.array(data_array[:,-1])

    for i in range(x.shape[0]):
        if np.isclose(z[i], 10**10):
            y[i] = 2

    if not flag_outliers:
        x1 = []
        y1 = []
        z1 = []
        for i in range(x.shape[0]):
            if not np.isclose(z[i], 10**10):
                x1 = np.append(x1, x[i])
                y1 = np.append(y1, y[i])
                z1 = np.append(z1, z[i])
        return x1, y1, z1

    return x, y, z
###############################################################################

fig = plt.figure()
if flag_2d:
    ax = fig.gca(projection='3d')

volume = (2 * np.pi) ** 3

i = 0
for n in N:
    fname = "callbacks/callback" + str(n) + ".csv"
    x, y, z = loadCallback(fname, flag_outliers=flag_outliers)
    if flag_2d:
        ax.scatter(x, y, z/volume, label=str(n))
    else:
        plt.plot(x,y, '.', label=labels[i])
    i += 1

if flag_2d:
    ax.set_xlabel('a0')
    ax.set_ylabel('a1')
    ax.set_zlabel('error')
else:
    plt.xlabel('a')
    plt.ylabel('error')
plt.legend()
plt.show()
