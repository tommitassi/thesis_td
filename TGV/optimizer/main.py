from optimizer import *
from functions import *

###############################################################################

x0 = np.array([0.002])
n_proc = 2
T_final = 0.004
folder_path = "solutions/"
fname_dns = "DNS_data.csv"

result = tauOptimizer(x0, n_proc, folder_path, T_final, fname_dns)
print("Result:", result)

setLine("    set Final time               =", 10, folder_path)
lifex_computeSolution(result, n_proc, folder_path)


## plot Error
# x_vec = np.linspace(0.001, 0.005, 2)
# n_proc = 2
# final_time = 0.004
# folder_path = "solutions/"
# fname_dns = "DNS_kinEnergy.csv"
#
# temp_error = 0
# tau_iterations = np.empty((0, 2))
#
# for x in x_vec:
#     lifex_computeSolution(np.array([x]), n_proc, folder_path)
#     flag_crashed = checkConvergence(folder_path, final_time)
#     temp_error = getError(fname_dns, folder_path+"turbulence_used.csv", flag_crashed)
#
#     tau_iterations = np.append(tau_iterations, [[x, temp_error]], axis=0)
#
# fname_cb = folder_path + "callback.csv"
# f_callback = open(fname_cb, 'w')
# f_callback.write("# x_i, error\n")
# np.savetxt(f_callback, tau_iterations, delimiter=',')
# f_callback.close()

###############################################################################

# import os
# import multiprocessing as multip
#
# def f(cmd):
#     os.system(cmd)
#
# cmd = 'mpirun -np 2 ./lifex_example_fluid_dynamics_tgv -o solutions'
#
# print("Launching new process...")
#
# # CALL FUNCTION FOR NEW THREAD
# new_thread = multip.Process(target=f, args=(cmd,))
#
# # INITIALIZE NEW THREAD
# new_thread.start()
#
# # WAIT UNTIL ALL THREADS ARE FINISHED
# os.waitpid(new_thread.pid, 0)
#
# print("Finito cazzo")

###############################################################################
# OLD FUNCTIONS

# ###############################################################################
# ## getVectorSolution() read data from a lifex simulation
# ## it returns the solution as a vector
#
# import h5py
#
# def getVectorSolution(fname, field_name="/velocity", flag_delete=False):
#
#     f = h5py.File(fname, 'r')
#
#     u = f[field_name][:]
#
#     if flag_delete:
#         os.system("rm " + fname)
#
#     return u
#
# ###############################################################################
# ## getError() computes the L2 norm of the error between two vectors of velocity
# ## solutions
# ## it returns the L2-error
#
# def getError(u, uh, flag_crashed=False):
#
#     if flag_crashed:
#         return 10**10
#
#     if u.shape != uh.shape:
#         print("Vectors' size do not match")
#         sys.exit(1)
#
#     err = np.linalg.norm(u-uh, ord=2, axis=1)
#
#     return np.linalg.norm(err, ord=2)
