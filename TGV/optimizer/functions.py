import numpy as np
import os

def setLine(string, value, folder_path):

    fname_prm = folder_path + "lifex_example_fluid_dynamics_tgv.prm"
    fname_prm_old = folder_path + "old.prm"
    os.rename(fname_prm, fname_prm_old)
    f_old = open(fname_prm_old, 'r')
    f_new = open(fname_prm, 'w')

    str_length = len(string)

    for i, line in enumerate(f_old):

        if line[:str_length] == string:
            f_new.write(line.replace(line[str_length:], " {}\n".format(value)))
            print(string+" {}\n".format(value))
        else :
            f_new.write(line)

    f_new.close()
    f_old.close()

    return 0

def restartTimestep(index, n_per_batch):

    value = int(index * n_per_batch)
    n = value
    digits = 0
    while(n > 0):
        digits += 1
        n = n // 10

    string = ""
    for i in range(4-digits):
        string += "0"

    string += str(value)

    return string

def saveResult(x, time, fname_cb):

    dim = x.shape[0]
    line = np.empty((0, 1 + dim))

    # store values to write callback file
    tau_append = [time]
    for i in range(dim):
        tau_append.append(x[i])

    line = np.append(line, [tau_append], axis=0)

    f_callback = open(folder_path + fname_cb, 'a')
    np.savetxt(f_callback, line, delimiter=',')
    f_callback.close()

    return 0
