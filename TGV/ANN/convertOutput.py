
fname_txt = "output.txt"
fname_csv = "callbacks/callback_q1_new2.csv"

f_txt = open(fname_txt, 'r')
f_csv = open(fname_csv, 'a')

string = "With x: ["
str_length = len(string)

for i, line in enumerate(f_txt):

    if line[:str_length] == string:
        f_csv.write(line)

f_csv.close()
f_txt.close()
