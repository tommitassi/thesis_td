import numpy as np
from annFunctions import *
from functions import setLine

folder_path = "solutions/"
fname_model = "models/model2.h5"
fname_dataset = "dataset16_1.csv"

def printWeights(model):
    A = np.zeros(13)

    weights, biases = model.layers[1].get_weights()
    A[0] = biases[0]
    A[1] = biases[1]
    A[2] = weights[0][0]
    A[3] = weights[0][1]

    weights, biases = model.layers[2].get_weights()
    A[4] = biases[0]
    A[5] = biases[1]
    A[6] = weights[0][0]
    A[7] = weights[0][1]
    A[8] = weights[1][0]
    A[9] = weights[1][1]

    weights, biases = model.layers[3].get_weights()
    A[10] = biases[0]
    A[11] = weights[0][0]
    A[12] = weights[1][0]

    for i in range(10):
        setLine("    set Neural network: A{} coefficient          =".format(i), A[i], folder_path)
    for i in range(10, 13):
        setLine("    set Neural network: A{} coefficient         =".format(i), A[i], folder_path)

    print(A)

    return 0

class CustomModel(keras.Model):

    def train_step(self, data):
        # Unpack the data
        x, z = data
        # Retrieve trainable weights
        trainable_vars = self.trainable_variables
        pushWeights(trainable_vars)

        with tf.GradientTape() as tape:
            y_pred = self(x, training=True) ## y = ANN(x)
            z_pred = operation_tf(y_pred) ## custom tf operation with custom gradient
            loss = keras.losses.mean_squared_error(z, z_pred) ## loss

        # Compute gradients
        gradients = tape.gradient(loss, trainable_vars)

        # Update weights
        self.optimizer.apply_gradients(zip(gradients, trainable_vars))

        # Update metrics (includes the metric that tracks the loss)
        loss_tracker.update_state(loss)

        # Return a dict mapping metric names to current value
        return {"loss": loss_tracker.result()}

fname_dataset = folder_path + fname_dataset
x, y = loadAnnDataset(fname_dataset, flag_full=1)
m = x.shape[0]

## Construct an instance of CustomModel
inputs = keras.Input(shape=(1,))
hidden1 = keras.layers.Dense(2, activation="elu")(inputs)
hidden2 = keras.layers.Dense(2, activation="elu")(hidden1)
outputs = keras.layers.Dense(1, activation="elu")(hidden2)
model = CustomModel(inputs, outputs)
model.summary()

model.compile(optimizer=keras.optimizers.Adam(1e-2), run_eagerly=True)

model.load_weights(folder_path + fname_model)

printWeights(model)
