import matplotlib.pyplot as plt
import numpy as np

def loadData(fname):
    all_data = []
    with open(fname) as f:
        for i, line in enumerate(f):
            fields = line.strip().split(",")
            all_data.append([float(v) for v in fields])

    return np.array(all_data, dtype="float32")

z_pred = loadData("solutions/prediction.csv")
z_true = loadData("solutions/z.csv")
loss = loadData("solutions/loss.csv")

m = z_pred.size

absolute_loss = np.abs(z_true-z_pred)
mean_absolute_loss = np.sum(absolute_loss) / m
print("Absolute loss:", mean_absolute_loss)

# squared_loss = (z_pred-z_true)**2
# mean_squared_loss = np.sum(squared_loss) / m
# print("Squared loss:", mean_squared_loss)

mean_loss = np.sum(loss) / m
print("Keras absolute loss:", mean_loss)

plt.figure()
plt.plot(z_pred, '.')
plt.plot(z_true, '.')

plt.figure()
plt.plot(loss, '.')

plt.figure()
plt.plot(absolute_loss, '.')

# plt.figure()
# plt.plot(squared_loss, '.')
# plt.title("Squared")

plt.show()
