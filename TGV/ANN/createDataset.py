import h5py
import numpy as np

filename = "referenceSolution/solution_ref_proj32q2_0001.h5"
file = h5py.File(filename, 'r')
# nodes = file['/Data0'][:]
u = file['/Block_0_t000000/Node/velocity'][:]
#
# filename = "solutions16/standardTau/solution_0010.h5"
# file = h5py.File(filename, 'r')
# uh = file['/velocity'][:]
#
# if u.shape != uh.shape:
#     print("ERROR: dim(uh) != dim(u)")
#
# # velocity modules
# u_mag = np.linalg.norm(u, ord=2, axis=1)
# uh_mag = np.linalg.norm(uh, ord=2, axis=1)
#
# err = np.abs(u_mag - uh_mag)
# print(np.linalg.norm(err, ord=2))

folder_path = "solutions32_q2/"
filename = folder_path + "solution_0000.h5"
# filename = "/home/tommit/Desktop/solution_0000_tris.h5"
file = h5py.File(filename, 'r')
uh = file['/velocity'][:]

A = np.zeros((uh.shape[0],2))
A[:,0] = np.linalg.norm(uh, ord=2, axis=1)
A[:,1] = np.linalg.norm(u, ord=2, axis=1)
f1 = open(folder_path + "newDataset.csv", "w")
f1.write('# u_norm_0, u_norm_ref_2\n')
np.savetxt(f1, A, delimiter=",")
f1.close()
