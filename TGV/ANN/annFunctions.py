from tensorflow import keras
import numpy as np

###############################################################################
## trainSingleAnn() trains an ANN from a dataset
## if flag_save==1, the model is saved on disk
## it returns nothing

def trainSingleAnn(fname_dataset, n_nodes, n_layers, max_epochs=10000,
                   batchType="full", lr0=1e-2, activation="relu",
                   decay_rate=1, flag_save=0):

    ##############
    # Load dataset
    train_x, train_y, val_x, val_y = loadAnnDataset(fname_dataset)
    m, n = train_x.shape

    ##############
    # Build model
    model = buildModel(n, n_nodes, n_layers, activation)

    ##############
    # Compile the model
    batch_size = getBatch(batchType, m)
    setupModel(model, m, batch_size, lr0, decay_rate)

    ##############
    # Train the model
    history = trainModel(model, train_x, train_y, val_x, val_y,
                         max_epochs, batch_size)

    ##############
    # Save the model
    if flag_save:
        saveModel(model)

    return model

###############################################################################
## saveModel() saves a ANN model on .json and .h5 files
## it returns 0

def saveModel(model, fname="models/newModel"):

    model_json = model.to_json() # save model to json
    with open(fname+".json", "w") as json_file:
        json_file.write(model_json)

    model.save_weights(fname+".h5") # save weights to HDF5
    print("Saved model to disk")

    return 0

###############################################################################
## buildModel() builds a sequential ANN model
## it returns the built model

def buildModel(n_features, n_nodes, n_layers, activation):

    model = keras.Sequential()

    # input layer
    model.add(keras.layers.Dense(n_nodes,
                                 activation=activation, # i.e. linear
                                 input_shape=(n_features,)))

    # hidden layers
    for i in range(n_layers-1):
        model.add(keras.layers.Dense(n_nodes,
                                     activation=activation,
                                     ))

    # output layer
    model.add(keras.layers.Dense(1, activation=activation))

    model.summary()

    return model

###############################################################################
## setupModel() setups the learning rate schedule, the metrics and compiles the
## model
## it returns nothing

def setupModel(model, m_dataset, batch_size, lr0, decay_rate=1):

    steps_per_epoch = m_dataset//batch_size

    lr_schedule = keras.optimizers.schedules.InverseTimeDecay(
        lr0,
        decay_steps=steps_per_epoch*1000,
        decay_rate=decay_rate,
        staircase=False)

    metrics = [
        keras.metrics.MeanSquaredError(),
        # keras.metrics.MeanSquaredLogarithmicError()
        ]

    model.compile(
        optimizer=keras.optimizers.Adam(lr0),
        # optimizer=keras.optimizers.SGD(lr0, momentum=0.9),
        loss="mean_squared_error",
        # metrics=metrics
        )

    return 0

###############################################################################
## trainModel() trains given model on given dataset
## it returns the history of the training

def trainModel(model, train_x, train_y, val_x, val_y, max_epochs, batch_size,
               callback=None, verbose=1):

    history = model.fit(
        train_x,
        train_y,
        batch_size=batch_size,
        epochs=max_epochs,
        validation_data=(val_x, val_y),
        callbacks=callback,
        verbose=verbose)

    return history

###############################################################################
## getBatch() gets the batch size from the string batchType (batchType == "full"
## takes the full dataset, while batchType == "n" takes a mini-batch of size n)
## it returns the batch size

def getBatch(batchType, m_dataset):

    if batchType == "full":
        batch_size = m_dataset
    else:
        batch_size = int(batchType)

    return batch_size

###############################################################################
## loadAnnDataset() loads the ANN dataset from a csv file
## it returns the normalized training features and targets, the normalized
## validation features and targets

def loadAnnDataset(fname, flag_full=0):

    print("Loading dataset")

    all_features = []
    all_targets = []
    with open(fname) as f:
        for i, line in enumerate(f):
            if i == 0:
                header = line[1:].strip().split(", ")
                print("\nHEADER:", header)
                continue  # Skip header
            fields = line.strip().split(",")
            all_features.append([float(v) for v in fields[:-1]])
            all_targets.append([float(fields[-1])])
            if i == 1:
                print("EXAMPLE FEATURES:", all_features[-1])
                print("EXAMPLE TARGETS :", all_targets[-1])

    features = np.array(all_features, dtype="float32")
    targets = np.array(all_targets, dtype="float32")
    m, n = features.shape
    print("\nfeatures.shape: ({}, {})".format(m, n))
    print("targets.shape:", targets.shape)

    ######################################
    ## Transform targets for better training
    # targets = - np.log10(targets) # no normalization

    ######################################
    ## Extract validation set
    if flag_full:
        return features, targets

    num_val_samples = int(len(features) * 0.2) ## 20% of all features
    index_val_samples = np.linspace(1, int(len(features)-2), num_val_samples, dtype=int) ## equal distribution choice
    # index_val_samples = np.arange(m-num_val_samples, m) ## last 20% choice
    val_features = features[index_val_samples]
    val_targets = targets[index_val_samples]
    train_features = np.delete(features, index_val_samples, axis=0)
    train_targets = np.delete(targets, index_val_samples, axis=0)

    print("\nNumber of training samples:", len(train_features))
    print("Number of validation samples:", num_val_samples)


    print("\nDataset successfully loaded\n")

    return train_features, train_targets, val_features, val_targets

###############################################################################
## loadAnnModel() loads the ANN model and weights from respectively a .json and
## .h5 files
## it returns the model

def loadAnnModel(fname, custom_objects=None):
    # fname must be the absolute path of the model's files without extensions

    print("Loading model:", fname)

    # create the model from .json
    json_file = open(fname + ".json", 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = keras.models.model_from_json(loaded_model_json,
                                                custom_objects=custom_objects)

    # load weights into the model from .h5
    loaded_model.load_weights(fname + ".h5")

    print("\nModel successfully loaded from disk")

    return loaded_model
