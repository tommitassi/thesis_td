import matplotlib.pyplot as plt
import numpy as np

folder_path = "solutions32/"
fname_predictions = "y_pred1.csv"

def loadData(fname):
    all_data = []
    with open(fname) as f:
        for i, line in enumerate(f):
            fields = line.strip().split(",")
            all_data.append([float(v) for v in fields])

    return np.array(all_data, dtype="float32")

A = loadData("solutions32_tez/y_pred.csv")
# B = loadData("solutions32/y_pred.csv")

x = A[:,0]

tezduyar = 1 / np.sqrt(250012,288 + x**2 *32) # for 32
# tezduyar = 1 / np.sqrt(250000,768 + x**2 *16) # for 16

np1 = 0.0046795 / np.sqrt(1 + 0.0 * x**2)
np2 = 0.0045377 / np.sqrt(1 + -0.2269348 * x**2)

plt.figure()

# plt.plot(B[:,0], B[:,1])
plt.plot(A[:,0], tezduyar, '#ff7f0e', label=r"Theoretical")
plt.plot(A[:,0], np1, '#2ca02c', label=r"Optimized $N_p = 1$")
plt.plot(A[:,0], np2, '#d62728', label=r"Optimized $N_p = 2$")
plt.plot(A[:,0], A[:,1], '#9467bd', label=r"ANN")

plt.xlabel(r'$|u_h|$', fontsize=12)
plt.ylabel(r'$\tau_M(u_h)$', fontsize=12)
plt.xlim(0,1)

plt.legend()

plt.show()
