## TRAIN Artificial Neural Network

from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plt
from annFunctions import *

fname_dataset = "initial_tezq2.csv"
n_nodes = 2
n_layers = 2
max_epochs = 1
activation = "elu"
flag_save = 1
lr0 = 1e-2

###############################################################################
# Main
# model = trainSingleAnn(fname_dataset, n_nodes, n_layers,
#                        max_epochs=max_epochs, activation=activation,
#                        flag_save=flag_save, lr0=lr0)

model = loadAnnModel("models/modelq2")

train_features, train_targets, val_features, val_targets = loadAnnDataset(fname_dataset)
u_norm = np.linspace(0, 1, 1000)
plt.plot(train_features, train_targets, '.', color='#ff7f0e', label='Dataset examples')
plt.plot(u_norm, model.predict(u_norm), '#1f77b4', linewidth=2, label='ANN')
plt.xlabel(r"$|u_h|$", fontsize=12)
plt.ylabel(r"$\tau_M$", fontsize=12)
plt.legend()
plt.show()

#########################################
# model = loadAnnModel("models/model1")
#
# print("Layer 0")
# weights, biases = model.layers[0].get_weights()
# w10 = biases[0]
# print("  w10:", biases[0])
# w11 = biases[1]
# print("  w11:", biases[1])
# w12 = weights[0][0]
# print("  w12:", weights[0][0])
# w13 = weights[0][1]
# print("  w13:", weights[0][1])
#
# print("Layer 1")
# weights, biases = model.layers[1].get_weights()
# w20 = biases[0]
# print("  w20:", biases[0])
# w21 = biases[1]
# print("  w21:", biases[1])
# w22 = weights[0][0]
# print("  w22:", weights[0][0])
# w23 = weights[0][1]
# print("  w23:", weights[0][1])
# w24 = weights[1][0]
# print("  w24:", weights[1][0])
# w25 = weights[1][1]
# print("  w25:", weights[1][1])
#
# print("Layer 2")
# weights, biases = model.layers[2].get_weights()
# w30 = biases[0]
# print("  w30:", biases[0])
# w31 = weights[0][0]
# print("  w31:", weights[0][0])
# w32 = weights[1][0]
# print("  w32:", weights[1][0])
#
# def act(x):
#     if x < 0:
#         return (np.exp(x) - 1.)
#     else:
#         return x
#
# def tau(u):
#     return act( w30 + w31 * act(w20 + w22* act(w10+w12*u) + w24 * act(w11+w13*u)) + w32 * act( w21 + w23 * act(w10+w12*u) + w25 * act(w11+w13*u)) )
#
# train_features, train_targets, val_features, val_targets = loadAnnDataset(fname_dataset)
# u = 0.1
# print(tau(u))
# print(model.predict(np.array([u]))[0][0])
# print(0.018 / np.sqrt(1 + 0.8*u*u))

############################################
# for i in range(n_layers+1):
#     weights, biases = model.layers[i].get_weights()
#     print("\nLayer:", i)
#     print("  biases:", biases)
#     print("  weights:", weights)
