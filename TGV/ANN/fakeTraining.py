import tensorflow as tf
from tensorflow import keras
import numpy as np
import os, sys, h5py
from annFunctions import *

tf.executing_eagerly()

folder_path = "solutions/"
sol1_fname = "solution_0500.h5"
sol2_fname = "solution_0500_grad.h5"

max_epochs = 1
lr0 = 1e-1

fname_dataset = "dataset32_1s.csv"
fname_weigths = "weights.csv"
fname_model_old = "models/model4.h5"
fname_model_new = "models/newModel"

loss_tracker = keras.metrics.Mean(name="loss")

class CustomModel(keras.Model):

    def train_step(self, data):
        # Unpack the data
        x, z = data
        # Retrieve trainable weights
        trainable_vars = self.trainable_variables
        pushWeights(trainable_vars)

        with tf.GradientTape() as tape:
            y_pred = self(x, training=True) ## y = ANN(x)
            z_pred = operation_tf(y_pred) ## custom tf operation with custom gradient
            loss = keras.losses.mean_squared_error(z, z_pred) ## loss

        # Compute gradients
        gradients = tape.gradient(loss, trainable_vars)

        # Update weights
        self.optimizer.apply_gradients(zip(gradients, trainable_vars))

        # Update metrics (includes the metric that tracks the loss)
        loss_tracker.update_state(loss)

        # Return a dict mapping metric names to current value
        return {"loss": loss_tracker.result()}


def operation_np(y):
    return fakeLaunch(sol1_fname)

def operation_grad_np(y, z, dz):
    z_delta_y = fakeLaunch(sol2_fname)
    return np.array(np.multiply(10000 * np.divide(z_delta_y - z, y[:,0]), dz), dtype='float32', ndmin=2).transpose()

@tf.custom_gradient
def operation_tf(y):
    z = tf.numpy_function(operation_np, [y], y.dtype)
    def grad(dz):
        return tf.numpy_function(operation_grad_np, [y, z, dz], y.dtype)
    return z, grad

def pushWeights(trainable_vars):
    A = np.zeros(13)

    A[0]  = np.array(trainable_vars[1])[0]
    A[1]  = np.array(trainable_vars[1])[1]
    A[2]  = np.array(trainable_vars[0])[0][0]
    A[3]  = np.array(trainable_vars[0])[0][1]
    A[4]  = np.array(trainable_vars[3])[0]
    A[5]  = np.array(trainable_vars[3])[1]
    A[6]  = np.array(trainable_vars[2])[0][0]
    A[7]  = np.array(trainable_vars[2])[0][1]
    A[8]  = np.array(trainable_vars[2])[1][0]
    A[9]  = np.array(trainable_vars[2])[1][1]
    A[10] = np.array(trainable_vars[5])[0]
    A[11] = np.array(trainable_vars[4])[0][0]
    A[12] = np.array(trainable_vars[4])[1][0]

    f = open(folder_path + fname_weigths, "w")
    np.savetxt(f, A.transpose(), delimiter=",")
    f.close()

    return 0

def fakeLaunch(fname):
    file_path = folder_path + fname

    file = h5py.File(file_path + ".h5", 'r')
    uh = file['/velocity'][:]

    return np.linalg.norm(uh, ord=2, axis=1)


################################################
fname_dataset = folder_path + fname_dataset
x, y = loadAnnDataset(fname_dataset, flag_full=1)
m = x.shape[0]

## Construct an instance of CustomModel
inputs = keras.Input(shape=(1,))
hidden1 = tf.keras.layers.Dense(2, activation="elu")(inputs)
hidden2 = tf.keras.layers.Dense(2, activation="elu")(hidden1)
outputs = keras.layers.Dense(1, activation="elu")(hidden2)
model = CustomModel(inputs, outputs)
model.summary()

lr_schedule = keras.optimizers.schedules.InverseTimeDecay(lr0, decay_steps=10, decay_rate=1)
model.compile(optimizer=keras.optimizers.Adam(lr_schedule), run_eagerly=True)

model.load_weights(folder_path + fname_model_old)

history = model.fit(x, y,
            batch_size=m,
            epochs=max_epochs,
            )

saveModel(model, fname=folder_path + fname_model_new)
