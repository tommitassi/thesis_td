from tensorflow import keras
import numpy as np
from annFunctions import loadAnnModel

folder_path = "solutions32/"
fname_model = "models/model10b_32"
fname_output = "y_pred.csv"

class CustomModel(keras.Model):

    def train_step(self, data):
        # Unpack the dataset
        x, z = data
        # Retrieve trainable weights
        trainable_vars = self.trainable_variables
        pushWeights(trainable_vars) # save on .csv

        with tf.GradientTape() as tape:
            y_pred = self(x, training=True) ## y = ANN(x)
            z_pred = operation_tf(y_pred) ## custom tf operation with custom gradient
            loss = keras.losses.mean_absolute_error(z, z_pred) ## loss

        # Compute gradients
        gradients = tape.gradient(loss, trainable_vars)

        # Update weights
        self.optimizer.apply_gradients(zip(gradients, trainable_vars))

        # Update metrics (includes the metric that tracks the loss)
        loss_tracker.update_state(loss)
        pushLoss(loss_tracker.result().numpy()) # save on .csv

        # Return a dict mapping metric names to current value
        return {"loss": loss_tracker.result()}

model = loadAnnModel(folder_path + fname_model, custom_objects={"CustomModel": CustomModel})
x = np.linspace(0,1, 100)
y_pred = model.predict(x)/1000
A = np.column_stack((x,y_pred))

f = open(folder_path + fname_output, "w")
np.savetxt(f, A, delimiter=",")
f.close()
