import numpy as np

fname = "initial_tezq2.csv"

f1 = open(fname, "w")
f1.write('# u_norm, tau\n')
f1.close()

size = 100
u_norm = np.linspace(0, 1, size)
# tau = 2 / np.sqrt(1 + 0.8*u_norm**2)
# tau = 1000 / np.sqrt(250012,288 + 32 * u_norm**2)
tau = 1000 / np.sqrt(2524.576 + 1024.0 * u_norm**2)

A = np.zeros((2, size))
A[0,:] = u_norm
A[1,:] = tau

f2 = open(fname, "ab")
np.savetxt(f2, A.transpose(), delimiter=",")
f2.close()
