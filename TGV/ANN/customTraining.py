# import multiprocessing as multip
import tensorflow as tf
from tensorflow import keras
import numpy as np
import os, sys, h5py
from annFunctions import *
from functions import setLine

tf.executing_eagerly()

folder_path = "solutions32/"
fname_model_old = "models/model10b_32.h5"

max_epochs = 43
lr0 = 1e-3
# n_proc = 2 # it doesnt work

sol_fname = "solution_0001"
fname_dataset = "dataset32_1.csv"

fname_weigths = "weights.csv"
fname_model_new = "models/newModel"
fname_loss = "loss_history.csv"

loss_tracker = keras.metrics.Mean(name="loss")

class CustomModel(keras.Model):

    def train_step(self, data):
        # Unpack the dataset
        x, z = data
        # Retrieve trainable weights
        trainable_vars = self.trainable_variables
        pushWeights(trainable_vars) # save on .csv

        with tf.GradientTape() as tape:
            y_pred = self(x, training=True) ## y = ANN(x)
            z_pred = operation_tf(y_pred) ## custom tf operation with custom gradient
            loss = keras.losses.mean_absolute_error(z, z_pred) ## loss

        # For debugging
        # pushTensor(loss, "loss.csv")
        # pushTensor(tf.transpose(z_pred), "prediction.csv")
        # pushTensor(tf.transpose(z), "z.csv")

        # Compute gradients
        gradients = tape.gradient(loss, trainable_vars)

        # Update weights
        self.optimizer.apply_gradients(zip(gradients, trainable_vars))

        # Update metrics (includes the metric that tracks the loss)
        loss_tracker.update_state(loss)
        pushLoss(loss_tracker.result().numpy()) # save on .csv

        # Return a dict mapping metric names to current value
        return {"loss": loss_tracker.result()}


def operation_np(y):
    z = launch_lifex("false")
    return z

def operation_grad_np(y, z, dz):
    z_delta_y = launch_lifex("true")
    grad = 10000 * np.divide(z_delta_y - z, y) # delta_y = y/10
    return np.multiply(grad, dz)

@tf.custom_gradient
def operation_tf(y):
    z = tf.numpy_function(operation_np, [y], y.dtype)
    def grad(dz):
        return tf.numpy_function(operation_grad_np, [y, z, dz], y.dtype)
    return z, grad


def launch_lifex(flag_gradient):

    ## Set .prm file (i.e. update weights)
    A = []
    with open(folder_path + fname_weigths) as f:
        for i, line in enumerate(f):
            fields = line.strip().split(",")
            A.append([float(v) for v in fields])

    for i in range(10):
        setLine("    set Neural network: A{} coefficient          =".format(i), A[i][0], folder_path)
    for i in range(10, 13):
        setLine("    set Neural network: A{} coefficient         =".format(i), A[i][0], folder_path)

    setLine("    set Neural network gradient computation     =", flag_gradient, folder_path)

    ## Launch lifex
    def launch_thread(cmd):
        os.system(cmd)

    fname_prm = folder_path + "lifex_example_fluid_dynamics_tgv.prm"

    cmd = "./lifex_example_fluid_dynamics_tgv -f " + fname_prm + " -o " + folder_path
    launch_thread(cmd)

    # cmd = "mpirun -np " + str(n_proc) + " ./lifex_example_fluid_dynamics_tgv -f " + fname_prm + " -o " + folder_path
    # print("\nLaunching lifex with {} processors...".format(n_proc))
    # new_thread = multip.Process(target=launch_thread, args=(cmd,)) # set new thread by calling function
    # new_thread.start() # start new thread
    # os.waitpid(new_thread.pid, 0) # wait until new thread is finished
    # print("Completed lifex simulation")

    ## Take results
    file_path = folder_path + sol_fname

    if os.path.exists(file_path + ".h5"):

        file = h5py.File(file_path + ".h5", 'r')
        uh = file['/velocity'][:].astype('float32')
        uh_norm = np.linalg.norm(uh, ord=2, axis=1)

        os.system("mv " + file_path + ".h5" + ' ' + file_path + "_used.h5")

    else: # the simulation crashed

        sys.exit("Lifex crashed")

    return np.array(uh_norm, dtype='float32', ndmin=2).transpose()

def pushWeights(trainable_vars):
    A = np.zeros(13)

    A[0]  = np.array(trainable_vars[1])[0]
    A[1]  = np.array(trainable_vars[1])[1]
    A[2]  = np.array(trainable_vars[0])[0][0]
    A[3]  = np.array(trainable_vars[0])[0][1]
    A[4]  = np.array(trainable_vars[3])[0]
    A[5]  = np.array(trainable_vars[3])[1]
    A[6]  = np.array(trainable_vars[2])[0][0]
    A[7]  = np.array(trainable_vars[2])[0][1]
    A[8]  = np.array(trainable_vars[2])[1][0]
    A[9]  = np.array(trainable_vars[2])[1][1]
    A[10] = np.array(trainable_vars[5])[0]
    A[11] = np.array(trainable_vars[4])[0][0]
    A[12] = np.array(trainable_vars[4])[1][0]

    f = open(folder_path + fname_weigths, "w")
    np.savetxt(f, A.transpose(), delimiter=",")
    f.close()

    return 0

def pushLoss(loss):

    f = open(folder_path + fname_loss, "a")
    f.write(str(loss) + ',\n')
    f.close()

    return 0

# For debugging
def pushTensor(tensor, fname):

    proto_tensor = tf.make_tensor_proto(tensor)
    np_array = tf.make_ndarray(proto_tensor)

    f = open(folder_path + fname, "w")
    np.savetxt(f, np_array.transpose(), delimiter=",")
    f.close()

    return 0

################################################
fname_dataset = folder_path + fname_dataset
x, y = loadAnnDataset(fname_dataset, flag_full=1)
m = x.shape[0]

## Construct an instance of CustomModel
inputs = keras.Input(shape=(1,))
hidden1 = tf.keras.layers.Dense(2, activation="elu")(inputs)
hidden2 = tf.keras.layers.Dense(2, activation="elu")(hidden1)
outputs = keras.layers.Dense(1, activation="elu")(hidden2)
model = CustomModel(inputs, outputs)
model.summary()

# lr_schedule = keras.optimizers.schedules.InverseTimeDecay(lr0, decay_steps=10, decay_rate=1)
model.compile(optimizer=keras.optimizers.Adam(lr0), run_eagerly=True)

model.load_weights(folder_path + fname_model_old)
history = model.fit(x, y,
            batch_size=m,
            epochs=max_epochs,
            shuffle=False,
            )

saveModel(model, fname=folder_path + fname_model_new)
