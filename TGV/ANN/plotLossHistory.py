import matplotlib.pyplot as plt
import numpy as np

def loadData(fname):
    all_data = []
    with open(fname) as f:
        for i, line in enumerate(f):
            fields = line.strip().split(",")
            all_data.append([float(v) for v in fields[:-1]])

    return np.array(all_data, dtype="float32")

loss = loadData("solutions32_tez/loss_history.csv")

plt.plot(loss[:,0], marker='.')
plt.xlabel("Epochs", fontsize=12)
plt.xticks([0, 2, 4, 6, 8, 10, 12, 14, 16, 18])
# plt.ylabel(r"$\frac{1}{K} \sum_k \; L(W, x_k)$", fontsize=12)
plt.ylabel("Mean loss function", fontsize=12)
# plt.yscale("log")
plt.show()
